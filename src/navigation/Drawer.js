/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';
import { StyleSheet, useWindowDimensions} from 'react-native';

import {
  DrawerItem,
  createDrawerNavigator,
  DrawerContentScrollView,
  useDrawerProgress,
  useDrawerStatus,
} from '@react-navigation/drawer';

import {useTheme} from '@react-navigation/native';

import Animated, {color} from 'react-native-reanimated';

import LinearGradient from 'react-native-linear-gradient';

import {Icon, Grid, Col, Thumbnail} from 'native-base';
import {View} from 'react-native';

import MainTabNavigation from './MainTabNavigation';

import {Alert} from 'react-native';
import {AuthProvider} from '../providers/Auth';
import {useDispatch, useSelector} from 'react-redux';
import {getCurrentUserAction} from '../redux/actions/CurrentUser';
import {setDrawerSelectedItemAction} from '../redux/actions/DrawerSelectedItem';
import StyledText from '../components/StyledText';

const Drawer = createDrawerNavigator();

const Screens = ({style}) => {
  return (
    <Animated.View style={StyleSheet.flatten([styles.stack, style])}>
      <MainTabNavigation />
    </Animated.View>
  );
};

const DrawerContent = props => {
  const progress = useDrawerProgress();
  const {width} = useWindowDimensions();
  props.setProgress(progress);

  const animated_width = Animated.interpolateNode(progress, {
    inputRange: [0, 1],
    outputRange: [width / 2, width / 1.67],
  });

  const dispatch = useDispatch();
  const {drawer_selected_item = 'home'} = useSelector(
    state => state.drawer_selected_item,
  );
  const getCurrentUser = useSelector(state => state.currentUser);
  const {currentUser = {}} = getCurrentUser;

  const {colors} = useTheme();

  const Items = [
    {
      key: 'home',
      label: 'Accueil',
      icon: 'home',
      route: 'HomeTab',
    },
    {
      key: 'store',
      label: 'Boutique',
      icon: 'shopping-cart',
      route: 'ShopTab',
    },
    {
      key: 'messages',
      label: 'Messages',
      icon: 'message',
      route: 'MessagesTab',
    },
    {
      key: 'settings',
      label: 'Paramètres',
      icon: 'settings',
      route: 'SettingsScreen',
    },
    {
      key: 'projects',
      label: 'Projets',
      icon: 'engineering',
      route: 'ProjectsTab',
    },
    {
      key: 'about',
      label: 'À propos',
      icon: 'contact-support',
      route: 'AboutScreen',
    },
  ];

  return (
    <Animated.View
      // {...props}
      style={{
        flex: 1,
        justifyContent: 'center',
        width: animated_width,
        backgroundColor: '#eee',
      }}>
      {/* <StatusBar backgroundColor={colors.primary} /> */}
      <View>
        <View
          style={{
            bottom: 60,
            marginLeft: 5,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <Thumbnail
            style={{marginRight: 10}}
            source={require('./../assets/images/default.png')}
          />
          <Animated.View style={{width: '60%'}}>
            <StyledText type="medium">{currentUser?.displayName}</StyledText>
            <StyledText type="regular" style={{color: colors.gray}}>
              {currentUser?.phone}
            </StyledText>
          </Animated.View>
        </View>
        {Items.map(item => (
          <DrawerItem
            focused={item.key === drawer_selected_item}
            activeBackgroundColor={colors.primary + '1d'}
            key={item.key}
            label={item.label}
            labelStyle={[
              styles.drawerLabel,
              {
                color:
                  item.key === drawer_selected_item
                    ? colors.primary
                    : colors.drawer_item,
                fontFamily:
                  item.key === drawer_selected_item
                    ? 'ProductSans-Bold'
                    : 'ProductSans-Regular',
              },
            ]}
            onPress={() => {
              dispatch(setDrawerSelectedItemAction(item.key));
              props.navigation.navigate(item.route);
            }}
            icon={() => (
              <Icon
                type="MaterialIcons"
                name={item.icon}
                style={{
                  color:
                    item.key === drawer_selected_item
                      ? colors.primary
                      : colors.drawer_item,
                  fontSize: 25,
                }}
              />
            )}
          />
        ))}

        <DrawerItem
          label="Deconnexion"
          style={{marginTop: 30}}
          labelStyle={[styles.drawerLabel, {color: colors.drawer_item}]}
          icon={() => (
            <Icon
              type="MaterialIcons"
              name="logout"
              style={{color: colors.drawer_item}}
              size={16}
            />
          )}
          onPress={() => {
            Alert.alert('INFORMATIONS', 'Voulez vraiment vous déconnecter ?', [
              {
                text: 'Annuler',
                style: 'cancel',
              },
              {
                text: 'Confirmer',
                onPress: async () => {
                  await AuthProvider.signOut();
                  dispatch(getCurrentUserAction());
                },
              },
            ]);
          }}
        />
      </View>
    </Animated.View>
  );
};

const DrawerNavigator = () => {
  const [progress, setProgress] = React.useState(new Animated.Value(0));
  const scale = Animated.interpolateNode(progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });
  const borderRadius = Animated.interpolateNode(progress, {
    inputRange: [0, 1],
    outputRange: [0, 16],
  });
  // const translateX = Animated.interpolateNode(progress, {
  //   inputRange: [0, 1],
  //   outputRange: [0, -43],
  // });

  const animatedStyle = {borderRadius, transform: [{scale}]};

  return (
    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerStyle: styles.drawerStyles,
        drawerContentContainerStyle: {flex: 1},
        sceneContainerStyle: {backgroundColor: '#eee'},
        drawerType: 'slide',
        // drawerHideStatusBarOnOpen: true,
        overlayColor: 'transparent',
        drawerActiveBackgroundColor: 'transparant',
        drawerActiveTintColor: 'white',
        drawerInactiveTintColor: 'white',
      }}
      drawerContent={props => {
        return <DrawerContent setProgress={setProgress} {...props} />;
      }}>
      <Drawer.Screen name="Screens">
        {props => <Screens {...props} style={animatedStyle} />}
      </Drawer.Screen>
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 5,
    // overflow: 'scroll',
    // borderWidth: 1,
  },
  drawerStyles: {
    flex: 1,
    width: '50%',
    backgroundColor: 'transparent',
  },
  drawerItem: {
    alignItems: 'flex-start',
    marginVertical: 0,
  },
  drawerLabel: {
    marginLeft: -16,
    fontFamily: 'ProductSans-regular',
    fontSize: 13,
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 60,
    marginBottom: 16,
    borderColor: 'white',
    borderWidth: StyleSheet.hairlineWidth,
  },
});

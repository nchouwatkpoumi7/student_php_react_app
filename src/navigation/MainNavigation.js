/* eslint-disable react-hooks/exhaustive-deps */
// Main React import
import React, {useEffect, useState} from 'react';
import SplashScreen from 'react-native-splash-screen';

// Navigation main modules
import {createNativeStackNavigator} from '@react-navigation/native-stack';

// React Native components import
import {View, ActivityIndicator, SafeAreaView} from 'react-native';

// Firebase modules import
import auth from '@react-native-firebase/auth';

// Redux modules and utils
import {useDispatch} from 'react-redux';
import {getCurrentUserAction} from '../redux/actions/CurrentUser';

// Navigation routes import
import DrawerNavigator from './../navigation/Drawer';

// Authentification screens import
import RegisterScreen from './../screens/Auth/RegisterScreen';
import AuthScreen from './../screens/Auth/AuthScreen';
import LoginScreen from './../screens/Auth/LoginScreen';
import CompleteAuthScreen from '../screens/Auth/CompleteAuthScreen';

// Other usefull components import
import StyledText from '../components/StyledText';
import {useTheme} from '@react-navigation/native';
import ForgotPasswordScreen from '../screens/Auth/ForgotPasswordScreen';

const MainNavigation = () => {
  const {colors} = useTheme();

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  const dispatch = useDispatch();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }

    if (user) {
      dispatch(getCurrentUserAction());
    } else {
      SplashScreen.hide();
    }
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    const subscriber2 = auth().onUserChanged(onAuthStateChanged);
    return () => {
      subscriber();
      subscriber2();
    };
  }, []);

  const AuthStack = createNativeStackNavigator();
  const CompleteAuthStack = createNativeStackNavigator();

  const AuthStackScreen = () => (
    <AuthStack.Navigator screenOptions={{headerShown: false}}>
      <AuthStack.Screen name="AuthScreen" component={AuthScreen} />
      <AuthStack.Screen name="RegisterScreen" component={RegisterScreen} />
      <AuthStack.Screen name="LoginScreen" component={LoginScreen} />
      <AuthStack.Screen
        name="ForgotPasswordScreen"
        component={ForgotPasswordScreen}
      />
    </AuthStack.Navigator>
  );

  const CompleteAuthStackScreen = () => (
    <CompleteAuthStack.Navigator screenOptions={{headerShown: false}}>
      <CompleteAuthStack.Screen
        name="CompleteAuthScreen"
        component={CompleteAuthScreen}
      />
    </CompleteAuthStack.Navigator>
  );

  if (initializing) {
    return (
      <View
        style={{
          backgroundColor: colors.primary,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <StyledText
          type="bold"
          style={{color: '#FFF', marginHorizontal: 10, fontSize: 30}}>
          MatCom Company
        </StyledText>
        <ActivityIndicator size={35} color="#FFF" />
      </View>
    );
  }

  if (user) {
    if (user.displayName) {
      return <DrawerNavigator />;
    }
    return <CompleteAuthStackScreen />;
  }

  return <AuthStackScreen />;
};

export default MainNavigation;

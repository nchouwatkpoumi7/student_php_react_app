import React, {useState, useEffect, Fragment} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import SplashScreen from 'react-native-splash-screen';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {Button, Icon} from 'native-base';
import {View, SafeAreaView, ActivityIndicator} from 'react-native';

import Tabbar from '../components/Tabbar';
import StyledText from '../components/StyledText';

import {getEntreprisesAction} from '../redux/actions/Entreprises';
import {getCartAction} from '../redux/actions/Cart';
import {getProductsCategoriesAction} from '../redux/actions/Products';
import {getLocationInfosAction} from '../redux/actions/LocationInfos';
import {getServicesAction} from '../redux/actions/Entreprises';

import HomeScreen from '../screens/HomeScreen';

import ShopScreen from '../screens/ShopScreen';
import ProductDetailsScreen from '../screens/Shop/ProductDetailsScreen';
import CheckoutPage from '../screens/Shop/CheckoutPage';
import CartScreen from '../screens/Shop/CartScreen';

import UpdateProfileScreen from '../screens/Settings/UpdateProfileScreen';

import EntrepriseDetailsScreen from '../screens/Entreprises/EntrepriseDetailsScreen';
import EntrepriseDeepDetailsScreen from '../screens/Entreprises/EntrepriseDeepDetailsScreen';
import EntrepriseListByTypeScreen from '../screens/Entreprises/EntreprisesListByTypeScreen';

import ServicesScreen from '../screens/ServicesScreen';

import ChatListScreen from '../screens/Chat/ChatListScreen';
import ChatRoomScreen from '../screens/Chat/ChatRoomScreen';
import ProductsByCategoriesScreen from '../screens/Shop/ProductsByCategoriesScreen';
import {getAppConfigsAction} from '../redux/actions/AppConfigs';
import {useTheme} from '@react-navigation/native';
import ProjectsScreen from '../screens/Projects/ProjectsScreen';
import ProjectDetailsScreen from '../screens/Projects/ProjectDetailsScreen';
import AboutScreen from '../screens/Settings/AboutScreen';

const Tab = createBottomTabNavigator();

const HomeStack = createNativeStackNavigator();
const ShopStack = createNativeStackNavigator();
const ChatStack = createNativeStackNavigator();
const ProjectsStack = createNativeStackNavigator();
const SettingsStack = createNativeStackNavigator();
const AboutStack = createNativeStackNavigator();

const HomeStackScreen = () => {
  return (
    <HomeStack.Navigator screenOptions={{headerShown: false}}>
      <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
      <HomeStack.Screen
        name="EntrepriseDetails"
        component={EntrepriseDetailsScreen}
      />
      <HomeStack.Screen
        name="EntrepriseDeepDetails"
        component={EntrepriseDeepDetailsScreen}
      />
      <HomeStack.Screen name="Services" component={ServicesScreen} />
      <HomeStack.Screen
        name="EntreprisesListByType"
        component={EntrepriseListByTypeScreen}
      />
    </HomeStack.Navigator>
  );
};

const ShopStackScreen = () => {
  return (
    <ShopStack.Navigator screenOptions={{headerShown: false}}>
      <ShopStack.Screen name="ShopScreen" component={ShopScreen} />
      <ShopStack.Screen
        name="ProductsByCategoriesScreen"
        component={ProductsByCategoriesScreen}
      />
      <ShopStack.Screen
        name="ProductDetailsScreen"
        component={ProductDetailsScreen}
      />
      <ShopStack.Screen name="CartScreen" component={CartScreen} />
      <ShopStack.Screen name="CheckoutScreen" component={CheckoutPage} />
    </ShopStack.Navigator>
  );
};

const ChatStackScreen = () => {
  return (
    <ChatStack.Navigator screenOptions={{headerShown: false}}>
      <ChatStack.Screen name="ChatList" component={ChatListScreen} />
      <ChatStack.Screen name="ChatRoom" component={ChatRoomScreen} />
    </ChatStack.Navigator>
  );
};

const ProjectsStackScreen = () => {
  return (
    <ProjectsStack.Navigator screenOptions={{headerShown: false}}>
      <ProjectsStack.Screen name="ProjectsScreen" component={ProjectsScreen} />
      <ProjectsStack.Screen
        name="ProjectDetailsScreen"
        component={ProjectDetailsScreen}
      />
    </ProjectsStack.Navigator>
  );
};

const SettingsStackScreen = () => {
  return (
    <SettingsStack.Navigator screenOptions={{headerShown: false}}>
      <SettingsStack.Screen
        name="SettingsScreen"
        component={UpdateProfileScreen}
      />
    </SettingsStack.Navigator>
  );
};

const AboutStackScreen = () => {
  return (
    <AboutStack.Navigator screenOptions={{headerShown: false}}>
      <AboutStack.Screen name="AboutScreen" component={AboutScreen} />
    </AboutStack.Navigator>
  );
};

const MainTabNavigation = () => {
  const dispatch = useDispatch();
  const {colors} = useTheme();

  const getCart = useSelector(state => state.cart);
  const getEntreprises = useSelector(state => state.entreprises);

  const [initializing, setInitializing] = useState(true);

  useEffect(() => {
    dispatch(getAppConfigsAction());
    dispatch(getLocationInfosAction());
    dispatch(getEntreprisesAction());
    dispatch(getProductsCategoriesAction());
    dispatch(getCartAction());
    dispatch(getServicesAction());

    return () => {};
  }, [dispatch]);

  if (
    !(getCart.loading === undefined || getCart.loading) &&
    // !(getAppConfigs.loading === undefined || getAppConfigs.loading) &&
    initializing
  ) {
    SplashScreen.hide();
    setInitializing(false);
  }

  if (initializing) {
    return (
      <View
        style={{
          backgroundColor: colors.primary,
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <StyledText
          type="bold"
          style={{color: '#FFF', marginHorizontal: 10, fontSize: 30}}>
          Walners Company
        </StyledText>
        <ActivityIndicator size={35} color="#FFF" />

        {getEntreprises.error && (
          <View>
            <StyledText
              type="bold"
              style={{color: '#FFF', marginHorizontal: 10, fontSize: 13}}>
              Veuillez verifier votre connexion a internet...
            </StyledText>
            <Button icon rounded>
              <Icon name="refresh" style={{fontSize: 25}} />
            </Button>
          </View>
        )}
      </View>
    );
  }

  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        lazy: false,
        tabBarStyle: {backgroundColor: 'black'},
      }}
      sceneContainerStyle={{
        backgroundColor: 'rgb(250, 250, 250)',
      }}
      tabBar={props => <Tabbar {...props} />}>
      <Tab.Screen name="HomeTab" component={HomeStackScreen} />
      <Tab.Screen name="ShopTab" component={ShopStackScreen} />
      <Tab.Screen
        options={{tabBarVisible: false}}
        name="MessagesTab"
        component={ChatStackScreen}
      />
      <Tab.Screen name="ProjectsTab" component={ProjectsStackScreen} />
      <Tab.Screen name="SettingsTab" component={SettingsStackScreen} />
      <Tab.Screen name="AboutTab" component={AboutStackScreen} />
    </Tab.Navigator>
  );
};

export default MainTabNavigation;

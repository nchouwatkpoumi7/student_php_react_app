/* eslint-disable react/prop-types */
import { useTheme } from '@react-navigation/native';
import React from 'react';
import { Text } from 'react-native';

/**
 *
 * @param {{style: StyleProp<TextStyle>, type: 'bold'|'medium'|'regular'|'italic'}} props
 * @returns
 */
const StyledText = (props) => {
	let font = 'ProductSans-Bold';
	if (props.type == 'regular') {
		font = 'ProductSans-Regular';
	} else if (props.type == 'medium') {
		font = 'ProductSans-Medium';
	} else if (props.type == 'italic') {
		font = 'ProductSans-Italic';
	}

	const { colors } = useTheme();

	if (props.style) {
		if (props.style.length == undefined) {
			return (
				<Text
					style={{
						fontSize: 13,
						color: colors.text,
						...props.style,
						fontFamily: font,
					}}
				>
					{props.children}
				</Text>
			);
		} else {
			return (
				<Text
					style={
						({ fontSize: 13, color: colors.text },
						[...props.style, { fontFamily: font }])
					}
				>
					{props.children}
				</Text>
			);
		}
	} else {
		return (
			<Text style={{ color: colors.text, fontFamily: font }}>
				{props.children}
			</Text>
		);
	}
};

export default StyledText;

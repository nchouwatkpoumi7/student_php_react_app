/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {
  Badge,
  Body,
  Left,
  ListItem,
  Right,
  Thumbnail,
  Text,
  Icon,
  View,
} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';
import DateHelpers from '../../helpers/DateHelpers';
import StyledText from '../StyledText';

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import {ChatProvider} from '../../services/ChatServices';

const ConversationItem = ({navigation, conversation}) => {
  const {colors} = useTheme();
  const [unreads, setUnreads] = React.useState(0);
  const [lastMessage, setLastMessage] = React.useState({});

  React.useEffect(() => {
    const unsubscribe = firestore()
      .collection('walcom-messages')
      .doc(conversation.id)
      .collection('messages')
      .onSnapshot(query => {
        ChatProvider.receiveAllMessages(query.docs, conversation);
        setUnreads(
          query.docs
            .map(doc => doc.data())
            .filter(
              msg =>
                msg.status !== 'seen' &&
                msg.author.id !== auth().currentUser.uid,
            ).length,
        );

        setLastMessage(
          query.docs
            .map(doc => ({id: doc.id, ...doc.data()}))
            .sort((a, b) => b.createdAt - a.createdAt)[0],
        );
      });
    return unsubscribe;
  }, [conversation]);

  return (
    <ListItem
      avatar
      onPress={() => {
        navigation.navigate('ChatRoom', {chat: conversation});
      }}>
      <Left style={{height: 60, justifyContent: 'center'}}>
        <Thumbnail
          style={{maxHeight: 60, maxWidth: 60}}
          source={{uri: conversation.image}}
        />
      </Left>
      <Body style={{height: 70, justifyContent: 'center'}}>
        <StyledText type="medium">{conversation.title}</StyledText>
        {conversation.last_message?.type === 'text' && (
          <View
            style={{maxHeight: 30, flexDirection: 'row', alignItems: 'center'}}>
            {conversation.last_message.author.id === auth().currentUser.uid && (
              <View style={{marginRight: 4}}>
                {lastMessage.status === 'seen' && (
                  <Icon
                    style={{fontSize: 18, color: colors.primary}}
                    type="MaterialIcons"
                    name="done-all"
                  />
                )}
                {lastMessage.status === 'delivered' && (
                  <Icon
                    style={{fontSize: 18, color: colors.text}}
                    type="MaterialIcons"
                    name="done-all"
                  />
                )}
                {lastMessage.status === 'sent' && (
                  <Icon
                    style={{fontSize: 18, color: colors.text}}
                    type="MaterialIcons"
                    name="done"
                  />
                )}
              </View>
            )}
            <Text note numberOfLines={1}>
              {conversation.last_message?.text}
            </Text>
          </View>
        )}
        {conversation.last_message?.type === 'image' && (
          <View
            style={{flexDirection: 'row', alignItems: 'center', paddingTop: 2}}>
            {conversation.last_message.author.id === auth().currentUser.uid && (
              <View style={{marginRight: 4}}>
                {lastMessage.status === 'seen' && (
                  <Icon
                    style={{fontSize: 18, color: colors.primary}}
                    type="MaterialIcons"
                    name="done-all"
                  />
                )}
                {lastMessage.status === 'delivered' && (
                  <Icon
                    style={{fontSize: 18, color: colors.text}}
                    type="MaterialIcons"
                    name="done-all"
                  />
                )}
                {lastMessage.status === 'sent' && (
                  <Icon
                    style={{fontSize: 18, color: colors.text}}
                    type="MaterialIcons"
                    name="done"
                  />
                )}
              </View>
            )}
            <Icon
              style={{fontSize: 20, opacity: 0.4}}
              type="MaterialIcons"
              name="image"
            />
          </View>
        )}
      </Body>
      <Right>
        <Text note>
          {DateHelpers.formatDate(conversation.last_message?.createdAt)}
        </Text>
        {unreads !== 0 && (
          <Badge
            primary
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 3,
              width: 24,
              height: 22,
            }}>
            <StyledText style={{color: colors.specialText, fontSize: 11}}>
              {unreads}
            </StyledText>
          </Badge>
        )}
      </Right>
    </ListItem>
  );
};

export default ConversationItem;

const styles = StyleSheet.create({});

/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';
import {useDispatch} from 'react-redux';
import {StyleSheet, Text, View, Alert} from 'react-native';
import {
  Card,
  CardItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Button,
} from 'native-base';

import StyledText from '../../StyledText';
import CurrencyHelpers from '../../../helpers/CurrencyHelpers';
import {withTheme} from '../../../helpers/hooks/withTheme';

class CartItem extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  add = () => {
    this.props.add_to_cart();
  };

  reduce = () => {
    if (this.props.item.quantity === 1) {
      Alert.alert(
        'Retirer du panier',
        'Voulez-vous vraiment retirer ' +
          this.props.item.title +
          ' du panier ?',
        [
          {
            text: 'Non',
            style: 'cancel',
          },
          {
            text: 'Oui',
            style: 'default',
            onPress: () => {
              this.props.remove_from_cart(this.props.item);
            },
          },
        ],
      );

      return;
    }

    this.props.reduce_from_cart();
  };

  render() {
    const {item, colors} = this.props;
    const factor = this.props.item.quantity;
    return (
      <Card noShadow style={{backgroundColor: '#FFF'}}>
        <CardItem style={{justifyContent: 'space-between'}}>
          <Left style={{flex: 2}}>
            <Thumbnail
              source={{
                uri: item.images[0],
              }}
            />
            <Body>
              <StyledText type="medium">{item.title}</StyledText>
              <StyledText type="regular" style={{fontSize: 11}}>
                {item.price} x {factor} ={' '}
                {CurrencyHelpers.format_amount(item.price * factor)} FCFA
              </StyledText>
            </Body>
          </Left>
          <Right
            style={{
              justifyContent: 'flex-end',
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Button
              onPress={this.reduce}
              style={{
                backgroundColor: colors.background,
                width: 40,
                height: 30,
                borderRadius: 8,
                justifyContent: 'center',
              }}>
              <StyledText style={{color: colors.text}}>-</StyledText>
            </Button>
            <StyledText
              style={{paddingHorizontal: 5, color: '#000', fontSize: 16}}>
              {factor}
            </StyledText>
            <Button
              onPress={this.add}
              style={{
                backgroundColor: colors.background,
                width: 40,
                height: 30,
                borderRadius: 8,
                justifyContent: 'center',
              }}>
              <StyledText style={{color: colors.text}}>+</StyledText>
            </Button>
          </Right>
        </CardItem>
      </Card>
    );
  }
}

export default withTheme(CartItem);

const styles = StyleSheet.create({});

/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';
import {View} from 'react-native';
import {Card, CardItem, Body, Left, Right, Icon} from 'native-base';

import {useTheme} from '@react-navigation/native';

// import { useSelector } from 'react-redux';
// import CartItem from '../../../components/Shop/Cart/CartItem';

import StyledText from '../../../components/StyledText';

const OrderHistoryItem = ({order}) => {
  const {colors} = useTheme();
  const format_date = datetime => {
    const months = [
      'Janvier',
      'Février',
      'Mars',
      'Avril',
      'Mai',
      'Juin',
      'Juillet',
      'Août',
      'Septembre',
      'Otobre',
      'Novembre',
      'Décembre',
    ];
    let current_datetime = new Date(datetime);
    let formatted_date =
      (current_datetime.getDate() > 9
        ? current_datetime.getDate()
        : '0' + current_datetime.getDate()) +
      ' ' +
      months[current_datetime.getMonth()] +
      ' ' +
      current_datetime.getFullYear();
    return formatted_date;
  };

  const StatusColor =
    order.status === undefined
      ? colors.primary
      : order.status
      ? colors.secondary
      : colors.danger;

  return (
    <Card>
      <CardItem>
        <Left style={{flex: 3, left: -12}}>
          <Body>
            <StyledText style={{fontSize: 11, color: StatusColor}}>
              COMMANDE #{order.id}
            </StyledText>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-start',
                left: -3,
              }}>
              {order.status === undefined ? (
                <>
                  <Icon
                    style={{fontSize: 18, paddingRight: 3, color: StatusColor}}
                    type="MaterialIcons"
                    name="autorenew"
                  />
                  <StyledText type="regular" style={{color: StatusColor}}>
                    en cours
                  </StyledText>
                </>
              ) : order.status === false ? (
                <>
                  <Icon
                    style={{
                      fontSize: 18,
                      paddingRight: 3,
                      color: StatusColor,
                    }}
                    type="MaterialIcons"
                    name="delete"
                  />
                  <StyledText type="regular" style={{color: StatusColor}}>
                    Rejétéé
                  </StyledText>
                </>
              ) : (
                <>
                  <Icon
                    style={{
                      fontSize: 18,
                      paddingRight: 3,
                      color: StatusColor,
                    }}
                    type="MaterialIcons"
                    name="check-circle"
                  />
                  <StyledText type="regular" style={{color: StatusColor}}>
                    Validée
                  </StyledText>
                </>
              )}
            </View>
          </Body>
        </Left>
        <Right style={{flex: 2, height: '100%'}}>
          <StyledText type="regular" style={{fontSize: 12}}>
            {format_date(order.created_at)}
          </StyledText>
        </Right>
      </CardItem>
      {order.products?.map((product, index) => (
        <CardItem
          key={index}
          style={{borderBottomWidth: 1, borderBottomColor: '#ddd'}}>
          <Left style={{flex: 3}}>
            <StyledText style={{fontSize: 13}}>{product.title}</StyledText>
          </Left>
          <Body style={{flex: 1}}>
            <StyledText type="regular" style={{fontSize: 11}}>
              {product.quantity} pcs
            </StyledText>
          </Body>
          <Right style={{flex: 2}}>
            <StyledText type="medium" style={{fontSize: 12}}>
              {product.price} FCFA
            </StyledText>
          </Right>
        </CardItem>
      ))}
      <CardItem
        style={{
          borderBottomWidth: 1,
          borderBottomColor: '#ddd',
          backgroundColor: StatusColor,
          paddingLeft: 2,
          paddingRight: 4,
        }}>
        <Left style={{alignItems: 'center', flex: 4}}>
          <Icon
            type="MaterialIcons"
            name="location-on"
            style={{color: colors.specialText}}
          />
          <StyledText style={{color: colors.specialText}}>
            {order.location}
          </StyledText>
        </Left>
        <Body
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <StyledText
            type="regular"
            style={{
              width: '100%',
              textAlign: 'right',
              color: colors.specialText,
            }}>
            Total
          </StyledText>
        </Body>
        <Right style={{flex: 2}}>
          <StyledText type="bold" style={{color: colors.specialText}}>
            {order.total_price} FCFA
          </StyledText>
        </Right>
      </CardItem>
    </Card>
  );
};

export default OrderHistoryItem;

/* eslint-disable react/prop-types */
import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import {Icon} from 'native-base';
import StyledText from '../StyledText';
import {useTheme} from '@react-navigation/native';

const CategoryCardIcon = ({navigation, category}) => {
  const {colors} = useTheme();
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={() => {
        navigation.navigate('ProductsByCategoriesScreen', {category});
      }}
      style={styles.card}>
      <Icon
        style={{fontSize: 22, marginRight: 5, opacity: 0.75}}
        type="MaterialIcons"
        name={category.icon}
      />
      <StyledText type="regular">{category.title}</StyledText>
    </TouchableOpacity>
  );
};

export default CategoryCardIcon;

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#FDFDFD',
    minWidth: 90,
    flex: 1,
    margin: 5,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 6,
    elevation: 3,
    borderRadius: 5,
  },
});

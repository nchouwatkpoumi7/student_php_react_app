/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import {Grid, Row, Col, Icon} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import StyledText from './../StyledText';
import {useTheme} from '@react-navigation/native';

import ImageView from 'react-native-image-viewing';
import FastImage from 'react-native-fast-image';

const ShopItemListCard = ({navigation, item}) => {
  const {colors} = useTheme();
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <>
      <ImageView
        images={item.images.map(img => ({uri: img}))}
        visible={modalOpen}
        onRequestClose={() => setModalOpen(false)}
        FooterComponent={() => (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <StyledText style={{color: colors.specialText}}>
              {item.title}
            </StyledText>
          </View>
        )}
      />
      <TouchableOpacity
        activeOpacity={0.9}
        style={[styles.grid, {backgroundColor: colors.card.background}]}
        onPress={() => {
          navigation.navigate('ProductDetailsScreen', {product: item});
        }}>
        <Grid>
          <Row>
            <Col size={4}>
              <TouchableOpacity
                style={{
                  padding: 10,
                  borderWidth: 0.2,
                  borderColor: '#aaa',
                  borderRadius: 4,
                }}
                onPress={() => setModalOpen(true)}>
                <FastImage
                  style={{
                    width: 70,
                    height: '100%',
                  }}
                  source={{
                    uri: item.images[0],
                    cache: FastImage.cacheControl.immutable,
                  }}
                />
              </TouchableOpacity>
            </Col>
            <Col size={7} style={{marginHorizontal: 8, marginTop: 5}}>
              <StyledText
                type="regular"
                style={{fontSize: 14, color: colors.card.text}}>
                {item.title}
              </StyledText>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 3,
                }}>
                <Icon
                  type="MaterialIcons"
                  name="bolt"
                  style={{color: colors.secondary, fontSize: 23}}
                />
                <StyledText
                  type="bold"
                  style={{fontSize: 15, color: colors.secondary}}>
                  {item.price} FCFA
                </StyledText>
              </View>
              <View>
                {item.quantity ? (
                  <StyledText
                    type="regular"
                    style={{
                      fontSize: 11,
                      textAlign: 'right',
                      color: colors.card.text,
                    }}>
                    {item.quantity} en stock
                  </StyledText>
                ) : (
                  <StyledText
                    type="medium"
                    style={{
                      fontSize: 11,
                      textAlign: 'right',
                      color: colors.danger,
                    }}>
                    stock épuisé
                  </StyledText>
                )}
              </View>
            </Col>
          </Row>
        </Grid>
      </TouchableOpacity>
    </>
  );
};

export default ShopItemListCard;

const styles = StyleSheet.create({
  grid: {
    height: 75,
    minWidth: 190,
    marginVertical: 5,
    marginHorizontal: 5,
    // borderWidth: 0.1,
    elevation: 2,
    backgroundColor: '#fefefe',
    borderColor: '#fefefe',
    borderRadius: 10,
  },
});

/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import {Grid, Row, Col, Icon, Toast} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, View, TouchableOpacity, ToastAndroid} from 'react-native';
import StyledText from '../StyledText';
import {useTheme} from '@react-navigation/native';

import ImageView from 'react-native-image-viewing';
import {useDispatch} from 'react-redux';
import {CartProvider} from '../../services/CartService';
import {getCartAction} from '../../redux/actions/Cart';
import FastImage from 'react-native-fast-image';

const ShopItemLongListCard = ({item, navigation}) => {
  const {colors} = useTheme();
  const [modalOpen, setModalOpen] = useState(false);

  const dispatch = useDispatch();

  const add_to_cart = () => {
    if (!item.quantity) {
      ToastAndroid.showWithGravity(
        'Le stock est épuisé pour ce produit',
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
      return;
    }
    CartProvider.add_to_cart(item).then(() => {
      dispatch(getCartAction());
      ToastAndroid.showWithGravity(
        'Article ajouté au panier !',
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
    });
  };

  return (
    <>
      <ImageView
        images={item.images.map(img => ({uri: img}))}
        visible={modalOpen}
        onRequestClose={() => setModalOpen(false)}
        FooterComponent={() => (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <StyledText style={{fontSize: 14, color: colors.specialText}}>
              {item.title}
            </StyledText>
          </View>
        )}
      />
      <TouchableOpacity
        activeOpacity={0.9}
        style={[styles.grid, {backgroundColor: colors.card.background}]}
        onPress={() => {
          navigation.navigate('ProductDetailsScreen', {product: item});
        }}>
        <Grid>
          <Row>
            <Col size={2}>
              <TouchableOpacity
                style={{
                  padding: 10,
                  borderWidth: 0.2,
                  borderColor: '#aaa',
                  borderRadius: 4,
                }}
                onPress={() => setModalOpen(true)}>
                <FastImage
                  style={{
                    width: 70,
                    height: '100%',
                  }}
                  source={{
                    uri: item.images[0],
                    cache: FastImage.cacheControl.immutable,
                  }}
                />
              </TouchableOpacity>
            </Col>
            <Col size={4} style={{marginHorizontal: 8, marginTop: 5}}>
              <StyledText
                type="regular"
                style={{fontSize: 15, color: colors.card.text}}>
                {item.title}
              </StyledText>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 3,
                }}>
                <Icon
                  type="MaterialIcons"
                  name="bolt"
                  style={{color: colors.gray, fontSize: 23}}
                />
                <StyledText
                  type="medium"
                  style={{fontSize: 15, color: colors.gray}}>
                  {item.price} FCFA
                </StyledText>
              </View>
              <View style={{marginTop: 6, flexDirection: 'row'}}>
                {[...new Array(5).fill('n')].map((t, index) => (
                  <Icon
                    style={{
                      fontSize: 15,
                      color: index < item.note + 1 ? '#ffc30f' : '#bbb',
                    }}
                    key={index}
                    type="MaterialIcons"
                    name="star"
                  />
                ))}
              </View>
              <View>
                {item.quantity ? (
                  <StyledText
                    type="regular"
                    style={{
                      fontSize: 11,
                      textAlign: 'right',
                      color: colors.card.text,
                    }}>
                    {item.quantity} en stock
                  </StyledText>
                ) : (
                  <StyledText
                    type="medium"
                    style={{
                      fontSize: 11,
                      textAlign: 'right',
                      color: colors.danger,
                    }}>
                    stock épuisé
                  </StyledText>
                )}
              </View>
            </Col>
            <Col size={1}>
              <TouchableOpacity
                onPress={add_to_cart}
                style={{
                  width: '100%',
                  height: '100%',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Icon
                  style={{
                    fontSize: 30,
                    color: colors.primary,
                  }}
                  type="MaterialIcons"
                  name="add-shopping-cart"
                />
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </TouchableOpacity>
    </>
  );
};

export default ShopItemLongListCard;

const styles = StyleSheet.create({
  grid: {
    height: 95,
    minWidth: 190,
    marginVertical: 5,
    elevation: 2,
    borderRadius: 10,
  },
});

/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable react/prop-types */
import { useTheme } from '@react-navigation/native';
import { Input, Item, Label, Icon } from 'native-base';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

const StyledInput = ({
	label,
	updatedValue,
	defaultValue,
	props = {},
	onChangeText = () => {},
	password = false,
	disabled = false,
	textarea = false,
}) => {
	const [focused, setFocused] = useState(defaultValue ? true : false);
	const [passwordVisible, setPasswordVisible] = useState(false);
	const [value, setValue] = useState(
		defaultValue ? defaultValue.toString() : ''
	);
	const { colors } = useTheme();

	useEffect(() => {
		if (updatedValue) {
			setValue(updatedValue.toString());
		}
		return () => {};
	}, [updatedValue]);

	return (
		<View>
			<Item floatingLabel style={styles.item}>
				<Label
					style={
						focused
							? {
								zIndex: 9999999999,
								alignSelf: 'flex-start',
								backgroundColor: '#FFF',
								left: 18,
								top: 2,
								color: colors.primary,
								fontWeight: 'bold',
							  }
							: { fontSize: 13, left: 8 }
					}
				>
					{label}
				</Label>
				<Input
					{...props}
					disabled={disabled}
					value={value}
					multiline={textarea}
					onChangeText={(text) => {
						onChangeText(text);
						setValue(text);
					}}
					secureTextEntry={password ? !passwordVisible : false}
					onFocus={() => {
						setFocused(true);
					}}
					onBlur={() => {
						if (!value) {
							setFocused(false);
						}
					}}
					style={{
						height: textarea ? 80 : 50,
						zIndex: focused ? -1 : 1,
						borderWidth: 1.5,
						borderColor: focused ? colors.placeholder : '#000',
						borderRadius: 10,
						color: colors.primary,
						fontWeight: 'bold',
						fontSize: 15,
					}}
				/>
			</Item>

			{password && (
				<TouchableOpacity
					onPress={() => setPasswordVisible((visible) => !visible)}
					style={{ right: 10, top: '50%', position: 'absolute' }}
				>
					<Icon
						type="MaterialIcons"
						name={!passwordVisible ? 'visibility-off' : 'visibility'}
						style={{ color: colors.primary }}
					/>
				</TouchableOpacity>
			)}
		</View>
	);
};

export default StyledInput;

const styles = StyleSheet.create({
	item: { borderBottomWidth: 0, marginVertical: 5 },
	label_focused: {},
	label: {},
});

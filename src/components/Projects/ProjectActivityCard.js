/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {View} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, ToastAndroid, TouchableOpacity} from 'react-native';
import StyledText from '../StyledText';
import DateHelpers from './../../helpers/DateHelpers';

import ImageView from 'react-native-image-viewing';

const ProjectActivityCard = ({activity, navigation}) => {
  const {colors} = useTheme();
  const [modalOpen, setModalOpen] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);

  let right_color = colors.primary;
  if (activity.status === 'completed') {
    right_color = colors.ios;
  }

  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      {activity.images?.length && (
        <ImageView
          swipeToCloseEnabled
          imageIndex={imageIndex}
          onImageIndexChange={setImageIndex}
          images={activity.images?.map(img => ({uri: img.image}))}
          visible={modalOpen}
          onRequestClose={() => setModalOpen(false)}
          FooterComponent={() => (
            <View
              style={{
                height: 100,
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <StyledText style={{color: colors.specialText}}>
                {activity.images[imageIndex].title}
              </StyledText>
            </View>
          )}
        />
      )}
      <View style={{flex: 1}}>
        <StyledText
          type="regular"
          style={{textAlign: 'center', paddingRight: 8}}>
          {DateHelpers.timeAgo(activity.updated_at)}
        </StyledText>
      </View>
      <TouchableOpacity
        onPress={() => {
          if (activity.images.length) {
            setModalOpen(true);
          } else {
            ToastAndroid.showWithGravity(
              'Aucune image disponible !',
              ToastAndroid.LONG,
              ToastAndroid.CENTER,
            );
          }
        }}
        style={[styles.card, {backgroundColor: colors.card.background}]}>
        <View style={styles.card_content}>
          <View style={styles.details_section}>
            <StyledText type="medium" style={{fontSize: 16}}>
              {activity.title}
            </StyledText>
            <View style={{marginTop: 3}}>
              <StyledText
                type="regular"
                style={{
                  color: colors.gray,
                  fontSize: 13,
                }}>
                {activity.description}
              </StyledText>
            </View>
          </View>
        </View>
        <View style={[styles.right_border, {backgroundColor: right_color}]} />
      </TouchableOpacity>
    </View>
  );
};

export default ProjectActivityCard;

const styles = StyleSheet.create({
  card: {
    flex: 6,
    marginTop: 15,
    flexDirection: 'row',
    elevation: 3,
    borderRadius: 10,
  },
  right_border: {
    width: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  card_content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 10,
    paddingVertical: 15,
  },
  details_section: {
    flex: 5,
    paddingRight: 8,
  },
});

/* eslint-disable react/prop-types */
import { useTheme } from '@react-navigation/native';

import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import StyledText from '../StyledText';

const StatusFilterButton = ({ status, setCurrentStatus, navigation }) => {
	const { colors } = useTheme();

	return (
		<TouchableOpacity
			key={status.key}
			onPress={() => setCurrentStatus(status.key)}
			style={[
				styles.button,
				{
					backgroundColor: status.active ? colors.primary : colors.background,
					borderColor: colors.primary,
				},
			]}
		>
			<StyledText
				type={status.active ? 'medium' : 'medium'}
				style={{
					color: status.active ? colors.specialText : colors.primary,
				}}
			>
				{status.title}
			</StyledText>
		</TouchableOpacity>
	);
};

export default StatusFilterButton;

const styles = StyleSheet.create({
	button: {
		paddingHorizontal: 10,
		paddingVertical: 5,
		borderRadius: 6,
		marginRight: 10,
		borderWidth: 1,
	},
});

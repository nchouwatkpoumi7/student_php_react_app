/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import Svg, {G, Circle} from 'react-native-svg';
import StyledText from './../StyledText';

const ajoute_zero = n => (n > 9 ? n : '0' + n);

const ProjectsStatusChart = ({
  label = 'projet',
  label_completed = 'achevé',
  completed,
  active,
  label_active = 'actif',
  idle,
  label_idle = 'en attente',
  type = 'global',
  progression = 0,
}) => {
  const {colors} = useTheme();
  const radius = 50;
  const circleCircumference = 2 * Math.PI * radius;

  const total = completed + active + idle;

  const completedPercentage = (completed / total) * 100;
  const activePercentage = (active / total) * 100;
  const idlePercentage = (idle / total) * 100;

  const completedStrokeDashoffset =
    circleCircumference - (circleCircumference * completedPercentage) / 100;
  const activeStrokeDashoffset =
    circleCircumference - (circleCircumference * activePercentage) / 100;
  const regularStrokeDashoffset =
    circleCircumference - (circleCircumference * idlePercentage) / 100;

  const completedAngle = (completed / total) * 360;
  const activeAngle = (active / total) * 360;
  const regularAngle = completedAngle + activeAngle;

  const Legends = [
    {
      color: colors.ios,
      percentage: completedPercentage,
      text: `${ajoute_zero(completed)} ${label}${
        completed === 1 ? '' : 's'
      } ${label_completed}${completed === 1 ? '' : 's'}`,
    },
    {
      color: colors.primary,
      percentage: activePercentage,
      text: `${ajoute_zero(active)} ${label}${
        active === 1 ? '' : 's'
      } ${label_active}${active === 1 ? '' : 's'}`,
    },
    {
      color: colors.gray_light,
      percentage: idlePercentage,
      text: `${ajoute_zero(idle)} ${label}${
        idle === 1 ? '' : 's'
      } ${label_idle}${idle === 1 ? '' : 's'}`,
    },
  ];

  return (
    <View style={{flexDirection: 'row', alignItems: 'center'}}>
      <View style={styles.graphWrapper}>
        <Svg height="160" width="160" viewBox="0 0 180 180">
          <G rotation={0} originX="0" originY="0">
            {total === 0 ? (
              <Circle
                cx="50%"
                cy="50%"
                r={radius}
                stroke="#F1F6F9"
                fill="transparent"
                strokeWidth="14"
              />
            ) : (
              <>
                <Circle
                  cx="50%"
                  cy="50%"
                  r={radius}
                  stroke={colors.ios}
                  fill="transparent"
                  strokeWidth="14"
                  strokeDasharray={circleCircumference}
                  strokeDashoffset={completedStrokeDashoffset}
                  rotation={0}
                  originX="90"
                  originY="90"
                  strokeLinecap="round"
                />
                <Circle
                  cx="50%"
                  cy="50%"
                  r={radius}
                  stroke={colors.primary}
                  fill="transparent"
                  strokeWidth="14"
                  strokeDasharray={circleCircumference}
                  strokeDashoffset={activeStrokeDashoffset}
                  rotation={completedAngle}
                  originX="90"
                  originY="90"
                  strokeLinecap="round"
                />
                <Circle
                  cx="50%"
                  cy="50%"
                  r={radius}
                  stroke={colors.gray_light}
                  fill="transparent"
                  strokeWidth="14"
                  strokeDasharray={circleCircumference}
                  strokeDashoffset={regularStrokeDashoffset}
                  rotation={regularAngle}
                  originX="90"
                  originY="90"
                  strokeLinecap="round"
                />
              </>
            )}
          </G>
        </Svg>
        {type === 'global' ? (
          <View style={[styles.inner_circle, {borderColor: colors.info}]}>
            <StyledText style={{fontSize: 17}}>
              {total > 9 ? total : '0' + total}
            </StyledText>
            <StyledText type="regular" style={{fontSize: 12}}>
              Projets
            </StyledText>
          </View>
        ) : (
          <View style={[styles.inner_circle, {borderColor: colors.info}]}>
            <StyledText style={{fontSize: 17}}>
              {progression > 9 ? progression : '0' + progression}%
            </StyledText>
            <StyledText type="regular" style={{fontSize: 12}}>
              terminé
            </StyledText>
          </View>
        )}
      </View>
      <View>
        {Legends.map((legend, index) => (
          <View
            key={index}
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginBottom: 8,
              display: legend.percentage ? 'flex' : 'none',
            }}>
            <View
              style={{
                width: 15,
                height: 15,
                borderRadius: 3,
                backgroundColor: legend.color,
                marginRight: 8,
              }}
            />
            <StyledText
              type="regular"
              style={{color: colors.gray, opacity: 0.8}}>
              {legend.text}
            </StyledText>
          </View>
        ))}
      </View>
    </View>
  );
};

export default ProjectsStatusChart;

const styles = StyleSheet.create({
  graphWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  inner_circle: {
    borderWidth: 1,
    borderColor: 'red',
    position: 'absolute',
    width: 68,
    height: 68,
    borderRadius: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

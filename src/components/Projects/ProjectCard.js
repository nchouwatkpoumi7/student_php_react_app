/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {Icon, View} from 'native-base';
import React from 'react';
import {StyleSheet, Touchable, TouchableOpacity} from 'react-native';
import StyledText from '../StyledText';

const ProjectCard = ({project, navigation}) => {
  const {colors} = useTheme();

  let left_color = colors.primary;
  if (project.status === 'completed') {
    left_color = colors.ios;
  } else if (project.status === 'idle') {
    left_color = colors.gray_light;
  }

  function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }
  function formatDate(date) {
    return [
      padTo2Digits(date.getDate()),
      padTo2Digits(date.getMonth() + 1),
      date.getFullYear(),
    ].join('/');
  }

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('EntrepriseDetails', {project: project});
      }}
      style={[styles.card, {backgroundColor: colors.card.background}]}>
      <View style={[styles.left_border, {backgroundColor: left_color}]} />
      <View style={styles.card_content}>
        <View style={styles.icon_container}>
          <View
            style={[styles.icon_parent, {backgroundColor: left_color + '66'}]}>
            <Icon
              style={{
                color: project.status === 'idle' ? colors.gray : colors.primary,
              }}
              type="MaterialIcons"
              name={project.entreprise?.category.icon}
            />
          </View>
        </View>
        <View style={styles.details_section}>
          <StyledText type="medium">{project.title}</StyledText>
          <StyledText type="regular" style={{color: colors.gray, fontSize: 12}}>
            {project.location}
          </StyledText>
          <StyledText type="regular" style={{color: colors.gray, fontSize: 12}}>
            {formatDate(new Date())}
          </StyledText>
          <View style={{marginTop: 12}}>
            <StyledText
              type="regular"
              style={{
                paddingBottom: 2,
                color: colors.gray,
                fontSize: 12,
              }}>
              {project.status !== 'idle' ? 'Progression ' : 'En attente '}
            </StyledText>
            <StyledText
              type="regular"
              style={{
                paddingBottom: 2,
                color: colors.gray,
                fontSize: 12,
              }}>
              {project.text && project.text.substring(0, 50) + '...'}
            </StyledText>
            {project.status !== 'idle' && (
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {/* <View style={{flex: 1, marginRight: 10}}>
                  <View
                    style={[
                      styles.progress_back,
                      {
                        backgroundColor: colors.gray_light,
                      },
                    ]}
                  />
                  <View
                    style={[
                      styles.progress_front,
                      {
                        backgroundColor:
                          project.status === 'completed'
                            ? colors.ios
                            : left_color,
                        width: project.progression + '%',
                      },
                    ]}
                  />
                </View>
                <StyledText
                  type="medium"
                  style={{
                    color:
                      project.status === 'completed' ? colors.ios : colors.text,
                  }}>
                  {project.progression}%
                </StyledText> */}
              </View>
            )}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default ProjectCard;

const styles = StyleSheet.create({
  card: {
    marginTop: 15,
    flexDirection: 'row',
    elevation: 3,
    borderRadius: 10,
  },
  left_border: {
    height: '100%',
    width: 10,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  card_content: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 10,
    paddingVertical: 15,
  },
  icon_container: {
    marginRight: 10,
    flex: 1,
  },
  icon_parent: {
    width: 45,
    height: 45,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  details_section: {
    flex: 5,
    paddingRight: 40,
  },
  progress_back: {
    width: '100%',
    height: 7,
    borderRadius: 1,
  },
  progress_front: {
    position: 'absolute',
    height: 7,
    borderRadius: 1,
  },
});

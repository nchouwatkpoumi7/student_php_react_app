/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useTheme } from '@react-navigation/native';
import { Input, Item, Label, Icon } from 'native-base';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import { StyleSheet, View, TouchableOpacity } from 'react-native';

const CustomStyledInput = ({
	label,
	updatedValue,
	defaultValue,
	props = {},
	onChangeText = () => {},
	password = false,
	// disabled = false,
	placeholder = '',
	textarea = false,
}) => {
	const [focused, setFocused] = useState(defaultValue ? true : false);
	const [passwordVisible, setPasswordVisible] = useState(false);
	const [value, setValue] = useState(
		defaultValue ? defaultValue.toString() : ''
	);

	const { colors } = useTheme();

	useEffect(() => {
		if (updatedValue) {
			setValue(updatedValue.toString());
		}
		return () => {};
	}, [updatedValue]);

	return (
		<FloatingLabelInput
			{...props}
			onFocus={() => {
				setFocused(true);
			}}
			onBlur={() => {
				if (!value) {
					setFocused(false);
				}
			}}
			isPassword={password}
			togglePassword={passwordVisible}
			onTogglePassword={(show) => setPasswordVisible(show)}
			label={label}
			value={value}
			multiline={textarea}
			staticLabel
			hintTextColor={'#aaa'}
			hint={placeholder}
			containerStyles={{
				borderWidth: 1,
				paddingHorizontal: 10,
				backgroundColor: '#fff',
				borderColor: colors.gray,
				borderRadius: 8,
			}}
			customLabelStyles={{
				colorFocused: colors.primary,
				colorBlurred: colors.gray,
				fontSizeFocused: 12,
				fontSizeBlurred: 12,
			}}
			labelStyles={{
				backgroundColor: '#fff',
				paddingHorizontal: 5,
				fontFamily: 'ProductSans-Regular',
			}}
			inputStyles={{
				color: colors.gray,
				paddingHorizontal: 10,
				fontFamily: 'ProductSans-Bold',
			}}
			onChangeText={(value) => {
				onChangeText(value);
				setValue(value);
			}}
		/>
	);
};

export default CustomStyledInput;

const styles = StyleSheet.create({
	item: { borderBottomWidth: 0, marginVertical: 5 },
	label_focused: {},
	label: {},
});

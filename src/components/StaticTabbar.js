import * as React from 'react';

import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Animated,
  Dimensions,
} from 'react-native';

import {Icon} from 'native-base';

const {width} = Dimensions.get('window');

// interface Tab {
//   name: string;
// }

// interface StaticTabbarProps {
//   tabs: Tab[];
//   value: Animated.Value;
//   state;
//   navigation;
// }

// export default class StaticTabbar extends React.PureComponent<StaticTabbarProps> {
export default class StaticTabbar extends React.PureComponent {
  values = [];
  the_state = {index: 0};
  state = {
    currentIndex: 0,
  };

  constructor(props) {
    super(props);
    const {tabs, navigation} = this.props;
    this.values = tabs.map(
      (tab, index) => new Animated.Value(index === 0 ? 1 : 0),
    );
  }

  componentDidMount() {
    const {tabs, navigation} = this.props;
    this.unsubscribe = navigation.addListener('state', () => {
      const state = navigation.getState();

      if (state.index < 4 && this.state.currentIndex !== state.index) {
        this.onPress(state.index);
        this.setState({currentIndex: state.index});
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  onPress = index => {
    const {value, tabs, navigation} = this.props;

    navigation.navigate(tabs[index].route);

    const tabWidth = width / tabs.length;
    Animated.sequence([
      Animated.parallel(
        this.values.map(v =>
          Animated.timing(v, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
          }),
        ),
      ),
      Animated.parallel([
        Animated.spring(value, {
          toValue: tabWidth * index,
          useNativeDriver: true,
        }),
        Animated.spring(this.values[index], {
          toValue: 1,
          useNativeDriver: true,
        }),
      ]),
    ]).start();
  };

  render() {
    const {onPress} = this;
    const {tabs, value} = this.props;
    return (
      <View style={styles.container}>
        {tabs.map((tab, key) => {
          const tabWidth = width / tabs.length;
          const cursor = tabWidth * key;
          const opacity = value.interpolate({
            inputRange: [cursor - tabWidth, cursor, cursor + tabWidth],
            outputRange: [1, 0, 1],
            extrapolate: 'clamp',
          });
          const translateY = this.values[key].interpolate({
            inputRange: [0, 1],
            outputRange: [64, 0],
            extrapolate: 'clamp',
          });
          const opacity1 = this.values[key].interpolate({
            inputRange: [0, 1],
            outputRange: [0, 1],
            extrapolate: 'clamp',
          });
          return (
            <React.Fragment {...{key}}>
              <TouchableWithoutFeedback onPress={() => onPress(key)}>
                <Animated.View style={[styles.tab, {opacity}]}>
                  <Icon
                    type="MaterialIcons"
                    name={tab.name}
                    style={{color: '#FFF', fontSize: 25}}
                  />
                </Animated.View>
              </TouchableWithoutFeedback>
              <Animated.View
                style={{
                  position: 'absolute',
                  top: -8,
                  left: tabWidth * key,
                  width: tabWidth,
                  height: 64,
                  justifyContent: 'center',
                  alignItems: 'center',
                  opacity: opacity1,
                  transform: [{translateY}],
                }}>
                <View style={styles.activeIcon}>
                  <Icon
                    type="MaterialIcons"
                    name={tab.name}
                    style={{color: "#ff6600", fontSize: 28}}
                    color="white"
                  />
                </View>
              </Animated.View>
            </React.Fragment>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '98%',
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 64,
  },
  activeIcon: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

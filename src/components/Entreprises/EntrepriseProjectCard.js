/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {View, Button, Icon} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';
import StyledText from '../StyledText';

import ImageView from 'react-native-image-viewing';

const EntrepriseProjectCard = ({project, navigation}) => {
  const {colors} = useTheme();
  const [modalOpen, setModalOpen] = React.useState(false);

  const [imageIndex, setImageIndex] = React.useState(0);

  let left_color = colors.primary;

  return (
    <>
      {project.images?.length && (
        <ImageView
          swipeToCloseEnabled
          imageIndex={imageIndex}
          onImageIndexChange={setImageIndex}
          images={project.images?.map(img => ({uri: img.image}))}
          visible={modalOpen}
          onRequestClose={() => setModalOpen(false)}
          FooterComponent={() => (
            <View
              style={{
                height: 100,
                flex: 1,
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <StyledText style={{color: colors.specialText}}>
                {project.images[imageIndex].title}
              </StyledText>
            </View>
          )}
        />
      )}

      <View style={[styles.card, {backgroundColor: colors.card.background}]}>
        <View
          style={[styles.left_border, {backgroundColor: left_color}]}></View>
        <View style={styles.card_content}>
          <View style={styles.details_section}>
            <StyledText type="medium" style={{fontSize: 16}}>
              {project.title}
            </StyledText>
            <StyledText
              type="regular"
              style={{color: colors.gray, fontSize: 12}}>
              {project.location}
            </StyledText>
            <StyledText
              type="regular"
              style={{paddingTop: 8, color: colors.gray, fontSize: 12}}>
              Client : {project.client}
            </StyledText>
          </View>
        </View>

        <Button
          rounded
          icon
          bordered
          style={{
            backgroundColor: colors.background,
            alignSelf: 'center',
            marginRight: 10,
          }}
          onPress={() => setModalOpen(true)}>
          <Icon type="MaterialIcons" name="visibility" />
        </Button>
      </View>
    </>
  );
};

export default EntrepriseProjectCard;

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
    elevation: 3,
    borderRadius: 10,
  },
  left_border: {
    height: '100%',
    width: 10,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  card_content: {
    flex: 1,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginLeft: 10,
    paddingVertical: 12,
  },
  details_section: {
    flex: 5,
    paddingRight: 40,
  },
});

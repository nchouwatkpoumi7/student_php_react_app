import React from 'react';
import {useTheme} from '@react-navigation/native';
import {Icon, View} from 'native-base';
import {StyleSheet, TouchableOpacity} from 'react-native';

import StyledText from '../StyledText';

const ServiceItem = ({service, navigation}) => {
  const {colors} = useTheme();

  return (
    <View>
      <TouchableOpacity
        activeOpacity={0.7}
        style={[
          styles.button,
          {
            backgroundColor: colors.primary,
          },
        ]}
        onPress={() => {
          navigation.navigate('EntreprisesListByType', {
            service,
          });
        }}>
        <Icon type="MaterialIcons" name={service.icon} style={styles.icon} />
      </TouchableOpacity>
      <StyledText type="regular" style={styles.title}>
        {service.title}
      </StyledText>
    </View>
  );
};

export default ServiceItem;

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#00f',
    elevation: 6,
    height: 50,
    width: 50,
    borderRadius: 25,
    marginHorizontal: 17,
    marginVertical: 8,
  },
  icon: {
    fontSize: 25,
    color: '#FFF',
  },
  title: {
    fontSize: 12,
    textAlign: 'center',
  },
});

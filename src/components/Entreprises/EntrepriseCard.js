/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import {Grid, Row, Col, Icon} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, Image, View, TouchableOpacity} from 'react-native';
import StyledText from './../StyledText';
import {useTheme} from '@react-navigation/native';

import ImageView from 'react-native-image-viewing';

const EntrepriseCard = ({navigation, entreprise}) => {
  const {colors} = useTheme();
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <>
      <ImageView
        images={[{uri: entreprise.image}]}
        visible={modalOpen}
        onRequestClose={() => setModalOpen(false)}
        FooterComponent={() => (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <StyledText style={{color: colors.specialText}}>
              {entreprise.title}
            </StyledText>
          </View>
        )}
      />
      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => navigation.navigate('EntrepriseDetails', {entreprise})}>
        <Grid style={[styles.grid, {backgroundColor: colors.card.background}]}>
          <Row>
            <Col size={4}>
              <TouchableOpacity onPress={() => setModalOpen(true)}>
                <Image
                  style={{
                    width: '100%',
                    height: '100%',
                    borderRadius: 10,
                    borderWidth: 0.7,
                    borderColor: '#aaa',
                  }}
                  source={{uri: entreprise.image}}
                />
              </TouchableOpacity>
            </Col>
            <Col size={7} style={{marginHorizontal: 8, marginTop: 5}}>
              <StyledText
                type="medium"
                style={{fontSize: 15, color: colors.card.text}}>
                {entreprise.title}
              </StyledText>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 3,
                }}>
                <Icon
                  type="MaterialIcons"
                  name="bolt"
                  style={{color: colors.gray, fontSize: 23}}
                />
                <StyledText type="regular" style={{color: colors.gray}}>
                  {entreprise.promotion.status
                    ? entreprise.promotion.message
                    : entreprise.category.title}
                </StyledText>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  marginTop: 7,
                  marginHorizontal: 2,
                  justifyContent: 'space-between',
                }}>
                <View style={{alignItems: 'center'}}>
                  <StyledText type="regular" style={{fontSize: 11}}>
                    Note
                  </StyledText>
                  <StyledText type="medium">{entreprise.note}</StyledText>
                </View>
                <View style={{alignItems: 'center'}}>
                  <StyledText type="regular" style={{fontSize: 11}}>
                    Projects
                  </StyledText>
                  <StyledText type="medium">
                    {entreprise.projects.number || 0}
                  </StyledText>
                </View>
                <View style={{alignItems: 'center'}}>
                  <StyledText type="regular" style={{fontSize: 11}}>
                    Tarifs
                  </StyledText>
                  <StyledText type="medium">N/A</StyledText>
                </View>
              </View>
            </Col>
          </Row>
        </Grid>
      </TouchableOpacity>
    </>
  );
};

export default EntrepriseCard;

const styles = StyleSheet.create({
  grid: {
    height: 100,
    marginVertical: 5,
    marginHorizontal: 5,
    borderWidth: 0.8,
    borderColor: '#ccc',
    borderRadius: 10,
  },
});

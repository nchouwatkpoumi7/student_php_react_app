/* eslint-disable react/prop-types */
/* eslint-disable no-undef */
import {Icon, ListItem, Left, Text, Right} from 'native-base';
import React from 'react';
import {StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';
import StyledText from '../StyledText';

const EntrepriseCardSearch = ({navigation, entreprise}) => {
  const {colors} = useTheme();

  return (
    <ListItem
      onPress={() => navigation.navigate('EntrepriseDetails', {entreprise})}>
      <Left>
        <StyledText type="medium" style={{color: colors.specialText}}>
          {entreprise.title}
        </StyledText>
      </Left>
      <Right>
        <Icon type="MaterialIcons" name="arrow-forward" />
      </Right>
    </ListItem>
  );
};

export default EntrepriseCardSearch;

const styles = StyleSheet.create({
  grid: {
    height: 100,
    marginVertical: 5,
    marginHorizontal: 5,
    borderWidth: 0.8,
    borderColor: '#ccc',
    borderRadius: 10,
  },
});

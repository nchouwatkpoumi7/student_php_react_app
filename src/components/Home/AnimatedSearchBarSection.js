/* eslint-disable react-native/no-inline-styles */
import React, {useRef} from 'react';
import {useFocusEffect} from '@react-navigation/core';
import {useTheme} from '@react-navigation/native';
import {Icon} from 'native-base';
import {
  BackHandler,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import * as Animatable from 'react-native-animatable';
import {useSelector} from 'react-redux';

const AnimatedSearchBarSection = ({
  setshowSettings,
  setEntreprisesToDisplay,
  setTypesEntreprisesToDisplay,
  setSearchText,
  searchText,
  settingActive,
  setSettingActive,
  settingOpen,
  setSettingOpen,
  Services,
}) => {
  const {colors} = useTheme();
  const inputRef = useRef(null);

  useFocusEffect(
    React.useCallback(() => {
      const backEvent = BackHandler.addEventListener(
        'hardwareBackPress',
        () => {
          if (searchText) {
            setSearchText('');
            inputRef?.current.clear();
            return true;
          } else if (settingActive) {
            setSettingOpen(false);
            return true;
          }

          return false;
        },
      );

      return () => {
        backEvent.remove();
      };
    }, [searchText, settingActive, setSearchText, setSettingOpen]),
  );

  const getEntreprises = useSelector(state => state.entreprises);
  const {entreprises = []} = getEntreprises;

  return (
    <Animatable.View
      animation={
        settingOpen
          ? {
              from: {
                bottom: -63,
              },
              to: {
                bottom: -20,
              },
            }
          : {
              from: {
                bottom: 70,
              },
              to: {
                bottom: 25,
              },
            }
      }
      style={[styles.searchContainer, {zIndex: 555555555}]}>
      <TextInput
        ref={inputRef}
        onChangeText={value => {
          setSearchText(value);

          if (!settingOpen) {
            setTypesEntreprisesToDisplay(
              Services.filter(
                service =>
                  service.title.toLowerCase().indexOf(value.toLowerCase()) !==
                  -1,
              ),
            );
          } else {
            setEntreprisesToDisplay(
              entreprises.filter(
                entreprise =>
                  entreprise.title
                    .toLowerCase()
                    .indexOf(value.toLowerCase()) !== -1,
              ),
            );
          }
        }}
        placeholder={
          !settingOpen
            ? 'Recherchez votre service '
            : 'Rechercher une entreprise'
        }
        style={{paddingHorizontal: 40, fontSize: 12, color: colors.text}}
      />

      <Icon
        type="MaterialIcons"
        name="search"
        style={[styles.searchIcon, {left: 8}]}
      />
      <TouchableOpacity
        onPress={() => {
          setSearchText('');
          setEntreprisesToDisplay([]);
          if (settingActive) {
            setSettingOpen(false);
            setshowSettings(false);
          } else {
            setSettingActive(true);
            setSettingOpen(true);
          }
        }}
        style={[styles.searchIcon, {right: 0, paddingHorizontal: 10}]}>
        <Icon type="MaterialIcons" name="apps" />
      </TouchableOpacity>
    </Animatable.View>
  );
};

export default AnimatedSearchBarSection;

const styles = StyleSheet.create({
  searchContainer: {
    marginHorizontal: 20,
    bottom: 27,
    backgroundColor: '#FFF',
    borderRadius: 5,
    elevation: 2,
  },
  searchIcon: {
    opacity: 0.4,
    position: 'absolute',
    top: '20%',
  },
});

/* eslint-disable no-undef */
/* eslint-disable no-unused-vars */
/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable react/prop-types */
import React, {useRef} from 'react';
import {useTheme} from '@react-navigation/native';
import {Header, View} from 'native-base';

import StyledText from '../StyledText';
import {useSelector} from 'react-redux';

import {LocationLoader} from './../../helpers/skeleton-loader/LocationLoader';

const LocationInfosSection = () => {
  const {colors} = useTheme();

  const getLocationInfos = useSelector(state => state.locationInfos);
  const {locationInfos = {infos: {street: '', adminArea5: ''}}} =
    getLocationInfos;

  return (
    <Header
      searchBar
      androidStatusBarColor={colors.primary}
      style={{height: 100, backgroundColor: colors.primary}}>
      <View style={{flex: 1, paddingTop: 20}}>
        <StyledText
          type="regular"
          style={{fontSize: 12, color: colors.specialText}}>
          Votre position
        </StyledText>
        {getLocationInfos.loading ? (
          <LocationLoader />
        ) : (
          <StyledText
            type="bold"
            style={{fontSize: 15, color: colors.specialText}}>
            {locationInfos.infos?.street}, {locationInfos.infos?.adminArea5},{' '}
            {locationInfos.infos?.adminArea3}
          </StyledText>
        )}
      </View>
    </Header>
  );
};

export default LocationInfosSection;

import React, {Fragment} from 'react';
import {useTheme} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import {List, View} from 'native-base';
import {ScrollView, StyleSheet, Dimensions} from 'react-native';

import * as Animatable from 'react-native-animatable';

import EntrepriseCardSearch from '../Entreprises/EntrepriseCardSearch';
import StyledText from '../StyledText';

const AnimatedOverlayContainer = ({
  setSettingActive,
  settingOpen,
  showSettings,
  setshowSettings,
  EntreprisesToDisplay,
  searchText,
  navigation,
}) => {
  const {colors} = useTheme();

  const [screenDimensions, setScreenDimensions] = React.useState({
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  });

  useFocusEffect(
    React.useCallback(() => {
      const DimensionsHandler = Dimensions.addEventListener(
        'change',
        newSizes => {
          setScreenDimensions({
            with: newSizes.window.width,
            height: newSizes.window.height,
          });
        },
      );

      return () => {
        DimensionsHandler.remove();
      };
    }, []),
  );

  const OpenAnimation = {
    from: {
      borderBottomLeftRadius: 5000,
      borderTopLeftRadius: 5000,
      borderBottomRightRadius: 5000,
      width: 0,
      height: 0,
      right: 25,
      marginTop: 120,
    },
    to: {
      borderBottomLeftRadius: 0,
      borderTopLeftRadius: 0,
      borderBottomRightRadius: 0,
      width: screenDimensions.height,
      height: screenDimensions.height,
      right: 0,
      marginTop: 0,
    },
  };

  const CloseAnimation = {
    from: {
      borderBottomLeftRadius: 0,
      borderTopLeftRadius: 0,
      borderBottomRightRadius: 0,
      width: screenDimensions.height,
      height: screenDimensions.height,
      right: 0,
      marginTop: 0,
    },
    to: {
      borderBottomLeftRadius: 5000,
      borderTopLeftRadius: 5000,
      borderBottomRightRadius: 5000,
      width: 0,
      height: 0,
      right: 25,
      marginTop: 120,
    },
  };

  return (
    <Animatable.View
      onAnimationEnd={() => {
        setSettingActive(active => (settingOpen ? active : false));
        if (settingOpen) {
          setshowSettings(true);
        }
      }}
      animation={settingOpen ? OpenAnimation : CloseAnimation}
      duration={500}
      style={[
        styles.container,
        {
          backgroundColor: colors.primary + 'ee',
        },
      ]}>
      <View
        style={[
          styles.content,
          {
            width: screenDimensions.width,
          },
        ]}>
        {showSettings === true && (
          <Fragment>
            {searchText.length !== 0 && (
              <ScrollView style={styles.scroll_view}>
                <StyledText
                  type="italic"
                  style={{
                    color: colors.specialText,
                    textAlign: 'center',
                    fontSize: 12,
                    marginBottom: 8,
                  }}>
                  {(EntreprisesToDisplay.length &&
                  EntreprisesToDisplay.length < 10
                    ? '0'
                    : '') + EntreprisesToDisplay.length}{' '}
                  entreprise(s) trouvée(s) pour{' '}
                  <StyledText style={{color: colors.specialText}}>
                    "{searchText}"
                  </StyledText>
                </StyledText>
                <List>
                  {[...EntreprisesToDisplay].map((item, index) => (
                    <EntrepriseCardSearch
                      entreprise={item}
                      navigation={navigation}
                      key={index}
                    />
                  ))}
                </List>
                <View style={styles.margin_component} />
              </ScrollView>
            )}
          </Fragment>
        )}
      </View>
    </Animatable.View>
  );
};

export default AnimatedOverlayContainer;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 1,
    paddingTop: 60,
  },
  content: {
    marginTop: 30,
    flex: 1,
    alignSelf: 'flex-end',
  },
  margin_component: {
    marginVertical: 200,
  },
  scroll_view: {
    flex: 1,
    marginHorizontal: 10,
  },
});

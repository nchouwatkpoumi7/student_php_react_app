export const format_amount = n => {
  let i = 1,
    result = '';
  for (const l of n.toString().split('').reverse()) {
    result += l;
    if (i % 3 === 0 && i < n.toString().length) {
      result += '.';
    }
    i++;
  }

  return result.split('').reverse().join('');
};

const CurrencyHelpers = {format_amount};
export default CurrencyHelpers;

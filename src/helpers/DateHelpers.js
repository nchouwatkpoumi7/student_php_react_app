const ajoute_zero = n => (n < 10 ? '0' + n : n);
export const formatDate = date => {
  const today = new Date();
  const d = new Date(date);
  if (
    today.getDate() === d.getDate() &&
    today.getMonth() === d.getMonth() &&
    today.getFullYear() === d.getFullYear()
  ) {
    return ajoute_zero(d.getHours()) + ':' + ajoute_zero(d.getMinutes());
  }
  const temp = new Date();
  temp.setDate(today.getDate() - 1);

  if (
    temp.getDate() === d.getDate() &&
    temp.getMonth() === d.getMonth() &&
    temp.getFullYear() === d.getFullYear()
  ) {
    return 'hier';
  } else {
    return (
      ajoute_zero(d.getDate()) +
      '/' +
      ajoute_zero(d.getMonth() + 1) +
      '/' +
      d.getFullYear()
    );
  }
};

const times = [
  ['seconde', 1],
  ['minute', 60],
  ['heure', 3600],
  ['jour', 86400],
  ['semaine', 604800],
  ['mois', 2592000],
  ['année', 31536000],
];

export function timeAgo(date) {
  var diff = Math.round((Date.now() - date) / 1000);
  for (var t = 0; t < times.length; t++) {
    if (diff < times[t][1]) {
      if (t === 0) {
        return "A l'instant";
      } else {
        diff = Math.round(diff / times[t - 1][1]);
        return 'il y a ' + diff + ' ' + times[t - 1][0] + (diff > 1 ? 's' : '');
      }
    }
  }
}

const DateHelpers = {formatDate, timeAgo};

export default DateHelpers;

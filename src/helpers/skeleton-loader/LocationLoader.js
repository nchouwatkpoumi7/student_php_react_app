import * as React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {SkeletonLoader} from './SkeletonLoader';

const backgroundColor = '#3a41b5';
const highlightColor = '#3F51B5';

export const LocationLoader = () => {
  return (
    <SkeletonLoader
      backgroundColor={backgroundColor}
      highlightColor={highlightColor}>
      <Item />
    </SkeletonLoader>
  );
};

const Item = () => {
  return <View style={styles.line} />;
};

const styles = StyleSheet.create({
  line: {
    height: 20,
    backgroundColor: backgroundColor,
    marginTop: 6,
    marginRight: 30,
  },
});

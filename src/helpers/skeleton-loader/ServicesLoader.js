import * as React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {SkeletonLoader} from './SkeletonLoader';

const backgroundColor = 'lightgrey';
const highlightColor = 'white';

export const ServicesLoader = () => {
  return (
    <SkeletonLoader
      backgroundColor={backgroundColor}
      highlightColor={highlightColor}>
      <View style={styles.container}>
        {new Array(10).fill(null).map((_, index) => (
          <Item key={index} />
        ))}
      </View>
    </SkeletonLoader>
  );
};

const Item = () => {
  const {width} = useWindowDimensions();
  return (
    <View style={styles.row}>
      <View style={styles.image} />
      <View style={styles.line} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 10,
  },
  image: {
    height: 50,
    width: 50,
    backgroundColor: backgroundColor,
    borderRadius: 25,
    marginHorizontal: 17,
    marginVertical: 8,
  },
  row: {
    // flexDirection: 'row',
    // marginBottom: 40,
  },
  line: {
    height: 5,
    backgroundColor: backgroundColor,
    marginHorizontal: 5,
  },
});

import * as React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {SkeletonLoader} from './SkeletonLoader';

const backgroundColor = 'lightgrey';
const highlightColor = 'white';

export const ItemListLoader = () => {
  return (
    <SkeletonLoader
      backgroundColor={backgroundColor}
      highlightColor={highlightColor}>
      <View style={styles.container}>
        {new Array(10).fill(null).map((_, index) => (
          <Item key={index} />
        ))}
      </View>
    </SkeletonLoader>
  );
};

const Item = () => {
  const {width} = useWindowDimensions();
  return (
    <View style={styles.row}>
      <View style={styles.image} />
      <View>
        <View style={[styles.line, {width: width * 0.6}]} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingLeft: 8,
  },
  image: {
    height: 100,
    width: 104,
    backgroundColor: backgroundColor,
    marginRight: 6,
    borderRadius: 15,
  },
  row: {
    flexDirection: 'row',
    // marginBottom: 40,
  },
  line: {
    height: 100,
    marginBottom: 10,
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    borderRadius: 10,
    backgroundColor: backgroundColor,
  },
});

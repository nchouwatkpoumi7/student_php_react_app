import * as React from 'react';
import {View, StyleSheet, useWindowDimensions} from 'react-native';
import {SkeletonLoader} from './SkeletonLoader';

const backgroundColor = 'lightgrey';
const highlightColor = 'white';

export const SliderLoader = () => {
  return (
    <SkeletonLoader
      backgroundColor={backgroundColor}
      highlightColor={highlightColor}>
      <Item />
    </SkeletonLoader>
  );
};

const Item = () => {
  const {width} = useWindowDimensions();
  return <View style={[styles.image, {width: width - 20}]} />;
};

const styles = StyleSheet.create({
  image: {
    height: 180,
    backgroundColor: backgroundColor,
    borderRadius: 10,
    marginRight: 10,
  },
});

/* eslint-disable react/prop-types */
import { useTheme } from '@react-navigation/native';
import { Button, Form, Input, Item } from 'native-base';
import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Modal } from 'react-native';
import StyledText from '../components/StyledText';

const InputModal = ({ visible, label, onRequestClose, onSubmit }) => {
	const { colors } = useTheme();

	const [value, setValue] = useState('');

	return (
		<Modal visible={visible} transparent onRequestClose={onRequestClose}>
			<View style={styles.modalContent}>
				<View style={[{ backgroundColor: colors.primary }, styles.errorModal]}>
					<TouchableOpacity
						style={styles.modalCloseButton}
						onPress={onRequestClose}
					>
						<StyledText style={{ color: colors.specialText }}>X</StyledText>
					</TouchableOpacity>
					<Form style={{ flex: 1, width: '100%', marginTop: 35 }}>
						<Item style={{ borderBottomWidth: 0 }}>
							<Input
								placeholder={label}
								style={{
									borderRadius: 10,
									backgroundColor: '#FFF',
									height: 50,
								}}
								onChangeText={setValue}
							/>
						</Item>

						<Item
							style={{
								marginTop: 10,
								borderBottomWidth: 0,
								justifyContent: 'center',
							}}
						>
							<Button
								onPress={() => {
									onSubmit(value);
								}}
								rounded
								style={{
									paddingHorizontal: 60,
									backgroundColor: colors.success,
								}}
							>
								<StyledText style={{ color: colors.specialText }}>
									VALIDER
								</StyledText>
							</Button>
						</Item>
					</Form>
				</View>
			</View>
		</Modal>
	);
};

export default InputModal;

const styles = StyleSheet.create({
	modalContent: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: 'rgba(1, 1, 1, .5)',
	},
	modalCloseButton: {
		paddingHorizontal: 10,
		position: 'absolute',
		top: 7,
		right: 5,
	},
	errorModal: {
		width: '85%',
		height: 150,
		paddingHorizontal: 15,
		borderRadius: 13,
		justifyContent: 'center',
		alignItems: 'center',
	},
});

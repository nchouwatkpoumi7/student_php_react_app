import React from 'react';
import {useTheme} from '@react-navigation/native';

export const withTheme = Component => {
  const Wrapper = props => {
    const {colors} = useTheme();

    return <Component colors={colors} {...props} />;
  };

  return Wrapper;
};

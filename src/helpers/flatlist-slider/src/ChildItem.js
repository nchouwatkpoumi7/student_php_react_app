/* eslint-disable react/prop-types */
import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

const ChildItem = ({ item, style, onPress, imageKey, local, height }) => {
	return (
		<TouchableOpacity
			style={styles.container}
			onPress={() => onPress(item)}>
			<Image
				style={[styles.image, style, { height: height }]}
				source={local ? item[imageKey] : { uri: item[imageKey] }}
			/>
		</TouchableOpacity>
	);
};

const styles = StyleSheet.create({
	container: {},
	image: {
		height: 230,
		resizeMode: 'stretch',
	},
});

export default ChildItem;

export const PROJECT_STATUS = [
  {
    key: 'all',
    title: 'Tout',
  },
  {
    key: 'completed',
    title: 'Projets achevés',
  },
  {
    key: 'active',
    title: 'Projets en cours',
  },
  {
    key: 'idle',
    title: 'Projets en pause',
  },
];

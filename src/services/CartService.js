import AsyncStorage from '@react-native-async-storage/async-storage';

class CartService {
  async getCart() {
    try {
      const res = await AsyncStorage.getItem('@CART');
      return res ? JSON.parse(res) : [];
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async setCart(cart) {
    try {
      await AsyncStorage.setItem('@CART', JSON.stringify(cart));
    } catch (e) {
      console.log(e);
    }
  }

  async add_to_cart(item, qty = 1, cart_items = [], mode = 'normal') {
    try {
      const res = cart_items.length ? cart_items : await this.getCart();
      const cart = res;
      let elem = {
        ...item,
        quantity: 0,
        unread: true,
        initial_quantity: item.quantity,
      };
      const index = cart.findIndex(elem_1 => elem_1.id == item.id);
      if (index != -1) {
        elem = cart[index];
      }

      elem.quantity += qty;

      if (elem.quantity > item.quantity) {
        return;
      }

      if (index != -1) {
        cart[index] = elem;
      } else {
        cart.push(elem);
      }
      if (mode !== 'normal') {
        return cart;
      }
      return await AsyncStorage.setItem('@CART', JSON.stringify(cart));
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async remove_from_cart(item, cart_items = [], mode = 'normal') {
    try {
      const res = cart_items.length ? cart_items : await this.getCart();
      const cart = res;
      const index = cart.findIndex(elem_1 => elem_1.id == item.id);
      const elem = cart[index];

      elem.quantity--;

      if (elem.quantity == 0) {
        cart.splice(index, 1);
      }
      if (mode !== 'normal') {
        return cart;
      }
      return await AsyncStorage.setItem('@CART', JSON.stringify(cart));
    } catch (e) {
      console.log(e);
      return false;
    }
  }

  async read_all_items() {
    const cart = await this.getCart();
    return await AsyncStorage.setItem(
      '@CART',
      JSON.stringify(cart.map(item => ({...item, unread: false}))),
    );
  }

  clear_cart() {
    return AsyncStorage.removeItem('@CART');
  }
}

export default CartService;

export const CartProvider = new CartService();

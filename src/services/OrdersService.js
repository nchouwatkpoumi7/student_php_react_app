import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
const WALCOM_ORDERS_COLLECTION = 'walcom-orders';
class OrdersService {
  async getAll() {
    try {
      const res = await firestore()
        .collection(WALCOM_ORDERS_COLLECTION)
        .where('user_id', '==', auth().currentUser.uid)
        .orderBy('created_at', 'desc')
        .get();
      if (res.size == 0) return [];
      else {
        return res.docs.map(doc => ({...doc.data(), id: doc.id}));
      }
    } catch (e) {
      console.log(e);
    }
  }

  async updateStock(products) {
    for (let product of products) {
      const ref = firestore().collection('walcom-products').doc(product.id);
      await ref.get().then(docSnap => {
        ref.update({
          quantity: docSnap.data().quantity - product.quantity,
        });
      });
    }
  }
}

export default OrdersService;

export const OrdersProvider = new OrdersService();

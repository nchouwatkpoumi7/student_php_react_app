import firestore from '@react-native-firebase/firestore';
const WALCOM_ENTREPRISES_COLLECTION = 'walcom-companies';
const WALCOM_ENTREPRISES_CATEGORIES_COLLECTION = 'walcom-companies-categories';

class EntrepriseService {
	async getAll() {
		try {
			const res = await firestore()
				.collection(WALCOM_ENTREPRISES_COLLECTION)
				.get();
			if (res.size == 0) return [];
			else {
				const result = [];
				for (let doc of res.docs) {
					const category = await this.getCategory(doc.data().category_ref);
					result.push({ id: doc.id, ...doc.data(), category });
				}

				return result;
			}
		} catch (e) {
			console.log(e);
		}
	}

	async getCategory(category_ref) {
		try {
			const doc = await firestore()
				.collection(WALCOM_ENTREPRISES_CATEGORIES_COLLECTION)
				.doc(category_ref)
				.get();
			return { id: doc.id, ...doc.data() };
		} catch (e) {
			console.log(e);
		}
	}

	async getCategories() {
		try {
			const res = await firestore()
				.collection(WALCOM_ENTREPRISES_CATEGORIES_COLLECTION)
				.get();
			if (res.size == 0) return [];
			else {
				return res.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
			}
		} catch (e) {
			console.log(e);
		}
	}
}

export default EntrepriseService;

export const EntrepriseProvider = new EntrepriseService();

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';

const WALCOM_USERS_COLLECTION = 'walcom-users';

class AuthService {
  async uploadFile(path) {
    let reference = storage().ref('users-profiles/' + auth().currentUser.uid);
    let task = reference.putString(
      path.replace('data:image/*;base64,', ''),
      'base64',
    );
    try {
      await task;
      console.log('Image uploaded to the bucket!');
      return await reference.getDownloadURL();
    } catch (e) {
      console.log('uploading image error => ', e);
      throw e;
    }
  }

  async updateImage(uri) {
    const url = await this.uploadFile(uri);
    auth().currentUser.updateProfile({photoURL: url});
    auth().currentUser.reload();
    return firestore()
      .collection(WALCOM_USERS_COLLECTION)
      .doc(auth().currentUser.uid)
      .set({image: url}, {merge: true});
  }

  async get(id) {
    try {
      const res = await firestore()
        .collection(WALCOM_USERS_COLLECTION)
        .doc(id)
        .get();
      return {...res.data(), id: res.id};
    } catch (reason) {
      console.log(reason);
      return null;
    }
  }

  async saveUserDatas(name, phone) {
    const {uid, email} = auth().currentUser;
    await firestore()
      .collection(WALCOM_USERS_COLLECTION)
      .doc(uid)
      .set({name, phone, email}, {merge: true});
    return auth()
      .currentUser.updateProfile({displayName: name})
      .then(() => {
        auth().currentUser.reload();
      });
  }

  register(email, password) {
    return new Promise((resolve, reject) => {
      auth()
        .createUserWithEmailAndPassword(email, password)
        .then(resolve)
        .catch(reject);
    });
  }

  login(email, password) {
    return auth().signInWithEmailAndPassword(email, password);
  }

  resetPassword(email) {
    return auth().sendPasswordResetEmail(email);
  }

  signOut() {
    return auth().signOut();
  }
}

export default AuthService;

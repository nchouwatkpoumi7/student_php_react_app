/* eslint-disable no-mixed-spaces-and-tabs */
import storage from '@react-native-firebase/storage';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import PushNotification from 'react-native-push-notification';

const WALCOM_MESSAGE_COLLECTION = 'walcom-messages';

class ChatServices {
  async uploadFile(path, imageName, type = 'file') {
    let reference = storage().ref('messages/attachments/' + imageName);
    let task =
      type === 'image'
        ? reference.putString(
            path.replace('data:image/*;base64,', ''),
            'base64',
          )
        : reference.putFile(path);
    try {
      await task;
      console.log('Image uploaded to the bucket!');
      return await reference.getDownloadURL();
    } catch (e) {
      console.log('uploading image error => ', e);
      throw e;
    }
  }

  async getConversationId(user_id, entreprise_id) {
    const query = await firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .where('user_id', '==', user_id)
      .where('entreprise_id', '==', entreprise_id)
      .get();

    if (query.size) {
      return query.docs[0].id;
    } else {
      return firestore()
        .collection(WALCOM_MESSAGE_COLLECTION)
        .add({user_id, entreprise_id})
        .then(res => res.id);
    }
  }

  getMessagesByConversation(conversation_id) {
    return firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .doc(conversation_id)
      .collection('messages')
      .get();
  }

  receiveMessage(conversation, message) {
    PushNotification.channelExists('walcom-messages', function (exists) {
      if (!exists) {
        PushNotification.createChannel(
          {
            channelId: 'walcom-messages', // (required)
            channelName: 'WalCom Messages', // (required)
            channelDescription: "Messages de l'application WalCom",
            vibrate: true,
          },
          created => {
            PushNotification.localNotification({
              channelId: 'walcom-messages',
              message:
                message.data().type === 'text'
                  ? message.data().text
                  : 'Vous avez reçu une pièce jointe !',
              title: conversation.title,
            });
          },
        );
      } else {
        PushNotification.localNotification({
          channelId: 'walcom-messages',
          message:
            message.data().type === 'text'
              ? message.data().text
              : 'Vous avez reçu une pièce jointe !',
          title: conversation.title,
        });
      }
    });

    return firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .doc(conversation.id)
      .collection('messages')
      .doc(message.id)
      .update({status: 'delivered'});
  }

  readMessage(conversation_id, message_id) {
    return firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .doc(conversation_id)
      .collection('messages')
      .doc(message_id)
      .update({status: 'seen'});
  }

  receiveAllMessages(messages, conversation) {
    for (let message of messages) {
      if (
        message.data().author.id !== auth().currentUser.uid &&
        message.data().status === 'sent'
      ) {
        this.receiveMessage(conversation, message);
      }
    }
  }

  readAllMessages(messages, conversation_id) {
    for (let message of messages) {
      if (
        message.data().author.id !== auth().currentUser.uid &&
        message.data().status !== 'seen'
      ) {
        this.readMessage(conversation_id, message.id);
      }
    }
  }

  async sendMessage(message, entreprise_id) {
    const data = message;

    if (message.type === 'file' || message.type === 'image') {
      await this.uploadFile(message.uri, message.id, message.type)
        .then(res => {
          data.uri = res;
        })
        .catch(e => {
          console.log(e);
        });
    }

    data.entreprise_id = entreprise_id;
    data.user_id = auth().currentUser.uid;
    data.status = 'sent';

    const conversation_id = await this.getConversationId(
      data.user_id,
      data.entreprise_id,
    );

    const ref = firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .doc(conversation_id);

    return ref
      .collection('messages')
      .doc(message.id)
      .set(message)
      .then(() => {
        ref.set({last_message: message, updated_at: Date.now()}, {merge: true});
        return conversation_id;
      });
  }

  async deleteMessage(message, conversation_id) {
    const ref = firestore()
      .collection(WALCOM_MESSAGE_COLLECTION)
      .doc(conversation_id);

    return ref.collection('messages').doc(message.id).delete();
  }
}

export default ChatServices;
export const ChatProvider = new ChatServices();

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
const WALCOM_PROJECTS_COLLECTION = 'walcom-projects';

class ProjectsService {
	async getAll() {
		try {
			const res = await firestore()
				.collection(WALCOM_PROJECTS_COLLECTION)
				.where('user_id', '==', auth().currentUser.uid)
				// .orderBy('updated_at', 'desc')
				.get();
			if (res.size == 0) return [];
			else {
				return res.docs.map((doc) => ({ ...doc.data(), id: doc.id }));
			}
		} catch (e) {
			console.log(e);
		}
	}
}

export default ProjectsService;

export const ProjectsProvider = new ProjectsService();

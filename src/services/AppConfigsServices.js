import firestore from '@react-native-firebase/firestore';
const WALCOM_CONFIGS_COLLECTION = 'walcom-system';

class AppConfigsServices {
	async getAll() {
		try {
			const res = await firestore().collection(WALCOM_CONFIGS_COLLECTION).get();
			if (res.size == 0) return [];
			else {
				const result = {};
				for (let doc of res.docs) {
					result[doc.id] = doc.data();
				}

				return result;
			}
		} catch (e) {
			console.log(e);
		}
	}
}

export default AppConfigsServices;

export const AppConfigsProvider = new AppConfigsServices();

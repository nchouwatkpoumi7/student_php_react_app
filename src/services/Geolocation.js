import { PermissionsAndroid } from 'react-native';
import Geolocation from 'react-native-geolocation-service';

class GeolocationServices {
	async getCurrentPosition() {
		return new Promise((resolve, reject) => {
			PermissionsAndroid.requestMultiple([
				'android.permission.ACCESS_COARSE_LOCATION',
				'android.permission.ACCESS_FINE_LOCATION',
			])
				.then(() => {
					const observable = Geolocation.watchPosition(
						async (position) => {
							resolve(position.coords);
							Geolocation.clearWatch(observable);
						},
						(error) => {
							reject(error);
						},
						{
							enableHighAccuracy: true,
							timeout: 15000,
							maximumAge: 10000,
							forceRequestLocation: true,
							interval: 1000,
						}
					);
				})
				.catch((e) => {
					reject(e);
				});
		});
	}
}

export default GeolocationServices;

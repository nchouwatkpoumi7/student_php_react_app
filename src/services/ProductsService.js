import firestore from '@react-native-firebase/firestore';
const WALCOM_PRODUCTS_COLLECTION = 'walcom-products';
const WALCOM_PRODUCTS_CATEGORIES_COLLECTION = 'walcom-products-categories';
class ProductsService {
  async getAll() {
    try {
      const res = await firestore()
        .collection(WALCOM_PRODUCTS_COLLECTION)
        .orderBy('updated_at', 'desc')
        .get();
      if (res.size == 0) return [];
      else {
        const result = [];
        for (let doc of res.docs) {
          const category = await this.getCategory(doc.category_ref);
          result.push({id: doc.id, ...doc.data(), category});
        }

        return result;
      }
    } catch (e) {
      console.log(e);
    }
  }

  async getCategory(category_ref) {
    try {
      const doc = await firestore()
        .collection(WALCOM_PRODUCTS_CATEGORIES_COLLECTION)
        .doc(category_ref)
        .get();
      return {id: doc.id, ...doc.data()};
    } catch (e) {
      console.log(e);
    }
  }

  async getCategories() {
    try {
      const res = await firestore()
        .collection(WALCOM_PRODUCTS_CATEGORIES_COLLECTION)
        .get();
      if (res.size == 0) return [];
      else {
        return res.docs.map(doc => ({...doc.data(), id: doc.id}));
      }
    } catch (e) {
      console.log(e);
    }
  }
}

export default ProductsService;

export const ProductsProvider = new ProductsService();

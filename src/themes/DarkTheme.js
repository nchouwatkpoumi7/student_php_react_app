export const currentTheme = {
  colors: {
    card: {
      background: '#000',
      text: '#fff',
    },

    text: '#fefefe',
    background: '#222',

    primary: '#333',
    secondary: '#777',

    success: '#5cb85c',
    warning: '#f0ad4e',
    danger: '#d9534f',
    info: '#62B1F6',
    ios: '#007aff',

    

    specialText: '#FFF',
    blackText: '#FFF',

    disabled: 'rgba(63, 81, 181, .6)',
    primary_grised: 'rgba(70, 90, 200, .75)',
    gray: '#524A4A',
    gray_light: '#bbb',
    gray_background: '#999',
    drawer_item: '#454545',
    // placeholder: '#3D32B5',
    placeholder: '#999',
    placeholderBg: 'rgba(20, 50, 100, .1)',
  },
  typography: {
    icon: 25,
    text: 13,
  },
};

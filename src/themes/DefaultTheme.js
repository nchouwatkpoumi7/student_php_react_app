export const currentTheme = {
  colors: {
    card: {
      background: '#FFF',
      text: '#000',
    },

    text: '#000',
    background: '#FEFEFE',

    // primary: '#3F51B5',
    primary: '#ff6600',
    // secondary: '#35a163',
    secondary: '#fe9a4a',
    success: '#5cb85c',
    warning: '#f0ad4e',
    danger: '#d9534f',
    info: '#62B1F6',
    ios: '#007aff',
    secondary_border: '#fe884a',
    primary_border: '#ff4400',


    specialText: '#FFFFFF',
    blackText: '#000',

    disabled: 'rgba(63, 81, 181, .6)',
    primary_grised: 'rgba(70, 90, 200, .75)',
    gray: '#524A4A',
    gray_light: '#bbb',
    gray_background: '#999',
    drawer_item: '#454545',
    // placeholder: '#3D32B5',
    placeholder: '#999',
    placeholderBg: 'rgba(20, 50, 100, .1)',
  },
  typography: {
    icon: 25,
    text: 13,
  },
};

/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  ListItem,
  Left,
  Button,
  Icon,
  Header,
  Body,
  Container,
  Content,
} from 'native-base';

import StyledText from '../components/StyledText';
import {useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';

const ServicesScreen = ({navigation}) => {
  const getServices = useSelector(state => state.services);
  const {services = []} = getServices;
  const {colors} = useTheme();

  console.log(getServices);

  return (
    <Container>
      <Header style={{backgroundColor: colors.primary}}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            LES SERVICES
          </StyledText>
        </Body>
      </Header>
      <Content scrollEnabled style={{paddingTop: 10}}>
        {services.map(service => (
          <ListItem
            key={service.icon}
            icon
            onPress={() => {
              navigation.navigate('EntreprisesListByType', {service});
            }}>
            <Left>
              <Button style={{backgroundColor: colors.primary}}>
                <Icon active type="MaterialIcons" name={service.icon} />
              </Button>
            </Left>
            <Body>
              <StyledText type="medium">{service.title}</StyledText>
            </Body>
          </ListItem>
        ))}
      </Content>
    </Container>
  );
};

export default ServicesScreen;

const styles = StyleSheet.create({});

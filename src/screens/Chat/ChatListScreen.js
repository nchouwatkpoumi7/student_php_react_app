/* eslint-disable react/prop-types */
import React from 'react';

import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

import {StyleSheet, TouchableOpacity} from 'react-native';
import {
  ListItem,
  Left,
  Icon,
  Header,
  Body,
  Text,
  Container,
  Content,
  Thumbnail,
  Right,
  Badge,
  View,
} from 'native-base';

import StyledText from '../../components/StyledText';
import ConversationItem from '../../components/Chat/ConversationItem';
import {useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';
import DateHelpers from '../../helpers/DateHelpers';
import PushNotification from 'react-native-push-notification';

const ChatListScreen = ({navigation}) => {
  const getEntreprises = useSelector(state => state.entreprises);
  const {entreprises = []} = getEntreprises;
  const {colors} = useTheme();

  const [Conversations, setConversations] = React.useState([]);

  React.useEffect(() => {
    const unsubscribe = firestore()
      .collection('walcom-messages')
      .where('user_id', '==', auth().currentUser.uid)
      .onSnapshot(query => {
        setConversations(
          query.docs
            .map(doc => {
              const entreprise = entreprises?.find(
                e => e.id === doc.data().entreprise_id,
              );
              return {
                id: doc.id,
                ...doc.data(),
                title: entreprise?.title,
                image: entreprise?.image,
              };
            })
            .sort((con, con2) => con2.updated_at - con.updated_at),
        );
      });

    return unsubscribe;
  }, [entreprises]);

  return (
    <Container>
      <Header style={{backgroundColor: colors.primary}}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.openDrawer();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="apps"
              style={{fontSize: 23, color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            CONVERSATIONS
          </StyledText>
        </Body>
      </Header>
      {Conversations.length ? (
        <Content scrollEnabled style={{paddingTop: 10}}>
          {Conversations.map((conversation, index) => (
            <ConversationItem
              key={index}
              conversation={conversation}
              navigation={navigation}
            />
          ))}
        </Content>
      ) : (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <StyledText>Aucune conversation à afficher !</StyledText>
        </View>
      )}
    </Container>
  );
};

export default ChatListScreen;

const styles = StyleSheet.create({});

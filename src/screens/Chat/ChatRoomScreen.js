/* eslint-disable react/prop-types */
// import { useActionSheet } from '@expo/react-native-action-sheet';
import {Chat} from '@flyerhq/react-native-chat-ui';
// import { PreviewData } from '@flyerhq/react-native-link-preview';
import {
  Modal,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Alert,
} from 'react-native';
import React, {useState} from 'react';
import DocumentPicker from 'react-native-document-picker';
// import OpenFile from 'react-native-files-viewer';
import FileViewer from 'react-native-file-viewer';
import {launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import {v4 as uuidv4} from 'uuid';

import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import {useTheme} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import StyledText from '../../components/StyledText';
import {Body, Form, Header, Icon, Left, Thumbnail} from 'native-base';
import {ChatProvider} from '../../services/ChatServices';

const ChatRoomScreen = ({navigation, route}) => {
  const {chat} = route.params;
  const {colors} = useTheme();

  // const { showActionSheetWithOptions } = useActionSheet();
  const [messages, setMessages] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [deleteModalVisible, setDeleteModalVisible] = useState(false);
  const [selectedMessage, setSelectedMessage] = useState({});
  const [conversation, setConversation] = useState(chat);
  const [messageText, setMessageText] = useState('');
  const user = {id: auth().currentUser.uid};

  useFocusEffect(
    React.useCallback(() => {
      ChatProvider.getConversationId(user.id, chat.entreprise_id).then(id => {
        setConversation({...conversation, id});
      });
      const unsubscribe = firestore()
        .collection('walcom-messages')
        .doc(conversation.id)
        .collection('messages')
        .orderBy('createdAt', 'desc')
        .onSnapshot(
          snapshot => {
            setMessages(
              snapshot.docs.map(doc => ({id: doc.id, ...doc.data()})),
            );
            ChatProvider.readAllMessages(snapshot.docs, conversation.id);
          },
          error => {
            console.log(error);
          },
        );
      return unsubscribe;
    }, [chat.entreprise_id, conversation, user.id]),
  );

  const handleDeleteMessage = async () => {
    await ChatProvider.deleteMessage(selectedMessage, conversation.id).then(
      () => {
        setDeleteModalVisible(false);
      },
    );
  };

  const addMessage = async message => {
    setMessages([message, ...messages]);
    const conversation_id = await ChatProvider.sendMessage(
      message,
      chat.entreprise_id,
    );
    setConversation({...conversation, id: conversation_id});
  };

  const handleAttachmentPress = () => {
    setModalVisible(true);
  };

  const handleFileSelection = async () => {
    try {
      const response = await DocumentPicker.pickSingle({
        type: [DocumentPicker.types.allFiles],
      });
      const fileMessage = {
        author: user,
        createdAt: Date.now(),
        id: uuidv4(),
        mimeType: response.type ?? undefined,
        name: response.name,
        size: response.size ?? 0,
        type: 'file',
        uri: response.uri,
        status: 'sending',
      };
      addMessage(fileMessage);
    } catch (e) {
      console.log(e);
    }
    setModalVisible(false);
  };

  const handleImageSelection = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
      freeStyleCropEnabled: true,
    })
      .then(response => {
        if (response?.data) {
          const imageMessage = {
            author: user,
            createdAt: Date.now(),
            height: response.height,
            id: uuidv4(),
            name: response.filename ?? response.path?.split('/').pop() ?? '🖼',
            size: response.size ?? 0,
            type: 'image',
            uri: `data:image/*;base64,${response.data}`,
            width: response.width,
            status: 'sending',
          };
          addMessage(imageMessage);
        }
      })
      .catch(e => {
        console.log(e);
      });
    setModalVisible(false);
  };

  const handleMessagePress = async message => {
    if (message.type === 'file') {
      try {
        FileViewer.open(message.uri).catch(error => {
          console.log(error);
        });
      } catch (e) {
        console.log(e);
      }
    }
  };

  const handlePreviewDataFetched = ({message, previewData}) => {
    setMessages(
      messages.map(m => (m.id === message.id ? {...m, previewData} : m)),
    );
  };

  const handleSendPress = () => {
    if (!messageText) return;
    const textMessage = {
      author: user,
      createdAt: Date.now(),
      id: uuidv4(),
      text: messageText,
      type: 'text',
      status: 'sending',
    };
    setMessageText('');
    addMessage(textMessage);
  };

  return (
    <>
      <Modal
        visible={modalVisible}
        transparent
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.modalContent}>
          <View style={[{backgroundColor: colors.primary}, styles.errorModal]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setModalVisible(false);
              }}>
              <StyledText style={{color: colors.specialText}}>X</StyledText>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <Icon
                onPress={handleImageSelection}
                type="MaterialIcons"
                name="image"
                style={{
                  fontSize: 35,
                  color: colors.specialText,
                  marginVertical: 5,
                }}
              />
              {/* <Icon
                onPress={handleFileSelection}
                type="MaterialIcons"
                name="post-add"
                style={{
                  fontSize: 35,
                  color: colors.specialText,
                  marginVertical: 5,
                }}
              /> */}
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        visible={deleteModalVisible}
        transparent
        onRequestClose={() => {
          setDeleteModalVisible(false);
        }}>
        <View style={styles.modalContent}>
          <View style={[{backgroundColor: colors.primary}, styles.errorModal]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setDeleteModalVisible(false);
              }}>
              <StyledText style={{color: colors.specialText}}>X</StyledText>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <TouchableOpacity onPress={handleDeleteMessage}>
                <Icon
                  type="MaterialIcons"
                  name="delete"
                  style={{
                    fontSize: 35,
                    color: colors.specialText,
                    marginVertical: 5,
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Header style={{backgroundColor: colors.primary}}>
        <Left style={{flex: 1}}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={{flexDirection: 'row', alignItems: 'center'}}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />

            <Thumbnail
              style={{maxHeight: 35, maxWidth: 35}}
              source={{uri: chat.image}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 3}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            {chat.title}
          </StyledText>
        </Body>
      </Header>
      <Chat
        onMessageLongPress={message => {
          setSelectedMessage(message);
          setDeleteModalVisible(true);
        }}
        emptyState={() => (
          <StyledText type="regular">
            {"Aucun messages pour l'instant..."}
          </StyledText>
        )}
        messages={[
          ...messages,
          // {
          // 	status: '',
          // },
        ]}
        onMessagePress={handleMessagePress}
        onPreviewDataFetched={handlePreviewDataFetched}
        user={user}
        customBottomComponent={() => (
          <Form
            style={{
              backgroundColor: '#f9f9f9',
              paddingHorizontal: 10,
              paddingBottom: 12,
              paddingTop: 10,
            }}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <View
                style={{
                  marginRight: 10,
                }}>
                <TouchableOpacity onPress={handleAttachmentPress}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="attachment"
                  />
                </TouchableOpacity>
              </View>
              <TextInput
                onChangeText={setMessageText}
                value={messageText}
                style={{
                  paddingLeft: 10,
                  borderWidth: 1 / 5,
                  borderRadius: 4,
                  flex: 1,
                  minHeight: 50,
                  maxHeight: 100,
                  fontFamily: 'ProductSans-Regular',
                }}
                multiline
                placeholder="Votre message..."
              />
              <View
                style={{
                  marginLeft: 10,
                }}>
                <TouchableOpacity onPress={handleSendPress}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="send"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </Form>
        )}
      />
    </>
  );
};

export default ChatRoomScreen;

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(1, 1, 1, .5)',
  },
  loadingModal: {
    width: '40%',
    height: 100,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButton: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: 7,
    right: 5,
  },
  errorModal: {
    width: '50%',
    height: 90,
    paddingHorizontal: 15,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

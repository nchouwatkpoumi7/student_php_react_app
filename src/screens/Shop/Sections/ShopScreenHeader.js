/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';
import {View, TouchableOpacity} from 'react-native';

import {useTheme} from '@react-navigation/native';
import {Badge, Icon, Header, Left, Right, Body} from 'native-base';
import StyledText from '../../../components/StyledText';

import {useSelector} from 'react-redux';

const ShopScreenHeader = ({navigation}) => {
  const {colors} = useTheme();

  const getCart = useSelector(state => state.cart);
  // const {cart = []} = getCart;

  return (
    <Header
      style={{
        alignItems: 'center',
        backgroundColor: colors.primary,
      }}>
      <Left>
        <TouchableOpacity
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'center',
          }}
          onPress={() => {
            navigation.openDrawer();
          }}>
          <Icon
            type="MaterialIcons"
            name="apps"
            style={{
              fontSize: 23,
              color: colors.specialText,
            }}
          />
        </TouchableOpacity>
      </Left>
      <Body
        style={{
          flex: 2,
          justifyContent: 'center',
        }}>
        <StyledText
          type="medium"
          style={{
            color: colors.specialText,
            textTransform: 'uppercase',
            fontSize: 13,
          }}>
          WALNERS STORE
        </StyledText>
      </Body>
      <Right>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => {
              navigation.push('CartScreen');
            }}>
            <Icon
              type="MaterialIcons"
              name="shopping-cart"
              style={{
                fontSize: 25,
                marginRight: 15,
                color: colors.specialText,
              }}
            />
            {getCart.cart &&
              getCart.cart?.filter(item => item.unread == true).length != 0 && (
                <Badge
                  style={{
                    position: 'absolute',
                    justifyContent: 'center',
                    alignItems: 'center',
                    top: -2,
                    right: 8,
                    width: 12,
                    height: 12,
                  }}
                />
              )}
          </TouchableOpacity>
        </View>
      </Right>
    </Header>
  );
};

export default ShopScreenHeader;

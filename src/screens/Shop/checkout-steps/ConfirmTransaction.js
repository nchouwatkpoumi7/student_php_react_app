/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import {Card, CardItem, Content} from 'native-base';
import OrderHistoryItem from '../../../components/Shop/Cart/OrderHistoryItem';
import StyledText from './../../../components/StyledText';
import CustomStyledInput from './../../../components/CustomStyledInput';
import {useTheme} from '@react-navigation/native';
import CurrencyHelpers from '../../../helpers/CurrencyHelpers';

const ConfirmTransaction = ({
  params,
  form_datas = {payment_method: {logo: ''}},
  setTransactionRef = () => {},
}) => {
  const {colors} = useTheme();
  console.log(form_datas);
  const total = params.total_price || 0;

  const [tRef, setTRef] = React.useState('');

  React.useEffect(() => {
    setTransactionRef(tRef);
    return () => {};
  }, [tRef]);

  return (
    <Content
      style={{
        backgroundColor: colors.background,
        paddingHorizontal: 8,
        marginTop: 0,
      }}>
      <StyledText type="medium" style={{marginBottom: 15, textAlign: 'center'}}>
        Procédure de paiment
      </StyledText>

      <Card>
        <CardItem style={{borderRadius: 15}}>
          <View>
            <View style={{alignItems: 'center', marginBottom: 10}}>
              <Image
                source={{uri: form_datas.payment_method.logo}}
                style={{width: 35, height: 35}}
              />
              <StyledText
                type="regular"
                style={{marginVertical: 5, textAlign: 'center'}}>
                Paiment par {form_datas.payment_method.title}
              </StyledText>
            </View>

            <StyledText type="regular" style={{textAlign: 'center'}}>
              Montant total
            </StyledText>
            <StyledText style={{textAlign: 'center', fontSize: 18}}>
              {CurrencyHelpers.format_amount(total)}FCFA
            </StyledText>

            <StyledText
              type="regular"
              style={{marginTop: 10, textAlign: 'center'}}>
              Faites un dépôt du montant total au
            </StyledText>
            <StyledText style={{textAlign: 'center', fontSize: 18}}>
              {form_datas.payment_method.number}
            </StyledText>

            <StyledText
              type="regular"
              style={{marginTop: 10, textAlign: 'center'}}>
              Nom de compte
            </StyledText>
            <StyledText style={{textAlign: 'center', fontSize: 18}}>
              {form_datas.payment_method.name}
            </StyledText>

            <StyledText
              type="regular"
              style={{textAlign: 'justify', marginTop: 10}}>
              &nbsp;&nbsp;Après dépôt, renseignez la référence de transaction
              contenue dans le SMS ci-dessous :
            </StyledText>
            <View style={{marginVertical: 15}}>
              <CustomStyledInput
                updatedValue={tRef}
                onChangeText={value => setTRef(value)}
                label="Référence de la transaction*"
              />
            </View>
          </View>
        </CardItem>
      </Card>
    </Content>
  );
};

export default ConfirmTransaction;

const styles = StyleSheet.create({});

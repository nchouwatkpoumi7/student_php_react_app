/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Content} from 'native-base';
import CustomStyledInput from '../../../components/CustomStyledInput';
import {useSelector} from 'react-redux';
import {useFocusEffect} from '@react-navigation/core';

const ShippingInfos = ({setShippingInfos = () => {}}) => {
  const {locationInfos = {infos: {}}} = useSelector(
    state => state.locationInfos,
  );
  const {infos = {}} = locationInfos;

  const getCurrentUser = useSelector(state => state.currentUser);
  const {currentUser} = getCurrentUser;

  const [values, setValues] = React.useState({
    full_name: currentUser.name,
    location: `${infos.adminArea5}, ${infos.street}`,
    phone_number: currentUser.phone,
    email: currentUser.email,
  });

  const onChange = e => {
    const new_values = {...values, [e.target.name]: e.target.value};
    setValues(new_values);
    setShippingInfos(new_values);
  };

  useFocusEffect(
    React.useCallback(() => {
      setShippingInfos({
        full_name: currentUser.name,
        location: `${infos.adminArea5}, ${infos.street}`,
        phone_number: currentUser.phone,
        email: currentUser.email,
      });
    }, []),
  );
  // const CurrentUser = auth().currentUser;

  return (
    <Content
      style={{
        height: '100%',
        paddingHorizontal: 12,
        marginTop: 10,
      }}>
      <View style={{marginVertical: 10}}>
        <CustomStyledInput
          placeholder="FOKAM Georges"
          label="Votre nom"
          updatedValue={values.full_name}
          onChangeText={value => {
            onChange({target: {name: 'full_name', value}});
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <CustomStyledInput
          placeholder=""
          label="Adresse de livraison"
          updatedValue={values.location}
          onChangeText={value => {
            onChange({target: {name: 'location', value}});
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <CustomStyledInput
          placeholder="654491597"
          label="Votre numéro de téléphone"
          updatedValue={values.phone_number}
          onChangeText={value => {
            onChange({target: {name: 'phone_number', value}});
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <CustomStyledInput
          placeholder="john.doe@gmail.com"
          label="Votre adresse email"
          updatedValue={values.email}
          onChangeText={value => {
            onChange({target: {name: 'email', value}});
          }}
        />
      </View>
    </Content>
  );
};

export default ShippingInfos;

const styles = StyleSheet.create({});

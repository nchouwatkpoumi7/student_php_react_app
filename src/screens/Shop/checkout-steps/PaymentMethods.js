/* eslint-disable react/prop-types */
import React from 'react';
import { StyleSheet } from 'react-native';
import {
	Content,
	List,
	Left,
	ListItem,
	Body,
	Thumbnail,
	Right,
	Button,
} from 'native-base';
import StyledText from '../../../components/StyledText';

const PaymentMethods = ({ payment_methods, setPaymentMethod }) => {
	return (
		<Content style={{ paddingHorizontal: 12, marginBottom: 60, marginTop: 10 }}>
			<StyledText
				type="regular"
				style={{ textAlign: 'center', marginVertical: 10, fontSize: 14 }}
			>
				Votre mode de paiement
			</StyledText>
			<List>
				{payment_methods.map((method) => (
					<ListItem
						onPress={() => {
							setPaymentMethod(method);
						}}
						style={{ marginVertical: 15 }}
						key={method.id}
						thumbnail
					>
						<Left>
							<Thumbnail square source={{ uri: method.logo }} />
						</Left>
						<Body>
							<StyledText type="regular">{method.title}</StyledText>
						</Body>
						{/* <Right>
							<Button transparent>
								<StyledText>Choisir</StyledText>
							</Button>
						</Right> */}
					</ListItem>
				))}
			</List>
		</Content>
	);
};

export default PaymentMethods;

const styles = StyleSheet.create({});

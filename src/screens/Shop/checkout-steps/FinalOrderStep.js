/* eslint-disable react/prop-types */
import React from 'react';
import {StyleSheet, View, ActivityIndicator} from 'react-native';
import StyledInput from '../../../components/StyledInput';
import StyledText from '../../../components/StyledText';

const FinalOrderStep = ({form_datas, pending, order_status}) => {
  React.useEffect(() => {
    console.log(form_datas);

    return () => {};
  }, []);

  if (pending)
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size={60} color="blue" />
        <StyledText style={{marginTop: 20}}>
          Envoie de la commande...
        </StyledText>
      </View>
    );

  return (
    <View style={{paddingHorizontal: 12, marginBottom: 60, marginTop: 10}}>
      <View style={{marginVertical: 5}}>
        <StyledInput label="votre nom*" />
      </View>
      <View style={{marginVertical: 5}}>
        <StyledInput label="votre adresse" />
      </View>
      <View style={{marginVertical: 5}}>
        <StyledInput label="votre numéro de téléphone" />
      </View>
      <View style={{marginVertical: 5}}>
        <StyledInput label="votre adresse email" />
      </View>
    </View>
  );
};

export default FinalOrderStep;

const styles = StyleSheet.create({});

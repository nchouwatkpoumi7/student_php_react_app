/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Icon, Toast} from 'native-base';

import ShippingInfos from './checkout-steps/ShippingInfos';
import PaymentMethods from './checkout-steps/PaymentMethods';
import ConfirmTransaction from './checkout-steps/ConfirmTransaction';
import FinalOrderStep from './checkout-steps/FinalOrderStep';
import StyledText from '../../components/StyledText';
import {CartProvider} from '../../services/CartService';
import PushNotification from 'react-native-push-notification';
import {useTheme} from '@react-navigation/native';
import {withTheme} from '../../helpers/hooks/withTheme';
import {OrdersProvider} from '../../services/OrdersService';
import {withDispatch} from '../../helpers/hooks/withDispatch';
import {getCartAction} from '../../redux/actions/Cart';

class CheckoutScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentStep: 0,
      steps: ['Contacts', 'Paiement', 'Validation', 'Terminé'],
      form_datas: {shipping_infos: {}, payment_method: null, t_ref: ''},
      payment_methods: [],
      pending: false,
      order_status: null,
    };

    this.validOrder = this.validOrder.bind(this);
  }

  componentDidMount() {
    console.log(this.props.route.params);
    firestore()
      .collection('walcom-payment-methods')
      .get()
      .then(res => {
        this.setState({
          payment_methods: res.docs.map(doc => ({
            id: doc.id,
            ...doc.data(),
          })),
        });
      });
  }

  validOrder() {
    const {form_datas} = this.state;
    if (!form_datas.t_ref?.trim()?.length) {
      Toast.show({
        type: 'warning',
        text: 'Veuillez rensigner la référence !',
        buttonText: 'ok',
      });
      return;
    }
    // Passer a l'etape suivante
    this.setState(prevState => ({currentStep: prevState.currentStep + 1}));

    // Lancer l'enregistrement de la commande
    this.setState({pending: true});
    const created_at = Date.now();
    firestore()
      .collection('walcom-orders')
      .doc(created_at.toString(36).toUpperCase())
      .set({
        ...form_datas,
        location: form_datas.shipping_infos.location,
        total_price: this.props.route.params.total_price,
        user_id: auth().currentUser.uid,
        products: this.props.route.params.order,
        created_at,
      })
      .then(async () => {
        OrdersProvider.updateStock(this.props.route.params.order);
        if (this.props.route.params.mode !== 'direct') {
          await CartProvider.clear_cart().then(() => {
            this.props.dispatch(getCartAction());
          });
        }
        this.setState({order_status: true});
        Toast.show({
          type: 'success',
          text: 'Commande enregistrée !',
          buttonText: 'OK',
          duration: 4000,
        });
        PushNotification.channelExists('walcom', function (exists) {
          if (!exists) {
            PushNotification.createChannel(
              {
                channelId: 'walcom', // (required)
                channelName: 'WalCom Notifications', // (required)
                channelDescription:
                  'Notifications relatives aux commandes WalCom Store',
                vibrate: true,
              },
              created => {
                PushNotification.localNotification({
                  channelId: 'walcom',
                  bigText:
                    "Votre commande a été envoyée avec succès ! L'administrateur vous contactera sous peu...",
                  title:
                    'Commande #' +
                    created_at.toString(36).toUpperCase() +
                    ' envoyée',
                  message: 'Votre commande a été envoyée avec succès !',
                });
              },
            );
          } else {
            PushNotification.localNotification({
              channelId: 'walcom',
              bigText:
                "Votre commande a été envoyée avec succès ! L'administrateur vous contactera sous peu...",
              title:
                'Commande #' +
                created_at.toString(36).toUpperCase() +
                ' envoyée',
              message: 'Votre commande a été envoyée avec succès !',
            });
          }
        });

        this.props.navigation.goBack();
      })
      .catch(e => {
        console.log(e);
        this.setState({order_status: false});
      })
      .finally(() => {
        this.setState({pending: false});
      });
  }

  render() {
    const {colors} = this.props;

    const styles = StyleSheet.create({
      centerElement: {justifyContent: 'center', alignItems: 'center'},
    });

    const {steps, currentStep} = this.state;

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          backgroundColor: colors.background,
        }}>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: colors.background,
            marginBottom: 10,
          }}>
          <TouchableOpacity
            style={[styles.centerElement, {width: 50, height: 50}]}
            onPress={() => {
              this.props.navigation.goBack();
            }}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{fontSize: 25}}
              color={colors.text}
            />
          </TouchableOpacity>
          <View style={[styles.centerElement, {height: 50}]}>
            <Text style={{fontSize: 18, color: colors.text}}>
              Validation de la commande
            </Text>
          </View>
        </View>

        <View style={{alignItems: 'center'}}>
          <View style={{width: 280, height: 70}}>
            <View style={{alignItems: 'center'}}>
              <View
                style={{
                  height: 2,
                  backgroundColor: 'gray',
                  width: 180,
                  position: 'absolute',
                  top: 13,
                  zIndex: 10,
                }}
              />
            </View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                position: 'absolute',
                zIndex: 20,
              }}>
              {steps.map((label, i) => (
                <View key={i} style={{alignItems: 'center', width: 70}}>
                  {i > currentStep && i != currentStep /* Not selected */ && (
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 30,
                        height: 30,
                        backgroundColor: '#fff',
                        borderWidth: 2,
                        borderColor: 'gray',
                        borderRadius: 15,
                        marginBottom: 10,
                      }}>
                      <Text style={{fontSize: 15, color: 'gray'}}>{i + 1}</Text>
                    </View>
                  )}
                  {i < currentStep /* Checked */ && (
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 30,
                        height: 30,
                        backgroundColor: colors.success,
                        borderWidth: 2,
                        borderColor: colors.success,
                        borderRadius: 15,
                        marginBottom: 10,
                      }}>
                      <Icon
                        type="MaterialIcons"
                        name="check"
                        style={{fontSize: 20, color: '#fff'}}
                        size={20}
                      />
                    </View>
                  )}
                  {i === currentStep /* Selected */ && (
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        width: 30,
                        height: 30,
                        backgroundColor: 'gray',
                        borderWidth: 2,
                        borderColor: 'gray',
                        borderRadius: 15,
                        marginBottom: 10,
                      }}>
                      <Text style={{fontSize: 13, color: '#ffffff'}}>
                        {i + 1}
                      </Text>
                    </View>
                  )}
                  <StyledText
                    type={i === currentStep ? 'bold' : 'regular'}
                    style={{fontSize: 11}}>
                    {label}
                  </StyledText>
                </View>
              ))}
            </View>
          </View>
        </View>

        <View style={{flex: 1, backgroundColor: '#fff'}}>
          {currentStep === 0 && (
            <ShippingInfos
              setShippingInfos={infos => {
                this.setState(prevState => ({
                  form_datas: {
                    ...prevState.form_datas,
                    shipping_infos: infos,
                  },
                }));
              }}
            />
          )}
          {currentStep === 1 && (
            <PaymentMethods
              payment_methods={this.state.payment_methods}
              setPaymentMethod={method => {
                this.setState(
                  prevState => ({
                    form_datas: {
                      ...prevState.form_datas,
                      payment_method: method,
                    },
                  }),
                  () => {
                    this.setState({currentStep: currentStep + 1});
                  },
                );
              }}
            />
          )}
          {currentStep === 2 && (
            <ConfirmTransaction
              form_datas={this.state.form_datas}
              params={this.props.route.params}
              setTransactionRef={t_ref => {
                this.setState(prevState => ({
                  form_datas: {
                    ...prevState.form_datas,
                    t_ref,
                  },
                }));
              }}
            />
          )}
          {currentStep === 3 && (
            <FinalOrderStep
              form_datas={this.state.form_datas}
              pending={this.state.pending}
              order_status={this.state.order_status}
              navigation={this.props.navigation}
            />
          )}

          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            {currentStep > 0 ? (
              <TouchableOpacity
                style={[
                  styles.centerElement,
                  {
                    left: 10,
                    bottom: 10,
                    paddingRight: 15,
                    width: 120,
                    height: 42,
                    backgroundColor: '#3F51B5',
                    elevation: 10,
                    borderRadius: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  },
                ]}
                onPress={() => {
                  if (currentStep > 0) {
                    this.setState({currentStep: currentStep - 1});
                  }
                }}>
                <Icon
                  style={{color: '#fff'}}
                  type="MaterialIcons"
                  name="arrow-left"
                />
                <StyledText style={{color: '#fff', fontSize: 15}}>
                  Précédent
                </StyledText>
              </TouchableOpacity>
            ) : (
              <Text> </Text>
            )}
            {currentStep === 0 && (
              <TouchableOpacity
                style={[
                  styles.centerElement,
                  {
                    bottom: 10,
                    right: 10,
                    paddingHorizontal: 15,
                    width: 120,
                    height: 42,
                    backgroundColor: '#3F51B5',
                    elevation: 10,
                    borderRadius: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  },
                ]}
                onPress={() => {
                  if (currentStep + 1 < steps.length) {
                    this.setState({currentStep: currentStep + 1});
                  }
                }}>
                <StyledText
                  style={{color: '#fff', fontSize: 15, paddingLeft: 6}}>
                  Suivant
                </StyledText>
                <Icon
                  style={{color: '#fff'}}
                  type="MaterialIcons"
                  name="arrow-right"
                />
              </TouchableOpacity>
            )}
            {currentStep === 2 && (
              <TouchableOpacity
                style={[
                  styles.centerElement,
                  {
                    bottom: 10,
                    right: 10,
                    paddingHorizontal: 15,
                    width: 150,
                    height: 42,
                    backgroundColor: '#3F51B5',
                    elevation: 10,
                    borderRadius: 10,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  },
                ]}
                onPress={() => {
                  this.validOrder();
                  console.log('Finish !');
                }}>
                <StyledText
                  style={{color: '#fff', fontSize: 15, paddingLeft: 6}}>
                  Valider la commande
                </StyledText>
                <Icon
                  style={{color: '#fff'}}
                  type="MaterialIcons"
                  name="arrow-right"
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default withDispatch(withTheme(CheckoutScreen));

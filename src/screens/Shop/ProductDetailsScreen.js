/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React from 'react';

import {useTheme} from '@react-navigation/native';

import {useDispatch} from 'react-redux';

import {Button, Icon, Content, Footer, Toast} from 'native-base';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  PixelRatio,
  Dimensions,
  ToastAndroid,
} from 'react-native';

import FastImage from 'react-native-fast-image';

import {CartProvider} from '../../services/CartService';

import {getCartAction} from '../../redux/actions/Cart';

import StyledText from '../../components/StyledText';

import ImageView from 'react-native-image-viewing';
import {useSelector} from 'react-redux';
import CurrencyHelpers from '../../helpers/CurrencyHelpers';

const ProductDetailsScreen = ({route, navigation}) => {
  const {colors} = useTheme();
  const dispatch = useDispatch();
  const getCategories = useSelector(state => state.products_categories);
  const {products_categories = []} = getCategories;

  const product = {
    ...route.params.product,
    category: products_categories?.find(
      c => c.id === route.params.product?.category_ref,
    ),
  };

  const [modalOpen, setModalOpen] = React.useState(false);
  const [sliderCurrentIndex, setSliderCurrentIndex] = React.useState(0);

  const [factor, setFactor] = React.useState(1);

  const add_to_cart = () => {
    if (!product.quantity) {
      ToastAndroid.showWithGravity(
        'Le stock est épuisé pour ce produit',
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
      return;
    }
    CartProvider.add_to_cart(product, factor).then(() => {
      ToastAndroid.showWithGravity(
        'Article ajouté au panier !',
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
      dispatch(getCartAction());
    });
  };

  const add = () => {
    if (factor < product.quantity) {
      setFactor(f => f + 1);
    }
  };

  const reduce = () => {
    setFactor(f => (f > 1 ? f - 1 : 1));
  };

  return (
    <View style={{flex: 1}}>
      <ImageView
        imageIndex={sliderCurrentIndex}
        onImageIndexChange={setSliderCurrentIndex}
        images={product.images?.map(uri => ({uri}))}
        visible={modalOpen}
        onRequestClose={() => setModalOpen(false)}
        FooterComponent={() => (
          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <StyledText style={{color: '#FFF'}}>
              {sliderCurrentIndex + 1} / {product.images?.length}
            </StyledText>
          </View>
        )}
      />
      <View style={{flex: 0.75, backgroundColor: colors.primary, padding: 10}}>
        <View>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="west"
              style={{color: colors.specialText + 'DD'}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            margin: 15,
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <StyledText
              type="regular"
              style={{color: colors.specialText, fontSize: 13}}>
              {product.category.title}
            </StyledText>
            <StyledText
              type="bold"
              style={{color: colors.specialText, fontSize: 18}}>
              {product.title}
            </StyledText>
          </View>
          <View>
            <StyledText
              type="regular"
              style={{color: colors.specialText, fontSize: 13}}>
              Prix
            </StyledText>
            <StyledText
              type="bold"
              style={{color: colors.specialText, fontSize: 18}}>
              {CurrencyHelpers.format_amount(product.price)} FCFA
            </StyledText>
          </View>
          <View>
            <View style={{flexDirection: 'row'}}>
              {[...new Array(5).fill('n')].map((t, index) => (
                <Icon
                  style={{
                    fontSize: 22,
                    color:
                      index <= product.note ? '#ffc30f' : colors.gray_light,
                  }}
                  key={index}
                  type="MaterialIcons"
                  name="star"
                />
              ))}
            </View>
          </View>
        </View>
      </View>

      <TouchableOpacity
        onPress={() => {
          setModalOpen(true);
        }}
        style={styles.image_container}>
        <FastImage
          style={styles.image}
          source={{
            cache: FastImage.cacheControl.immutable,
            uri: product.images[0],
          }}
        />
      </TouchableOpacity>

      <Content
        scrollEnabled
        style={{
          padding: 15,
          bottom: 15,
          borderTopLeftRadius: 15,
          borderTopRightRadius: 15,
          backgroundColor: colors.background,
        }}>
        {product.quantity ? (
          <StyledText
            type="regular"
            style={{
              color: colors.card.text,
            }}>
            {product.quantity} en stock
          </StyledText>
        ) : (
          <StyledText
            type="medium"
            style={{
              color: colors.danger,
            }}>
            stock épuisé
          </StyledText>
        )}
        <View style={{paddingTop: 40}}>
          <StyledText
            type="regular"
            style={{color: colors.gray, fontSize: 15, textAlign: 'justify'}}>
            {product.description}
          </StyledText>
        </View>
        <View
          style={{
            marginTop: 30,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={reduce}
            style={[
              styles.button_factor,
              {
                borderColor: colors.gray_light,
                backgroundColor: colors.background,
              },
            ]}>
            <StyledText style={{color: colors.gray, fontSize: 20}}>
              -
            </StyledText>
          </TouchableOpacity>
          <StyledText
            style={{paddingHorizontal: 5, color: colors.gray, fontSize: 16}}>
            {factor > 9 ? factor : '0' + factor}
          </StyledText>
          <TouchableOpacity
            onPress={add}
            style={[
              styles.button_factor,
              {
                borderColor: colors.gray_light,
                backgroundColor: colors.background,
              },
            ]}>
            <StyledText style={{color: colors.gray, fontSize: 20}}>
              +
            </StyledText>
          </TouchableOpacity>
        </View>
      </Content>

      <Footer
        style={{
          backgroundColor: colors.background,
          justifyContent: 'space-between',
          alignItems: 'center',
          marginHorizontal: 15,
          marginBottom: 10,
          elevation: 0,
        }}>
        <TouchableOpacity
          onPress={add_to_cart}
          style={[
            styles.add_to_cart_button,
            {
              borderColor: colors.primary,
              backgroundColor: colors.background,
            },
          ]}>
          <Icon
            style={{color: colors.primary}}
            type="MaterialIcons"
            name="add-shopping-cart"
          />
        </TouchableOpacity>
        <Button
          style={styles.buy_now_button}
          onPress={() => {
            if (product.quantity < factor) {
              ToastAndroid.showWithGravity(
                'Votre demande est supérieure au stock',
                ToastAndroid.LONG,
                ToastAndroid.CENTER,
              );
              return;
            }
            navigation.navigate('CheckoutScreen', {
              mode: 'direct',
              order: [
                {
                  ...product,
                  quantity: factor,
                },
              ],
              total_price: product.price * factor,
            });
          }}>
          <StyledText style={{color: colors.specialText}}>
            ACHETER MAINTENANT
          </StyledText>
        </Button>
      </Footer>
    </View>
  );
};

export default ProductDetailsScreen;

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 12,
    marginTop: 15,
    flex: 1,
  },
  backButtonView: {
    position: 'absolute',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  backIcon: {
    fontSize: 25,
    color: '#FFF',
  },
  image_container: {
    position: 'absolute',
    width: Dimensions.get('window').width / 2,
    height: Dimensions.get('window').width / 2,
    top: '18%',
    right: 10,
    zIndex: 2,
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 300,
    borderWidth: 2 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderColor: '#aaa',
  },
  button_factor: {
    borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderRadius: 12,
    width: 40,
    height: 35,
    justifyContent: 'center',
    alignItems: 'center',
  },
  add_to_cart_button: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    height: 50,
    borderRadius: 15,
    borderWidth: 3 / PixelRatio.getPixelSizeForLayoutSize(1),
  },
  buy_now_button: {
    flex: 1,
    marginLeft: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    height: 50,
  },
});

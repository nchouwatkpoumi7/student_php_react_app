/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable react/prop-types */
import * as React from 'react';
import {StyleSheet, TouchableOpacity, Alert} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Title,
  Icon,
  Toast,
} from 'native-base';
import {useTheme} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';

import {useDispatch} from 'react-redux';

import {getCartAction} from '../../redux/actions/Cart';

import StyledText from '../../components/StyledText';

import {CartProvider} from './../../services/CartService';
import CartTabView from './cart/CartTabView';

const CartScreen = ({navigation}) => {
  const {colors} = useTheme();
  const dispatch = useDispatch();

  useFocusEffect(
    React.useCallback(() => {
      CartProvider.read_all_items().then(() => {
        dispatch(getCartAction());
      });
    }, [dispatch]),
  );

  const clear_cart = () => {
    Alert.alert('INFORMATIONS', 'Voulez-vous vraiment vider votre panier ?', [
      {
        style: 'cancel',
        text: 'NON',
      },
      {
        style: 'default',
        text: 'OUI',
        onPress: () => {
          CartProvider.clear_cart().then(() => {
            dispatch(getCartAction());
            Toast.show({
              text: 'Votre panier a été vidé !',
              buttonText: 'OK',
              duration: 3000,
            });
          });
        },
      },
    ]);
  };

  return (
    <Container>
      <Header>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <Title>
            <StyledText
              type="medium"
              style={{fontSize: 15, color: colors.specialText}}>
              PANIER
            </StyledText>
          </Title>
        </Body>
        <Right>
          <TouchableOpacity onPress={clear_cart} style={{marginRight: 7}}>
            <Icon
              type="MaterialIcons"
              name="delete"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Right>
      </Header>
      <CartTabView colors={colors} navigation={navigation} />
    </Container>
  );
};

export default CartScreen;

const styles = StyleSheet.create({});

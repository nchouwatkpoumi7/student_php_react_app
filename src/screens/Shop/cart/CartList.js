/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
/* eslint-disable no-mixed-spaces-and-tabs */
import React, {useCallback} from 'react';
import {Alert, StyleSheet, FlatList} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import {Content, Footer, Button, Left, Right, Icon} from 'native-base';
import {useDispatch, useSelector} from 'react-redux';
import CartItem from '../../../components/Shop/Cart/CartItem';
import StyledText from '../../../components/StyledText';
import CurrencyHelpers from '../../../helpers/CurrencyHelpers';
import {CartProvider} from '../../../services/CartService';
import {getCartAction} from '../../../redux/actions/Cart';

const CartList = ({navigation}) => {
  const {colors} = useTheme();
  const getCart = useSelector(state => state.cart);
  const [TempCart, setTempCart] = React.useState([]);

  const dispatch = useDispatch();

  const add_to_cart = item => {
    const prevCart = [...TempCart];
    const index = prevCart.findIndex(c => c.id === item.id);
    prevCart[index].quantity++;
    setTempCart(prevCart);
  };

  const reduce_from_cart = item => {
    const prevCart = [...TempCart];
    const index = prevCart.findIndex(c => c.id === item.id);
    prevCart[index].quantity--;
    setTempCart(prevCart);
  };

  const remove_from_cart = item => {
    const prevCart = [...TempCart];
    const index = prevCart.findIndex(c => c.id === item.id);
    prevCart.splice(index, 1);
    setTempCart(prevCart);
  };

  useFocusEffect(
    React.useCallback(() => {
      if (!getCart.loading) {
        setTempCart(getCart.cart || []);
      }
    }, [getCart.cart, getCart.loading]),
  );

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('beforeRemove', () => {
      CartProvider.setCart(TempCart).then(() => {
        dispatch(getCartAction());
      });
    });

    return unsubscribe;
  }, [TempCart, dispatch, navigation]);

  return (
    <>
      <FlatList
        style={(styles.content, {color: colors.background})}
        data={TempCart}
        renderItem={({item, index}) => (
          <CartItem
            key={index}
            item={item}
            add_to_cart={() => add_to_cart(item)}
            reduce_from_cart={() => reduce_from_cart(item)}
            remove_from_cart={() => remove_from_cart(item)}
          />
        )}
      />
      <Footer
        style={{padding: 10, height: 80, backgroundColor: colors.background}}>
        <Left>
          <StyledText
            type="regular"
            style={{marginBottom: 3, color: colors.blackText}}>
            Total
          </StyledText>
          <StyledText style={{color: colors.blackText, fontSize: 18}}>
            {CurrencyHelpers.format_amount(
              TempCart?.length
                ? TempCart.map(item => item.price * item.quantity).reduce(
                    (total, x) => total + x,
                  )
                : 0,
            )}
            FCFA
          </StyledText>
        </Left>
        <Right>
          <Button
            onPress={() => {
              if (TempCart.length) {
                navigation.navigate('CheckoutScreen', {
                  mode: 'cart',
                  order: TempCart,
                  total_price: TempCart.map(
                    item => item.price * item.quantity,
                  ).reduce((total, x) => total + x, 0),
                });
              } else {
                Alert.alert('Informations', 'Votre panier est vide !');
              }
            }}
            rounded
            iconRight
            style={{
              paddingLeft: 15,
              backgroundColor: colors.primary,
            }}>
            <StyledText type="medium" style={{color: colors.specialText}}>
              Acheter Maintenant
            </StyledText>
            <Icon
              type="MaterialIcons"
              name="arrow-right"
              style={{color: colors.specialText}}
            />
          </Button>
        </Right>
      </Footer>
    </>
  );
};

export default CartList;

const styles = StyleSheet.create({
  content: {},
});

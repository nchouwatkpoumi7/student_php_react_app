import React, {useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {Content} from 'native-base';

import OrderHistoryItem from '../../../components/Shop/Cart/OrderHistoryItem';
import {useFocusEffect} from '@react-navigation/core';
import {useTheme} from '@react-navigation/native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const CartHistory = () => {
  const {colors} = useTheme();

  const [Orders, setOrders] = React.useState([]);

  useFocusEffect(
    React.useCallback(() => {
      const unsubscribe = firestore()
        .collection('walcom-orders')
        .where('user_id', '==', auth().currentUser.uid)
        .onSnapshot(snapshot => {
          setOrders(snapshot.docs.map(doc => ({id: doc.id, ...doc.data()})));
        });
      return () => {
        unsubscribe();
      };
    }, []),
  );

  return (
    <Content
      padder
      scrollEnabled
      style={[styles.content, {backgroundColor: colors.background}]}>
      {Orders.map((order, index) => (
        <OrderHistoryItem key={index} order={order} />
      ))}
    </Content>
  );
};

export default CartHistory;

const styles = StyleSheet.create({
  content: {},
});

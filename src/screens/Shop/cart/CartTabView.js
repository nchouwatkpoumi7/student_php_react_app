/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable react/prop-types */
import * as React from 'react';
import {TabView, SceneMap} from 'react-native-tab-view';
import {View, useWindowDimensions, PixelRatio} from 'react-native';
import {Button} from 'native-base';

import StyledText from '../../../components/StyledText';
import CartList from './CartList';
import CartHistory from './CartHistory';

const CartTabView = ({colors, navigation}) => {
  const layout = useWindowDimensions();

  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'first', title: 'COMMANDE EN COURS'},
    {key: 'second', title: 'HISTORIQUE'},
  ]);

  return (
    <TabView
      navigationState={{index, routes}}
      renderTabBar={props => (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          {routes.map(({key, title}, ind) => (
            <Button
              key={key}
              onPress={() => setIndex(ind)}
              full
              style={
                routes[index].key === key
                  ? {
                      flex: 1,
                      borderBottomColor: colors.specialText,
                      borderBottomWidth:
                        6 / PixelRatio.getPixelSizeForLayoutSize(1),
                    }
                  : {
                      flex: 1,
                    }
              }>
              <StyledText
                type={routes[index].key === key ? 'medium' : 'regular'}
                style={{color: '#FFF'}}>
                {title}
              </StyledText>
            </Button>
          ))}
        </View>
      )}
      renderScene={SceneMap({
        first: () => <CartList navigation={navigation} />,
        second: CartHistory,
      })}
      onIndexChange={setIndex}
      initialLayout={{width: layout.width}}
    />
  );
};

export default CartTabView;

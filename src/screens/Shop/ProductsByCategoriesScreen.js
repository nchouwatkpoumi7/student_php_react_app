/* eslint-disable react/prop-types */
import React from 'react';
import {useTheme} from '@react-navigation/native';

import {StyleSheet, TouchableOpacity} from 'react-native';
import {Left, Icon, Header, Body, Container, Content} from 'native-base';

import StyledText from '../../components/StyledText';
import ShopItemLongListCard from '../../components/Shop/ShopItemLongListCard';

import firestore from '@react-native-firebase/firestore';
import {ProductsProvider} from '../../services/ProductsService';

const ProductsByCategoriesScreen = ({navigation, route}) => {
  const {colors} = useTheme();
  const {category} = route.params;

  const [ProductsList, setProductsList] = React.useState([]);

  React.useEffect(() => {
    const unsubscribe = firestore()
      .collection('walcom-products')
      .where('category_ref', '==', category.id)
      .orderBy('updated_at', 'desc')
      .onSnapshot(query => {
        setProductsList(query.docs.map(doc => ({id: doc.id, ...doc.data()})));
      });
    return () => {
      unsubscribe();
    };
  }, [category.id]);

  return (
    <Container>
      <Header style={{backgroundColor: colors.primary}}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            {category.title}
          </StyledText>
        </Body>
      </Header>
      <Content scrollEnabled style={{paddingTop: 10}}>
        {ProductsList.map(product => (
          <ShopItemLongListCard
            key={product.id}
            item={product}
            navigation={navigation}
          />
        ))}
      </Content>
    </Container>
  );
};

export default ProductsByCategoriesScreen;

const styles = StyleSheet.create({});

/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {Button, Icon, Header, Content, Left, Body, Fab} from 'native-base';
import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  PixelRatio,
} from 'react-native';
import StyledText from '../../components/StyledText';

import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image';

const EntrepriseDetailsScreen = ({route, navigation}) => {
  const {project} = route.params;

  // console.log(entreprise);

  const {colors} = useTheme();

  return (
    <View style={{flex: 1}}>
      <Header
        style={{backgroundColor: colors.primary}}
        androidStatusBarColor={colors.primary}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            {project.title}
          </StyledText>
        </Body>
      </Header>
      <Content scrollEnabled style={{backgroundColor: colors.background}}>
        <FastImage
          style={{
            alignSelf: 'center',
            width: Dimensions.get('window').width,
            height: 150,
            borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
          }}
          source={require('../../assets/images/svg/draw_constr.png')}
        />
        <View style={{padding: 12}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flex: 1}}>
              <StyledText style={{fontSize: 20}}>
                Le title de la requete
              </StyledText>
              <StyledText type="medium" style={{fontSize: 14}}>
                22/09/2022
              </StyledText>
            </View>
            <View>
              <Button
                rounded
                icon
                style={{
                  marginBottom: 10,
                  paddingHorizontal: 5,
                  borderRadius: 20,
                  backgroundColor: colors.primary,
                }}
                onPress={() => {
                  navigation.navigate('ChatRoom', {
                    // chat: {
                    //   ...project,
                    //   entreprise_id: project.id,
                    //   id: null,
                    // },
                  });
                }}>
                <Icon
                  style={{color: colors.specialText}}
                  type="MaterialIcons"
                  name="question-answer"
                />
              </Button>
            </View>
          </View>

          <StyledText
            type="medium"
            style={{
              marginTop: 10,
              textAlign: 'justify',
              lineHeight: 18,
              fontSize: 15,
              color: colors.text,
              opacity: 0.7,
            }}>
            Votre demande : --- Le lorem ipsum est, en imprimerie, une suite de
            mots sans signification utilisée à titre provisoire pour calibrer
            une mise en page, le texte définitif venant remplacer le faux-texte
            dès qu'il est prêt ou que la mise en page est achevée. Généralement,
            on utilise un texte en faux latin, le Lorem ipsum ou Lipsum
          </StyledText>

          <View style={{margin: 16}} />

          <Button
            hasText
            rounded
            style={{
              alignSelf: 'flex-end',
              paddingLeft: 15,
              borderRadius: 20,
              backgroundColor: colors.background,
              borderColor: colors.primary,
              borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
            }}
            onPress={() => navigation.navigate('EntrepriseDeepDetails')}>
            <StyledText style={{color: colors.primary}}>
              {"Voir la reponse de l'admin"}
            </StyledText>
            <Icon
              style={{color: colors.primary}}
              type="MaterialIcons"
              name="arrow-forward"
            />
          </Button>
        </View>
      </Content>
    </View>
  );
};

export default EntrepriseDetailsScreen;

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 12,
    marginTop: 15,
    flex: 1,
  },
  backButtonView: {
    position: 'absolute',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  backIcon: {
    fontSize: 25,
    color: '#FFF',
  },
});

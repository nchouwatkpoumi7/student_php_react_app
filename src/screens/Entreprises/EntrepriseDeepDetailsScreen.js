/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {
  Icon,
  Header,
  Content,
  Left,
  Body,
  Grid,
  Row,
  Col,
  Fab,
} from 'native-base';
import React from 'react';
import {StyleSheet, View, TouchableOpacity, PixelRatio} from 'react-native';
import StyledText from '../../components/StyledText';

import FastImage from 'react-native-fast-image';
import EntrepriseProjectCard from '../../components/Entreprises/EntrepriseProjectCard';
import CurrencyHelpers from '../../helpers/CurrencyHelpers';

const EntrepriseDeepDetailsScreen = ({route, navigation}) => {
  // const {entreprise} = route.params;

  // console.log(entreprise);

  const {colors} = useTheme();

  return (
    <View style={{flex: 1}}>
      <Header
        style={{backgroundColor: colors.primary}}
        androidStatusBarColor={colors.primary}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            {/* {entreprise.title} */}
            Nom de l'admin
          </StyledText>
        </Body>
      </Header>
      <Content
        scrollEnabled
        padder
        style={{backgroundColor: colors.background}}>
        <Grid
          style={{
            marginTop: 12,
            padding: 12,
            borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
            borderRadius: 4,
          }}>
          <Row
            style={{
              paddingBottom: 8,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <StyledText
              type="medium"
              style={{textTransform: 'uppercase', fontSize: 16}}>
              Reponse a votre requete
            </StyledText>
          </Row>
          <Row>
            <Col>
              <StyledText type="regular">Titre</StyledText>
            </Col>
            <Col>
              <StyledText type="medium">
                {/* {CurrencyHelpers.format_amount(entreprise.fiche.capital)} FCFA */}
                Le Titre de la requete
              </StyledText>
            </Col>
          </Row>
          <Row>
            <Col>
              <StyledText type="regular">Status</StyledText>
            </Col>
            <Col>
              <StyledText type="medium">
                {/* {entreprise.fiche.status} */}
                le status de la requete
              </StyledText>
            </Col>
          </Row>
          <Row>
            <Col>
              <StyledText type="regular">Date d'envoie</StyledText>
            </Col>
            <Col>
              <StyledText type="medium">
                {/* {entreprise.fiche.created_at} */}
                date d'envoie requete
              </StyledText>
            </Col>
          </Row>
          <Row>
            <Col>
              <StyledText type="regular">Date de création</StyledText>
            </Col>
            <Col>
              <StyledText type="medium">
                {/* {entreprise.fiche.created_at} */}
                date de reponse
              </StyledText>
            </Col>
          </Row>
        </Grid>

        <View style={{marginVertical: 20}}>
          <StyledText
            type="regular"
            style={{
              fontSize: 15,
            }}>
            Projets réalisés
          </StyledText>
          {/* {entreprise.projects?.liste.map((project, index) => (
            <EntrepriseProjectCard
              key={index}
              project={project}
              navigation={navigation}
            />
          ))} */}
          <StyledText
            type="regular"
            style={{
              fontSize: 16,
            }}>
            La reponse de l'admin : --- Le lorem ipsum est, en imprimerie, une
            suite de mots sans signification utilisée à titre provisoire pour
            calibrer une mise en page, le texte définitif venant remplacer le
            faux-texte dès qu'il est prêt ou que la mise en page est achevée.
            Généralement, on utilise un texte en faux latin, le Lorem ipsum ou
            Lipsum.
          </StyledText>
        </View>
      </Content>

      <Fab
        active
        direction="up"
        containerStyle={{}}
        style={{backgroundColor: colors.primary}}
        position="bottomRight"
        onPress={() => {
          navigation.navigate('ChatRoom', {
            // chat: {
            //   ...entreprise,
            //   entreprise_id: entreprise.id,
            //   id: null,
            // },
          });
        }}>
        <Icon type="MaterialIcons" name="question-answer" />
      </Fab>
    </View>
  );
};

export default EntrepriseDeepDetailsScreen;

const styles = StyleSheet.create({
  content: {
    marginHorizontal: 12,
    marginTop: 15,
    flex: 1,
  },
  backButtonView: {
    position: 'absolute',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  backIcon: {
    fontSize: 25,
    color: '#FFF',
  },
  text: {
    width: '100%',
    color: '#000',
  },
  nameText: {
    fontSize: 18,
  },
});

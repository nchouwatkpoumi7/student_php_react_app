/* eslint-disable react/prop-types */
import React from 'react';
import {useTheme} from '@react-navigation/native';
import {useSelector} from 'react-redux';

import {StyleSheet, TouchableOpacity} from 'react-native';
import {Left, Icon, Header, Body, Container, Content, View} from 'native-base';

import StyledText from '../../components/StyledText';
import EntrepriseCard from '../../components/Entreprises/EntrepriseCard';

const EntrepriseListByTypeScreen = ({navigation, route}) => {
  const {colors} = useTheme();
  const {service} = route.params;

  const getEntreprises = useSelector(state => state.entreprises);
  const {entreprises = []} = getEntreprises;

  const Entreprises = entreprises.filter(
    entreprise => entreprise.category_ref === service.id,
  );

  return (
    <Container>
      <Header
        style={{backgroundColor: colors.primary}}
        androidStatusBarColor={colors.primary}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body style={{flex: 2}}>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            {service.title}
          </StyledText>
        </Body>
      </Header>
      {!Entreprises.length ? (
        <View
          style={{
            flex: 1,
            marginBottom: 100,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <StyledText>Aucune societé enregistrée dans ce service...</StyledText>
        </View>
      ) : (
        <Content
          scrollEnabled={Entreprises.length !== 0}
          style={{paddingTop: 10}}>
          {Entreprises.map(entreprise => (
            <EntrepriseCard
              key={entreprise.id}
              entreprise={entreprise}
              navigation={navigation}
            />
          ))}
        </Content>
      )}
    </Container>
  );
};

export default EntrepriseListByTypeScreen;

const styles = StyleSheet.create({});

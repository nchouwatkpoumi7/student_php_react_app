/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import React, {useRef, useState} from 'react';
import {
  PixelRatio,
  StyleSheet,
  View,
  ScrollView,
  TouchableOpacity,
  BackHandler,
} from 'react-native';

import {useTheme} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import {TextInput} from 'react-native-gesture-handler';
import {
  Badge,
  Icon,
  Header,
  Left,
  Right,
  Container,
  Content,
  Body,
} from 'native-base';
import StyledText from '../components/StyledText';

import ShopItemListCard from '../components/Shop/ShopItemListCard';
import CategoryCardIcon from '../components/Shop/CategoryCardIcon';
import ShopScreenHeader from './Shop/Sections/ShopScreenHeader';

import {useSelector} from 'react-redux';
import ShopItemLongListCard from '../components/Shop/ShopItemLongListCard';

import {ProductsProvider} from './../services/ProductsService';

import firestore from '@react-native-firebase/firestore';

const ShopScreen = ({navigation}) => {
  const {colors} = useTheme();

  const [searchText, setSearchText] = useState('');
  const [ProductsList, setProductsList] = useState([]);

  // const getProducts = useSelector(state => state.products);
  const getCategories = useSelector(state => state.products_categories);
  // eslint-disable-next-line no-unused-vars
  // const {loading = true, products = []} = getProducts;

  const Articles = ProductsList.filter(
    product =>
      product.title?.toLowerCase().indexOf(searchText.toLowerCase()) !== -1,
  );

  const MostArticles = ProductsList.sort((a, b) => b.level - a.level).slice(
    0,
    4,
  );

  const inputRef = useRef(null);

  React.useEffect(() => {
    const unsubscribe = firestore()
      .collection('walcom-products')
      .orderBy('updated_at', 'desc')
      .onSnapshot(query => {
        setProductsList(query.docs.map(doc => ({id: doc.id, ...doc.data()})));
      });
    return () => {
      unsubscribe();
    };
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      const backEvent = BackHandler.addEventListener(
        'hardwareBackPress',
        () => {
          if (searchText) {
            setSearchText('');
            inputRef?.current.clear();
            return true;
          }

          return false;
        },
      );

      return () => {
        backEvent.remove();
      };
    }, [searchText]),
  );

  return (
    <Container>
      <ShopScreenHeader navigation={navigation} />
      <Content>
        <ScrollView>
          <View style={[styles.content, {backgroundColor: colors.background}]}>
            <View style={styles.searchSection}>
              <TextInput
                ref={inputRef}
                onChangeText={setSearchText}
                keyboardType="web-search"
                placeholder="Rechercher un produit"
                style={[styles.textInput, {borderColor: colors.light}]}
              />
              {searchText ? (
                <Icon
                  onPress={() => {
                    setSearchText('');
                    inputRef.current?.clear();
                  }}
                  type="MaterialIcons"
                  name="close"
                  style={styles.searchIcon}
                />
              ) : (
                <Icon
                  type="MaterialIcons"
                  name="search"
                  style={styles.searchIcon}
                />
              )}
            </View>

            <View style={searchText ? {marginTop: 10} : styles.mainSection}>
              {!searchText && (
                <>
                  <View style={styles.categoriesHeader}>
                    <StyledText type="regular" style={styles.topCategories}>
                      Categories disponibles
                    </StyledText>
                  </View>

                  <View style={styles.categoriesRow}>
                    {getCategories.products_categories?.map(
                      (category, index) => (
                        <CategoryCardIcon
                          navigation={navigation}
                          key={index}
                          category={{id: index, ...category}}
                        />
                      ),
                    )}
                  </View>

                  <View style={styles.productsSection}>
                    <View>
                      <StyledText
                        type="regular"
                        style={styles.productSectionTitle}>
                        Produits en vedette
                      </StyledText>
                    </View>

                    <ScrollView
                      showsHorizontalScrollIndicator={false}
                      horizontal
                      style={styles.scrollArticles}>
                      {MostArticles.map((item, index) => (
                        <ShopItemListCard
                          navigation={navigation}
                          key={index}
                          item={item}
                        />
                      ))}
                    </ScrollView>
                  </View>
                </>
              )}

              <View>
                {!searchText && (
                  <View style={{marginBottom: 5}}>
                    <StyledText
                      type="regular"
                      style={styles.productSectionTitle}>
                      Liste des articles disponibles
                    </StyledText>
                  </View>
                )}
                {Boolean(searchText) && (
                  <StyledText
                    type="italic"
                    style={{
                      color: colors.text,
                      textAlign: 'center',
                      fontSize: 12,
                      marginBottom: 8,
                    }}>
                    {(Articles.length && Articles.length < 10 ? '0' : '') +
                      Articles.length}{' '}
                    article(s) trouvé(s) pour{' '}
                    <StyledText style={{color: colors.text}}>
                      "{searchText}"
                    </StyledText>
                  </StyledText>
                )}
                {Articles.map((item, index) => (
                  <ShopItemLongListCard
                    navigation={navigation}
                    key={index}
                    item={item}
                  />
                ))}
              </View>
            </View>
          </View>
        </ScrollView>
      </Content>
    </Container>
  );
};

export default ShopScreen;

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  headerTitle: {
    fontSize: 22,
    left: 20,
    bottom: 20,
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
  headerImage: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
  },
  content: {
    paddingHorizontal: 10,
    paddingTop: 20,
  },
  searchSection: {
    // marginHorizontal: 10,
  },
  textInput: {
    paddingHorizontal: 25,
    borderWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    borderRadius: 10,
    height: 45,
  },
  searchIcon: {
    position: 'absolute',
    right: 12,
    top: '20%',
    opacity: 0.5,
  },
  mainSection: {
    marginTop: 22,
  },
  categoriesHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 8,
  },
  topCategories: {
    paddingLeft: 5,
    fontSize: 14,
  },
  seeMoreButton: {
    paddingLeft: 15,
    height: 35,
  },
  seeMoreButtonText: {
    paddingRight: 0,
  },
  categoriesRow: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  productsSection: {
    marginVertical: 15,
  },
  firstProductsSection: {
    marginTop: 15,
    marginBottom: 0,
  },
  productSectionTitle: {
    fontSize: 14,
    paddingLeft: 5,
  },
  scrollArticles: {
    marginTop: 4,
  },
});

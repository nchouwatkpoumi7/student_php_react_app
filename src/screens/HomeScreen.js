import React, {useState, Fragment} from 'react';
import {useTheme} from '@react-navigation/native';
import {
  Body,
  Button,
  Col,
  Container,
  Content,
  Grid,
  Icon,
  Left,
  ListItem,
  Row,
  View,
} from 'native-base';
import {
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import StyledText from '../components/StyledText';
import {SliderBox} from 'react-native-image-slider-box';

import {useSelector} from 'react-redux';

import ImageView from 'react-native-image-viewing';
import EntrepriseCard from '../components/Entreprises/EntrepriseCard';
import ServiceItem from '../components/Entreprises/ServiceItem';
import FastImage from 'react-native-fast-image';
import LocationInfosSection from '../components/Home/LocationInfosSection';
import AnimatedSearchBarSection from '../components/Home/AnimatedSearchBarSection';
import AnimatedOverlayContainer from '../components/Home/AnimatedOverlayContainer';
import {ItemListLoader} from './../helpers/skeleton-loader/ItemListLoader';
import {SliderLoader} from './../helpers/skeleton-loader/SliderLoader';
import {ServicesLoader} from './../helpers/skeleton-loader/ServicesLoader';
import ProjectCard from '../components/Projects/ProjectCard';

const HomeScreen = ({route, navigation}) => {
  const {colors} = useTheme();

  const [settingActive, setSettingActive] = useState(false);
  const [settingOpen, setSettingOpen] = useState(false);
  const [showSettings, setshowSettings] = useState(false);

  const [modalOpen, setModalOpen] = useState(false);
  const [sliderCurrentIndex, setSliderCurrentIndex] = useState(0);

  const [EntreprisesToDisplay, setEntreprisesToDisplay] = useState([]);
  const [TypesEntreprisesToDisplay, setTypesEntreprisesToDisplay] = useState(
    [],
  );

  const [searchText, setSearchText] = useState('');

  const getEntreprises = useSelector(state => state.entreprises);
  const {entreprises = []} = getEntreprises;

  const getAppConfigs = useSelector(state => state.app_configs);
  const {app_configs = {}} = getAppConfigs;
  console.log(getAppConfigs);

  const getServices = useSelector(state => state.services);
  const {services = []} = getServices;

  const Entreprises = entreprises.slice(0, 3);

  const Services = services.filter(p => p.in_home);

  return (
    <Container style={{backgroundColor: colors.background}}>
      <ImageView
        imageIndex={sliderCurrentIndex}
        images={app_configs['app-assets']?.slider_images.map(s => ({
          uri: s.image,
        }))}
        visible={modalOpen}
        onRequestClose={() => setModalOpen(false)}
        onImageIndexChange={setSliderCurrentIndex}
        FooterComponent={() => (
          <View style={styles.image_view_footer}>
            <StyledText style={{color: colors.specialText}}>
              {sliderCurrentIndex} / 4
            </StyledText>
          </View>
        )}
      />
      <StatusBar backgroundColor={colors.primary} />
      {settingOpen === false && <LocationInfosSection />}

      <AnimatedSearchBarSection
        searchText={searchText}
        setSearchText={setSearchText}
        settingActive={settingActive}
        setSettingActive={setSettingActive}
        settingOpen={settingOpen}
        setSettingOpen={setSettingOpen}
        setEntreprisesToDisplay={setEntreprisesToDisplay}
        setTypesEntreprisesToDisplay={setTypesEntreprisesToDisplay}
        setshowSettings={setshowSettings}
        Services={Services}
      />

      {settingActive === true && (
        <AnimatedOverlayContainer
          navigation={navigation}
          showSettings={showSettings}
          setshowSettings={setshowSettings}
          setSettingActive={setSettingActive}
          EntreprisesToDisplay={EntreprisesToDisplay}
          searchText={searchText}
          settingOpen={settingOpen}
        />
      )}

      <Content
        showsVerticalScrollIndicator={false}
        style={[
          styles.content,
          {
            backgroundColor: colors.background,
          },
        ]}>
        {searchText != null && searchText !== '' && searchText !== undefined ? (
          <View>
            <StyledText
              type="italic"
              style={{
                color: colors.text,
                textAlign: 'center',
                fontSize: 12,
                marginBottom: 8,
              }}>
              {(!!TypesEntreprisesToDisplay.length &&
              TypesEntreprisesToDisplay.length < 10
                ? '0'
                : '') + TypesEntreprisesToDisplay.length}{' '}
              service(s) trouvé(s) pour{' '}
              <StyledText style={{color: colors.text}}>
                "{searchText}"
              </StyledText>
            </StyledText>
            {TypesEntreprisesToDisplay.map(service => (
              <ListItem key={service.id} icon onPress={() => {}}>
                <Left>
                  <Button style={{backgroundColor: colors.ios}}>
                    <Icon active type="MaterialIcons" name={service.icon} />
                  </Button>
                </Left>
                <Body>
                  <StyledText type="regular">{service.title}</StyledText>
                </Body>
              </ListItem>
            ))}
          </View>
        ) : (
          <Fragment>
            {!Boolean(app_configs['app-assets']) && (
              <View style={{height: 180, flex: 1}}>
                <SliderLoader />
              </View>
            )}

            {!!Boolean(app_configs['app-assets']) && (
              <View style={styles.slider_container}>
                <SliderBox
                  autoplayInterval={6000}
                  ImageComponent={FastImage}
                  sliderBoxHeight={180}
                  ImageComponentStyle={styles.slider_image}
                  images={app_configs['app-assets']?.slider_images.map(
                    img => img.image,
                  )}
                  onCurrentImagePressed={index => {
                    setSliderCurrentIndex(index);
                    setModalOpen(true);
                  }}
                  autoplay
                  circleLoop
                />
              </View>
            )}
            <Grid style={styles.ServicesGrid}>
              <Row style={styles.align_center}>
                <Col>
                  <StyledText style={styles.top_services} type="regular">
                    Top services
                  </StyledText>
                </Col>
                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('Services');
                    }}
                    style={styles.seeMoreButton}>
                    <StyledText style={styles.seeMoreButtonText} type="regular">
                      Voir plus
                    </StyledText>
                  </TouchableOpacity>
                </Col>
              </Row>

              {getServices.loading ? (
                <Row style={{flex: 1, height: 80}}>
                  <ServicesLoader />
                </Row>
              ) : (
                <ScrollView
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  style={styles.services_scrollview}>
                  {Services.map((service, index) => (
                    <ServiceItem
                      key={index}
                      navigation={navigation}
                      service={service}
                    />
                  ))}
                </ScrollView>
              )}
            </Grid>
            {/* Enterprises Card */}
            <View style={styles.societyList}>
              {(getEntreprises.loading !== false || !Entreprises.length) && (
                <ItemListLoader />
              )}
              {Entreprises.map((entreprise, index) => (
                <EntrepriseCard
                  key={index}
                  navigation={navigation}
                  entreprise={entreprise}
                />
              ))}
              {/* {ProjectsToDisplay.map((project, index) => (
                <ProjectCard
                  key={index}
                  project={project}
                  navigation={navigation}
                />
              ))} */}
            </View>
          </Fragment>
        )}
      </Content>
    </Container>
  );
};
const ProjectsToDisplay = [
  {
    status: 'idle',
    location: 'Douala, Bonamoussadi',
    entreprise: {category: {icon: 'engineering'}},
    title: 'Hanniel Tatsabong',
    text: "je suis le text que l'utilisateur va entre a travers le formulaire",
    image:
      'http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/asteroid_blue.png',
  },
  {
    status: 'Progression',
    location: 'Moscou, Kremlin',
    entreprise: {category: {icon: 'engineering'}},
    title: 'Vladimir Putin',
    text: "je suis le text que l'utilisateur va entre a travers le formulaire",
    image:
      'http://commondatastorage.googleapis.com/codeskulptor-assets/lathrop/asteroid_blue.png',
  },
];
export default HomeScreen;

const styles = StyleSheet.create({
  align_center: {
    alignItems: 'center',
  },
  image_view_footer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    bottom: 10,
    marginHorizontal: 10,
  },
  slider_container: {
    width: Dimensions.get('window').width,
    right: 10,
  },
  slider_image: {
    borderRadius: 10,
    width: '96%',
  },
  ServicesGrid: {
    paddingTop: 8,
    alignItems: 'center',
  },
  top_services: {
    fontSize: 16,
  },
  seeMoreButton: {
    paddingLeft: 40,
    paddingRight: 8,
    paddingVertical: 8,
  },
  seeMoreButtonText: {
    textAlign: 'right',
    fontSize: 14,
    opacity: 0.5,
  },
  services_scrollview: {
    paddingVertical: 6,
  },
  societyList: {
    // marginTop: 10,
    // marginBottom: 10,
    marginVertical: 10,
  },
});

/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import {useTheme} from '@react-navigation/native';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Modal,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Content,
  Form,
  Toast,
} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';

import StyledText from '../../components/StyledText';
import {AuthProvider} from '../../providers/Auth';
import {useDispatch, useSelector} from 'react-redux';
import CustomStyledInput from '../../components/CustomStyledInput';
import {getCurrentUserAction} from '../../redux/actions/CurrentUser';

import ImageView from 'react-native-image-viewing';

const UpdateProfileScreen = ({navigation}) => {
  const {colors} = useTheme();
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(false);

  const getCurrentUser = useSelector(state => state.currentUser);
  const {currentUser} = getCurrentUser;

  const [displayName, setDisplayName] = useState(currentUser.name);
  const [phoneNumber, setPhoneNumber] = useState(currentUser.phone);
  const [photoURL, setPhotoURL] = useState(currentUser.photoURL);
  const [viewImage, setViewImage] = useState(false);

  const [modalVisible, setModalVisible] = useState(false);

  React.useEffect(() => {
    setDisplayName(currentUser.name);
    setPhoneNumber(currentUser.phone);
    return () => {};
  }, [currentUser]);

  const [error, setError] = useState({
    enabled: false,
    success: false,
    message: '',
  });

  const HandleUpdateProfile = async () => {
    if (
      !(currentUser.name != displayName || currentUser.phone != phoneNumber)
    ) {
      Toast.show({
        type: 'warning',
        text: 'Aucune modifications',
        buttonText: 'OK',
        duration: 4000,
      });
      return false;
    }

    if (currentUser.name != displayName || currentUser.phone != phoneNumber) {
      setLoading(true);
      AuthProvider.saveUserDatas(displayName, phoneNumber)
        .then(() => {
          setError({
            enabled: true,
            success: true,
            message: 'Votre profil a été mis à jour avec succès !',
          });
          dispatch(getCurrentUserAction());
        })
        .catch(e => {
          console.log(e);
          setError({
            enabled: true,
            success: false,
            message: 'Erreur lors de la mise à jour. Veuillez re-essayer !',
          });
        });
    }

    setLoading(false);
  };

  const handleImageSelection = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
      freeStyleCropEnabled: true,
    })
      .then(response => {
        if (response?.data) {
          setLoading(true);
          const uri = `data:image/*;base64,${response.data}`;
          AuthProvider.updateImage(uri)
            .then(() => {
              setPhotoURL(uri);
              dispatch(getCurrentUserAction());
            })
            .finally(() => {
              setLoading(false);
            });
        }
      })
      .catch(e => {
        console.log(e);
      });
    setModalVisible(false);
  };

  return (
    <Container style={{backgroundColor: colors.specialText}}>
      <ImageView
        images={[
          photoURL
            ? {uri: photoURL}
            : require('./../../assets/images/default.png'),
        ]}
        visible={viewImage}
        onRequestClose={() => setViewImage(false)}
      />
      <Modal
        visible={modalVisible}
        transparent
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.modalContent}>
          <View style={[{backgroundColor: colors.primary}, styles.errorModal]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setModalVisible(false);
              }}>
              <StyledText style={{color: colors.specialText}}>X</StyledText>
            </TouchableOpacity>
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              <Icon
                onPress={handleImageSelection}
                type="MaterialIcons"
                name="image"
                style={{
                  fontSize: 35,
                  color: colors.specialText,
                  marginVertical: 5,
                }}
              />
            </View>
          </View>
        </View>
      </Modal>

      <Modal visible={loading} transparent>
        <View style={styles.modalContent}>
          <View
            style={[{backgroundColor: colors.primary}, styles.loadingModal]}>
            <StyledText style={{color: colors.specialText, marginVertical: 5}}>
              Chargement...
            </StyledText>
            <ActivityIndicator size={30} color={colors.specialText} />
          </View>
        </View>
      </Modal>

      <Modal
        visible={error.enabled}
        transparent
        onRequestClose={() => {
          setError({enabled: false, message: ''});
        }}>
        <View style={styles.modalContent}>
          <View
            style={[
              {
                backgroundColor: error.success
                  ? colors.success
                  : colors.warning,
              },
              styles.errorModal,
            ]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setError({enabled: false, message: ''});
              }}>
              <StyledText style={{color: colors.specialText}}>X</StyledText>
            </TouchableOpacity>
            <StyledText style={{color: colors.specialText, marginVertical: 5}}>
              {error.message}
            </StyledText>
          </View>
        </View>
      </Modal>

      <Header style={{backgroundColor: colors.primary}}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            Mise a jour
          </StyledText>
        </Body>
        <Right>
          <TouchableOpacity
            activeOpacity={0.3}
            onPress={HandleUpdateProfile}
            style={[styles.checkButton]}>
            <Icon
              type="MaterialIcons"
              name="check"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Right>
      </Header>

      <Content>
        <View style={styles.imageEditing}>
          <TouchableOpacity
            onPress={() => {
              setViewImage(true);
            }}>
            <Image
              source={
                photoURL
                  ? {uri: photoURL}
                  : require('./../../assets/images/default.png')
              }
              style={styles.image}
            />
          </TouchableOpacity>
          <View
            style={{
              position: 'absolute',
              width: 130,
              height: 130,
              justifyContent: 'flex-end',
              alignItems: 'baseline',
              bottom: 10,
              right: 30,
            }}>
            <TouchableOpacity
              onPress={() => {
                setModalVisible(true);
              }}
              style={{
                padding: 8,
                borderRadius: 30,
                backgroundColor: colors.primary,
              }}>
              <Icon
                style={{color: colors.specialText}}
                type="MaterialIcons"
                name="camera"
              />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{flex: 1, paddingHorizontal: 10}}>
          <Form>
            <View style={{marginVertical: 15}}>
              <CustomStyledInput
                label="Votre nom"
                updatedValue={displayName}
                onChangeText={text => {
                  setDisplayName(text);
                }}
              />
            </View>

            <CustomStyledInput
              props={{
                keyboardType: 'numeric',
                textContentType: 'telephoneNumber',
                dataDetectorTypes: 'phoneNumber',
              }}
              label="Votre numéro de téléphone"
              updatedValue={phoneNumber}
              onChangeText={text => {
                setPhoneNumber(text);
              }}
            />

            <View style={{marginVertical: 20}} />
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default UpdateProfileScreen;

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(1, 1, 1, .5)',
  },
  loadingModal: {
    width: '50%',
    height: 100,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButton: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: 7,
    right: 5,
  },
  errorModal: {
    width: '70%',
    height: 90,
    paddingHorizontal: 15,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalGrid: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%',
  },
  backButton: {
    paddingLeft: 5,
    paddingRight: 10,
    paddingVertical: 7,
  },
  checkButton: {
    paddingRight: 8,
    paddingLeft: 30,
    paddingVertical: 12,
  },
  imageEditing: {
    alignItems: 'center',
    marginVertical: 25,
  },
  image: {
    width: 120,
    height: 120,
    borderRadius: 60,
  },
  cameraButton: {
    position: 'absolute',
    right: '27%',
    top: '60%',
    paddingVertical: 20,
    width: 50,
    height: 50,
  },
});

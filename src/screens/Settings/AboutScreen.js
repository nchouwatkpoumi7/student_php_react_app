/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import {useTheme} from '@react-navigation/native';
import {Linking, StyleSheet, TouchableOpacity} from 'react-native';
import {
  Container,
  Header,
  Left,
  Body,
  Icon,
  Content,
  Card,
  CardItem,
  View,
} from 'native-base';

import StyledText from '../../components/StyledText';
import {useSelector} from 'react-redux';

const AboutScreen = ({navigation}) => {
  const {colors} = useTheme();

  const getAppConfigs = useSelector(state => state.app_configs);
  const {app_configs} = getAppConfigs;

  return (
    <Container style={{backgroundColor: colors.specialText}}>
      <Header style={{backgroundColor: colors.primary}}>
        <Left>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.backButton}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{color: colors.specialText}}
            />
          </TouchableOpacity>
        </Left>
        <Body>
          <StyledText
            type="regular"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            A PROPOS DE NOUS
          </StyledText>
        </Body>
      </Header>

      <Content padder style={{backgroundColor: colors.background}}>
        <Card>
          <CardItem>
            <View style={{flex: 1}}>
              <View style={{alignItems: 'center', marginBottom: 20}}>
                <StyledText>{app_configs['about-us']?.our_name}</StyledText>
                <StyledText type="regular">
                  {app_configs['about-us']?.email}
                </StyledText>
              </View>
              <View
                style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(app_configs['about-us']?.whatsapp_link);
                  }}
                  style={[styles.contact_button]}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="mail"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(app_configs['about-us']?.whatsapp_link);
                  }}
                  style={[styles.contact_button]}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="facebook"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(app_configs['about-us']?.phone_link);
                  }}
                  style={[styles.contact_button]}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="phone"
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => {
                    Linking.openURL(app_configs['about-us']?.website);
                  }}
                  style={[styles.contact_button]}>
                  <Icon
                    style={{color: colors.primary}}
                    type="MaterialIcons"
                    name="language"
                  />
                </TouchableOpacity>
              </View>
            </View>
          </CardItem>
        </Card>
        <View padder>
          <View style={{marginVertical: 10}}>
            <StyledText type="regular">Téléphone</StyledText>
            <StyledText style={{fontSize: 18}}>
              {app_configs['about-us']?.phone_number}
            </StyledText>
          </View>
          <View style={{marginBottom: 10}}>
            <StyledText type="regular">Facebook</StyledText>
            <StyledText style={{fontSize: 18}}>
              {app_configs['about-us']?.facebook}
            </StyledText>
          </View>
          <View style={{marginBottom: 20}}>
            <StyledText type="regular">Site web</StyledText>
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(app_configs['about-us']?.website);
              }}>
              <StyledText style={{color: colors.ios, fontSize: 18}}>
                {app_configs['about-us']?.website}
              </StyledText>
            </TouchableOpacity>
          </View>
          <View style={{marginBottom: 10}}>
            {/* <StyledText type="regular">Facebook</StyledText> */}
            <StyledText
              type="regular"
              style={{fontSize: 18, textAlign: 'justify'}}>
              Nous avons sogneusement conçu et perfectionné tous nos processus
              de constuction, guidés par notre engagement envers le succès de
              nos clients et nous nous efforçons de continuellement tirer parti
              des meilleures pratiques, équipements et technologies disponibles.
            </StyledText>
          </View>
        </View>
      </Content>
    </Container>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  backButton: {
    paddingLeft: 5,
    paddingRight: 10,
    paddingVertical: 7,
  },
  contact_button: {
    width: 50,
    height: 50,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    // elevation: 6,
  },
});

/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import {
  StyleSheet,
  Image,
  Modal,
  TouchableOpacity,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import {View, Button, Input, Form, Item, Icon} from 'native-base';
import StyledText from '../../components/StyledText';
import {useTheme} from '@react-navigation/native';
import {AuthProvider} from '../../providers/Auth';

export default function ForgotPasswordScreen({navigation}) {
  const {colors} = useTheme();

  const [email, setEmail] = useState('');

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState({enabled: false, message: ''});

  const ValidateEmail = mail => {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      mail,
    );
  };

  // Handle create account button press
  async function loginAccount() {
    if (!email) {
      setError({enabled: true, message: 'Veullez remplir tous les champs !'});
      return false;
    }

    if (!ValidateEmail(email)) {
      setError({enabled: true, message: "Format d'adresse mail invalide !"});
      return false;
    }

    setLoading(true);

    AuthProvider.resetPassword(email)
      .then(() => {
        setLoading(false);

        setError({
          enabled: true,
          message:
            'Un mail de réinitialisation de mot de passe vous a été envoyé. Veuillez consulter votre boîte de réception !',
          severity: 'success',
        });
      })
      .catch(e => {
        setLoading(false);

        let message = e.message;
        if (e.code === 'auth/user-not-found') {
          message =
            'Utilisateur inexistant ! Veuillez créer un nouveau compte.';
        }

        setError({enabled: true, message});
      });
  }

  return (
    <ScrollView style={{flex: 1, backgroundColor: '#FFF', paddingTop: 15}}>
      <Modal
        visible={error.enabled}
        transparent
        onRequestClose={() => {
          setError({enabled: false, message: ''});
        }}>
        <View style={styles.modalContent}>
          <View
            style={[
              {
                backgroundColor:
                  error.severity === 'success'
                    ? colors.success
                    : colors.warning,
              },
              styles.errorModal,
            ]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setError({enabled: false, message: ''});
              }}>
              <StyledText
                type="medium"
                style={{fontSize: 16, color: colors.specialText}}>
                X
              </StyledText>
            </TouchableOpacity>
            <StyledText
              type="regular"
              style={{
                lineHeight: 18,
                fontSize: 14,
                textAlign: 'justify',
                color: colors.specialText,
                marginVertical: 10,
              }}>
              &nbsp;{error.message}
            </StyledText>
          </View>
        </View>
      </Modal>

      <View style={{}}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          style={{
            backgroundColor: colors.background,
            flexDirection: 'row',
            alignItems: 'center',
            alignSelf: 'flex-start',
            paddingHorizontal: 10,
            paddingBottom: 15,
            borderWidth: 0,
            borderColor: colors.background,
          }}>
          <Icon
            style={{color: colors.gray}}
            type="MaterialIcons"
            name="keyboard-backspace"
          />

          <StyledText type="medium" style={{color: colors.gray, fontSize: 15}}>
            Retour
          </StyledText>
        </TouchableOpacity>
      </View>

      <View style={{flex: 1, marginRight: 15, marginTop: 40}}>
        <View
          style={{
            flex: 1,
            height: 150,
            justifyContent: 'center',
            marginLeft: 15,
          }}>
          <StyledText
            type="medium"
            style={{color: colors.text, fontSize: 20, paddingBottom: 10}}>
            Mot de passe oublié ?
          </StyledText>
          <StyledText type="regular" style={{color: colors.gray}}>
            Entrez l'adresse e-mail associée à votre compte et nous vous
            enverons un mail contenant les instructions pour réinitialiser votre
            mot de passe.
          </StyledText>
        </View>
        <Form>
          <Item style={{borderBottomWidth: 0}}>
            <Icon
              type="MaterialIcons"
              name="person"
              style={{
                zIndex: 2,
                position: 'absolute',
                left: 12,
                color: colors.gray,
              }}
            />
            <Input
              placeholderTextColor={colors.placeholder}
              placeholder="Adresse mail"
              style={{
                color: colors.gray,
                // fontWeight: 'bold',
                paddingLeft: 50,
                borderColor: colors.gray,
                borderWidth: 1 / 2,
                borderRadius: 10,
                backgroundColor: '#FFF',
                overflow: 'hidden',
                margin: 0,
              }}
              onChangeText={setEmail}
            />
          </Item>
        </Form>

        <View style={{marginLeft: 13, marginTop: 8}}>
          <Button
            disabled={loading}
            rounded={!loading}
            // bordered
            style={[
              styles.button,
              {
                borderWidth: loading ? 0 : 1 / 5,
                borderColor: colors.primary,
                backgroundColor: loading ? colors.secondary : colors.primary,
              },
            ]}
            onPress={() => {
              loginAccount();
            }}>
            {!loading ? (
              <StyledText
                style={[styles.buttonText, {color: colors.specialText}]}>
                Envoyer le lien de récupération
              </StyledText>
            ) : (
              <ActivityIndicator color={colors.specialText} size={25} />
            )}
          </Button>
          <View
            style={{
              marginTop: 30,
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <StyledText type="regular" style={{color: colors.placeholder}}>
              Pas encore membre ?{' '}
            </StyledText>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('RegisterScreen');
              }}>
              <StyledText style={{color: colors.secondary}}>
                Inscrivez-vous
              </StyledText>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(1, 1, 1, .5)',
  },
  loadingModal: {
    width: '50%',
    height: 100,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButton: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: 7,
    right: 5,
  },
  errorModal: {
    width: '72%',
    height: 125,
    paddingHorizontal: 4,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonsSection: {
    flex: 1,
    marginVertical: 20,
    justifyContent: 'flex-end',
  },
  button: {
    borderRadius: 10,
    marginVertical: 6,
    height: 50,
    alignSelf: 'center',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    width: '100%',
    textAlign: 'center',
  },
});

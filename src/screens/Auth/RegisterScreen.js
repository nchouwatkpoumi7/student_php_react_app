/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import {
  StyleSheet,
  Image,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
  ScrollView,
  Dimensions,
} from 'react-native';
import {Content, View, Button, Input, Form, Item, Icon} from 'native-base';
import StyledText from '../../components/StyledText';
import {useTheme} from '@react-navigation/native';
import {AuthProvider} from '../../providers/Auth';

export default function RegisterScreen({navigation}) {
  const {colors} = useTheme();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [passwordVisible, setPasswordVisible] = useState(false);

  const [loading, setLoading] = useState(false);

  const [error, setError] = useState({enabled: false, message: ''});

  const ValidateEmail = mail => {
    return /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      mail,
    );
  };

  async function createAccount() {
    if (!email.replace(/ /g, '') || !password || !confirmPassword) {
      setError({enabled: true, message: 'Veullez remplir tous les champs !'});
      return false;
    }

    if (!ValidateEmail(email.replace(/ /g, ''))) {
      setError({enabled: true, message: "Format d'adresse mail invalide !"});
      return false;
    }

    if (password.length < 6) {
      setError({
        enabled: true,
        message: 'Le mot de passe doit comporter au moins 6 caractères !',
      });
      return false;
    }

    if (confirmPassword !== password) {
      setError({
        enabled: true,
        message: 'Le mot de passe et sa confirmation doivent être identiques !',
      });
      return false;
    }

    setLoading(true);

    AuthProvider.register(email.replace(/ /g, ''), password)
      .then(() => {
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);

        console.log(e);
        let message = e.message;
        if (e.code === 'auth/email-already-in-use') {
          message = 'Cette adresse Email est déjà utilisée !';
        } else if (e.code === 'auth/invalid-email') {
          message = 'Veuillez entrer une adresse mail valide !';
        } else if (e.code === 'auth/weak-password') {
          message = 'Le mot de passe doit comporter au moins 6 caractères !';
        } else if (e.code === 'auth/network-request-failed') {
          message = 'Look ta connexion internet Molah';
        }

        setError({enabled: true, message});
      });
  }

  const inputStyles = {
    field: {
      color: colors.gray,
      paddingLeft: 50,
      borderColor: colors.gray,
      borderWidth: 1 / 2,
      borderRadius: 10,
      backgroundColor: '#FFF',
      overflow: 'hidden',
      margin: 0,
    },
    icon: {
      zIndex: 2,
      position: 'absolute',
      left: 12,
      color: colors.gray,
    },
  };

  return (
    <View style={{flex: 1, backgroundColor: '#FFF'}}>
      <View
        style={{
          position: 'absolute',
          width: '100%',
          height: 100,
          top: 0,
          left: 0,
          zIndex: 999,
          backgroundColor: colors.background,
        }}>
        <Image
          style={{
            bottom: 1.3,
            height: Dimensions.get('window').width / 1.3 / 2.25,
            width: Dimensions.get('window').width / 1.3,
          }}
          source={require('./../../assets/images/svg/curve-1.png')}
        />
      </View>
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.background,
          paddingTop: 50,
        }}>
        <Modal
          visible={error.enabled}
          transparent
          onRequestClose={() => {
            setError({enabled: false, message: ''});
          }}>
          <View style={styles.modalContent}>
            <View
              style={[{backgroundColor: colors.warning}, styles.errorModal]}>
              <TouchableOpacity
                style={styles.modalCloseButton}
                onPress={() => {
                  setError({enabled: false, message: ''});
                }}>
                <StyledText style={{color: colors.specialText}}>X</StyledText>
              </TouchableOpacity>
              <StyledText
                style={{color: colors.specialText, marginVertical: 5}}>
                {error.message}
              </StyledText>
            </View>
          </View>
        </Modal>

        <View
          style={{
            marginBottom: 50,
            flexDirection: 'row',
            alignItems: 'center',
          }}>
          <View
            style={{
              backgroundColor: colors.primary,
              borderRadius: 10,
              width: 50,
              height: 40,
              justifyContent: 'center',
              alignItems: 'center',
              marginRight: 3,
            }}>
            <StyledText style={{color: colors.specialText, fontSize: 28}}>
              M
            </StyledText>
          </View>
          <StyledText style={{fontSize: 28}}>ATCOM</StyledText>
        </View>

        <View style={{width: '97%'}}>
          <Form>
            <Item
              style={{right: 7.5, borderBottomWidth: 0, marginVertical: 10}}>
              <Icon
                type="MaterialIcons"
                name="email"
                style={inputStyles.icon}
              />
              <Input
                placeholderTextColor={colors.placeholder}
                placeholder="Adresse mail"
                style={inputStyles.field}
                onChangeText={setEmail}
              />
            </Item>
            <Item style={{right: 7.5, borderBottomWidth: 0}} last>
              <Icon
                type="MaterialIcons"
                name="lock"
                style={[
                  inputStyles.icon,
                  {
                    left: 28,
                  },
                ]}
              />
              <Input
                placeholderTextColor={colors.placeholder}
                textContentType="password"
                secureTextEntry={!passwordVisible}
                placeholder="Mot de passe"
                style={inputStyles.field}
                onChangeText={setPassword}
              />
              <TouchableOpacity
                style={{position: 'absolute', right: 10}}
                onPress={() => {
                  setPasswordVisible(visible => !visible);
                }}>
                <Icon
                  type="MaterialIcons"
                  name={passwordVisible ? 'visibility-off' : 'visibility'}
                  style={{color: colors.gray, fontSize: 23}}
                />
              </TouchableOpacity>
            </Item>
            <Item
              style={{right: 7.5, borderBottomWidth: 0, marginVertical: 10}}
              last>
              <Icon
                type="MaterialIcons"
                name="lock"
                style={[
                  inputStyles.icon,
                  {
                    left: 28,
                  },
                ]}
              />
              <Input
                placeholderTextColor={colors.placeholder}
                textContentType="password"
                secureTextEntry={!passwordVisible}
                placeholder="Confirmer mot de passe"
                style={inputStyles.field}
                onChangeText={setConfirmPassword}
              />
              <TouchableOpacity
                style={{position: 'absolute', right: 10}}
                onPress={() => {
                  setPasswordVisible(visible => !visible);
                }}>
                <Icon
                  type="MaterialIcons"
                  name={passwordVisible ? 'visibility-off' : 'visibility'}
                  style={{color: colors.gray, fontSize: 23}}
                />
              </TouchableOpacity>
            </Item>
          </Form>

          <View style={{marginTop: 5}}>
            <Button
              disabled={loading}
              rounded={!loading}
              style={[
                styles.button,
                {
                  borderWidth: loading ? 0 : 1 / 5,
                  borderColor: colors.primary_border,
                  backgroundColor: loading ? colors.secondary : colors.primary,
                },
              ]}
              onPress={createAccount}>
              {!loading ? (
                <StyledText
                  style={[styles.buttonText, {color: colors.specialText}]}>
                  S'inscrire maintenant
                </StyledText>
              ) : (
                <ActivityIndicator color={colors.specialText} size={25} />
              )}
            </Button>
            <View
              style={{
                marginTop: 20,
                flexDirection: 'row',
                justifyContent: 'center',
              }}>
              <StyledText type="regular" style={{color: colors.gray}}>
                Déjà membre ?{' '}
              </StyledText>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('LoginScreen');
                }}>
                <StyledText style={{color: colors.secondary}}>
                  Connectez-vous
                </StyledText>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(1, 1, 1, .5)',
  },
  loadingModal: {
    width: '50%',
    height: 100,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButton: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: 7,
    right: 5,
  },
  errorModal: {
    width: '70%',
    height: 90,
    paddingHorizontal: 15,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonsSection: {
    flex: 1,
    marginVertical: 20,
    justifyContent: 'flex-end',
  },
  button: {
    borderRadius: 10,
    marginVertical: 6,
    height: 50,
    alignSelf: 'center',
    width: '97%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonText: {
    width: '100%',
    textAlign: 'center',
  },
});

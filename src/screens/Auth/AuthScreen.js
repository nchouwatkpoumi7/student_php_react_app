/* eslint-disable react/prop-types */
import React, {useEffect} from 'react';
import {StyleSheet, Image} from 'react-native';
import {View, Button} from 'native-base';
import StyledText from '../../components/StyledText';
import {useTheme} from '@react-navigation/native';

const AuthScreen = ({navigation}) => {
  const {colors} = useTheme();

  useEffect(() => {
    return () => {};
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <StyledText
          type="regular"
          style={{
            textAlign: 'center',
            fontSize: 18,
            color: colors.gray,
          }}>
          Bienvenue sur
        </StyledText>
        <StyledText
          type="medium"
          style={{
            textAlign: 'center',
            fontSize: 33,
            color: colors.primary,
          }}>
          MatCom Company
        </StyledText>
      </View>
      <Image
        style={{alignSelf: 'center', width: '98%', height: '55%'}}
        source={require('./../../assets/images/svg/draw_constr.png')}
      />
      <View style={styles.buttonsSection}>
        <Button
          bordered
          // rounded
          style={[
            styles.button,
            {
              backgroundColor: colors.primary,
              borderColor: colors.primary_border,
            },
          ]}
          onPress={() => {
            navigation.navigate('LoginScreen');
          }}>
          <StyledText style={[styles.buttonText, {color: colors.specialText}]}>
            Se connecter
          </StyledText>
        </Button>
        <Button
          bordered
          // rounded
          style={[
            styles.button,
            {
              borderColor: colors.secondary_border,
              backgroundColor: colors.secondary,
            },
          ]}
          onPress={() => {
            navigation.navigate('RegisterScreen');
          }}>
          <StyledText type='small' style={[styles.buttonText, {color: colors.specialText}]}>
            {"S'inscrire"}
          </StyledText>
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imageBackground: {
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonsSection: {
    flex: 1,
    marginVertical: 20,
    justifyContent: 'flex-end',
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 6,
    alignSelf: 'center',
    height: 52,
    borderRadius: 10,
  },
  buttonText: {
    width: '85%',
    textAlign: 'center',
  },
});

export default AuthScreen;

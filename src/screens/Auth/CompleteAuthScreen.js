/* eslint-disable react/no-unescaped-entities */
/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import React, {useState} from 'react';
import {useTheme} from '@react-navigation/native';

import {
  StyleSheet,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
  ScrollView,
} from 'react-native';

import {Content, View, Button, Input, Form, Item, Icon} from 'native-base';

import {AuthProvider} from '../../providers/Auth';

import StyledText from '../../components/StyledText';

export default function CompleteAuthScreen({navigation}) {
  const {colors} = useTheme();

  const [username, setUsername] = useState('');
  const [phone, setPhone] = useState('');

  const [loading, setLoading] = useState(false);

  const [error, setError] = useState({enabled: false, message: ''});

  async function saveUserDatas() {
    if (!username || !phone) {
      setError({enabled: true, message: 'Veullez remplir tous les champs !'});
      return false;
    }

    setLoading(true);

    AuthProvider.saveUserDatas(username, phone)
      .then(() => {
        setLoading(false);
      })
      .catch(e => {
        setLoading(false);
        console.log(e);
        let message = e.message;
        if (e.code === 'auth/email-already-in-use') {
          message = 'Cette adresse Email est déjà utilisée !';
        } else if (e.code === 'auth/invalid-email') {
          message = 'Veuillez entrer une adresse mail valide !';
        } else if (e.code === 'auth/weak-password') {
          message = 'Le mot de passe doit comporter au moins 6 caractères !';
        }

        setError({enabled: true, message});
      });
  }

  const inputStyles = {
    field: {
      color: colors.gray,
      paddingLeft: 50,
      borderColor: colors.gray,
      borderWidth: 1 / 2,
      borderRadius: 10,
      backgroundColor: '#FFF',
      overflow: 'hidden',
      margin: 0,
    },
    icon: {
      zIndex: 2,
      position: 'absolute',
      left: 12,
      color: colors.gray,
    },
  };

  return (
    <React.Fragment>
      <Modal visible={loading} transparent>
        <View style={styles.modalContent}>
          <View
            style={[{backgroundColor: colors.primary}, styles.loadingModal]}>
            <StyledText style={{color: colors.specialText, marginVertical: 5}}>
              Authentification
            </StyledText>
            <ActivityIndicator size={30} color={colors.specialText} />
          </View>
        </View>
      </Modal>

      <Modal
        visible={error.enabled}
        transparent
        onRequestClose={() => {
          setError({enabled: false, message: ''});
        }}>
        <View style={styles.modalContent}>
          <View style={[{backgroundColor: colors.warning}, styles.errorModal]}>
            <TouchableOpacity
              style={styles.modalCloseButton}
              onPress={() => {
                setError({enabled: false, message: ''});
              }}>
              <StyledText style={{color: colors.specialText}}>X</StyledText>
            </TouchableOpacity>
            <StyledText style={{color: colors.specialText, marginVertical: 5}}>
              {error.message}
            </StyledText>
          </View>
        </View>
      </Modal>

      <View
        padder
        style={{
          flex: 1,
          marginBottom: 70,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <View style={{alignSelf: 'center', marginVertical: 30}}>
          <StyledText type="medium" style={{color: colors.gray, fontSize: 20}}>
            Complétez votre profil
          </StyledText>
        </View>

        <View
          style={{
            flex: 1,
            justifyContent: 'center',
          }}>
          <Form>
            <Item
              style={{
                marginLeft: 0,
                borderBottomWidth: 0,
                marginVertical: 10,
              }}>
              <Icon
                type="MaterialIcons"
                name="person"
                style={inputStyles.icon}
              />
              <Input
                placeholderTextColor={colors.placeholder}
                placeholder="Votre nom"
                style={inputStyles.field}
                onChangeText={setUsername}
              />
            </Item>
            <Item
              style={{
                marginLeft: 0,
                borderBottomWidth: 0,
              }}>
              <Icon
                type="MaterialIcons"
                name="phone"
                style={inputStyles.icon}
              />
              <Input
                keyboardType="numeric"
                textContentType="telephoneNumber"
                dataDetectorTypes="phoneNumber"
                placeholderTextColor={colors.placeholder}
                placeholder="Numéro de téléphone"
                style={inputStyles.field}
                onChangeText={setPhone}
              />
            </Item>
          </Form>

          <View style={{marginTop: 5}}>
            <Button
              rounded
              style={[
                styles.button,
                {
                  backgroundColor: colors.primary,
                  borderColor: colors.primary,
                },
              ]}
              onPress={saveUserDatas}>
              <StyledText
                style={[styles.buttonText, {color: colors.specialText}]}>
                Accéder à la plateforme
              </StyledText>
            </Button>
          </View>
        </View>
      </View>
    </React.Fragment>
  );
}

const styles = StyleSheet.create({
  modalContent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(1, 1, 1, .5)',
  },
  loadingModal: {
    width: '50%',
    height: 100,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalCloseButton: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: 7,
    right: 5,
  },
  errorModal: {
    width: '70%',
    height: 90,
    paddingHorizontal: 15,
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
  },
  buttonsSection: {
    flex: 1,
    marginVertical: 20,
    justifyContent: 'flex-end',
  },
  button: {
    borderRadius: 10,
    marginVertical: 20,
    borderWidth: 1 / 5,
    height: 50,
    width: '100%',
    alignSelf: 'center',
  },
  buttonText: {
    width: '100%',
    textAlign: 'center',
  },
});

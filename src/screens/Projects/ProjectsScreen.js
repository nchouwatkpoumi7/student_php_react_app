/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {Body, Container, Content, Header, Icon, Left, View} from 'native-base';
import React from 'react';
import {
  PixelRatio,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ProjectCard from '../../components/Projects/ProjectCard';
import ProjectsStatusChart from '../../components/Projects/ProjectStatusChart';
import StatusFilterButton from '../../components/Projects/StatusFilterButton';
import StyledText from '../../components/StyledText';
import {PROJECT_STATUS} from '../../constants/projects';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

const ProjectsScreen = ({navigation}) => {
  const {colors} = useTheme();

  const getEntreprises = useSelector(state => state.entreprises);

  const {entreprises = []} = getEntreprises;

  const [projects, setProjects] = React.useState([]);
  const [currentStatus, setCurrentStatus] = React.useState('all');
  const [ProjectsToDisplay, setProjectsToDisplay] = React.useState([]);
  const [allProjects, setAllProjects] = React.useState([]);
  const dispatch = useDispatch();

  const Status = PROJECT_STATUS.map(status => ({
    ...status,
    active: status.key === currentStatus,
  }));

  React.useEffect(() => {
    const unsubscribe = firestore()
      .collection('walcom-projects')
      .where('user_id', '==', auth().currentUser.uid)
      .orderBy('updated_at', 'desc')
      .onSnapshot(snapshot => {
        setProjects(snapshot.docs.map(doc => ({id: doc.id, ...doc.data()})));
      });

    return () => {
      unsubscribe();
    };
  }, [currentStatus, dispatch, entreprises, navigation]);

  React.useEffect(() => {
    if (projects.length) {
      setAllProjects(projects);
    }

    if (currentStatus === 'all') {
      setProjectsToDisplay(prev =>
        !projects.length
          ? prev
          : projects.map(p => ({
              ...p,
              entreprise: entreprises.find(
                entreprise => entreprise.id === p.entreprise_id,
              ),
            })),
      );
    } else {
      setProjectsToDisplay(prev =>
        !projects.length
          ? prev
          : projects
              .filter(p => p.status === currentStatus)
              .map(p => ({
                ...p,
                entreprise: entreprises.find(
                  entreprise => entreprise.id === p.entreprise_id,
                ),
              })),
      );
    }
    return () => {};
  }, [currentStatus, dispatch, entreprises, navigation, projects]);

  return (
    <Container>
      <Header
      androidStatusBarColor={colors.primary}
        style={{
          alignItems: 'center',
          backgroundColor: colors.primary,
        }}>
        <Left>
          <TouchableOpacity
            style={styles.open_drawer}
            onPress={() => {
              navigation.openDrawer();
            }}>
            <Icon
              type="MaterialIcons"
              name="apps"
              style={{
                fontSize: 23,
                color: colors.specialText,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body
          style={{
            flex: 2,
            jmarginLeft: 20,
          }}>
          <StyledText
            type="medium"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 13,
            }}>
            GESTION DES PROJETS
          </StyledText>
        </Body>
      </Header>
      {projects.length > 0 && (
        <Content style={{backgroundColor: colors.background}}>
          <View>
            <ProjectsStatusChart
              active={allProjects.filter(p => p.status === 'active').length}
              completed={
                allProjects.filter(p => p.status === 'completed').length
              }
              idle={allProjects.filter(p => p.status === 'idle').length}
            />
          </View>
          <View
            style={{
              borderBottomColor: '#888',
              borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
              marginHorizontal: 15,
              marginBottom: 25,
            }}
          />
          <ScrollView
            showsHorizontalScrollIndicator={false}
            horizontal
            style={{paddingHorizontal: 15}}>
            {Status.map(status => (
              <StatusFilterButton
                key={status.key}
                setCurrentStatus={setCurrentStatus}
                status={status}
              />
            ))}
          </ScrollView>
          <View padder>
            {ProjectsToDisplay.map((project, index) => (
              <ProjectCard
                key={index}
                project={project}
                navigation={navigation}
              />
            ))}
          </View>
        </Content>
      )}
      {!projects.length && (
        <View
          style={{
            flex: 1,
            backgroundColor: colors.background,
            justifyContent: 'center',
            alignItems: 'center',
            padding: 10,
          }}>
          <StyledText>
            Vous n'avez pas initié de projets pour l'instant.
          </StyledText>
          <StyledText type="medium" style={{marginTop: 8, textAlign: 'center'}}>
            Entrez en contact avec une entreprise pour lancer votre projet.
          </StyledText>
        </View>
      )}
    </Container>
  );
};

export default ProjectsScreen;

const styles = StyleSheet.create({
  open_drawer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
});

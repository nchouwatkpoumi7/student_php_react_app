/* eslint-disable react/prop-types */
import {useTheme} from '@react-navigation/native';
import {
  Body,
  Container,
  Content,
  Header,
  Icon,
  Left,
  View,
  Fab,
} from 'native-base';
import React from 'react';
import {PixelRatio, StyleSheet, TouchableOpacity} from 'react-native';
import ProjectsStatusChart from '../../components/Projects/ProjectStatusChart';
import ProjectActivityCard from '../../components/Projects/ProjectActivityCard';
import StyledText from '../../components/StyledText';

const ProjectDetailsScreen = ({route, navigation}) => {
  const {colors} = useTheme();
  const {project} = route.params;

  return (
    <Container>
      <Header
        style={{
          alignItems: 'center',
          backgroundColor: colors.primary,
        }}>
        <Left>
          <TouchableOpacity
            style={styles.open_drawer}
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              type="MaterialIcons"
              name="arrow-back"
              style={{
                fontSize: 23,
                color: colors.specialText,
              }}
            />
          </TouchableOpacity>
        </Left>
        <Body
          style={{
            flex: 2,
            justifyContent: 'center',
          }}>
          <StyledText
            type="medium"
            style={{
              color: colors.specialText,
              textTransform: 'uppercase',
              fontSize: 12,
            }}>
            {project.title}
          </StyledText>
        </Body>
      </Header>
      <Content style={{backgroundColor: colors.background}}>
        <View>
          <ProjectsStatusChart
            type="single"
            progression={project.progression}
            label="tâche"
            label_active="active"
            label_completed="achevée"
            active={
              project.activities?.filter(p => p.status === 'active').length
            }
            completed={
              project.activities?.filter(p => p.status === 'completed').length
            }
            idle={0}
          />
        </View>

        <View style={styles.divider} />

        <View padder>
          <StyledText type="regular">Évolution des travaux</StyledText>
          {project.activities
            ?.sort((a, b) => b.updated_at - a.updated_at)
            .map((activity, index) => (
              <ProjectActivityCard
                key={index}
                activity={activity}
                navigation={navigation}
              />
            ))}
        </View>
      </Content>
      <Fab
        active
        direction="up"
        containerStyle={{}}
        style={{backgroundColor: colors.primary}}
        position="bottomRight"
        onPress={() => {
          navigation.navigate('ChatRoom', {
            chat: {
              ...project.entreprise,
              entreprise_id: project.entreprise.id,
              id: null,
            },
          });
        }}>
        <Icon type="MaterialIcons" name="question-answer" />
      </Fab>
    </Container>
  );
};

export default ProjectDetailsScreen;

const styles = StyleSheet.create({
  open_drawer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    paddingLeft: 6,
  },
  divider: {
    borderBottomColor: '#888',
    borderBottomWidth: 1 / PixelRatio.getPixelSizeForLayoutSize(1),
    marginHorizontal: 15,
    marginBottom: 8,
  },
});

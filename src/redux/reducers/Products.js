import * as ActionsType from '../constants/Products';

export function ProductsReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_PRODUCTS_REQUEST:
      return {
        loading: true,
        products: [],
      };
    case ActionsType.GET_PRODUCTS_SUCCESS:
      return {
        loading: false,
        products: action.payload.products,
      };
    case ActionsType.GET_PRODUCTS_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export function ProductsCategoriesReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_PRODUCTS_CATEGORIES_REQUEST:
      return {
        loading: true,
        products_categories: [],
      };
    case ActionsType.GET_PRODUCTS_CATEGORIES_SUCCESS:
      return {
        loading: false,
        products_categories: action.payload.products_categories,
      };
    case ActionsType.GET_PRODUCTS_CATEGORIES_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

import * as ActionsType from '../constants/DrawerSelectedItem';

export function DrawerSelectedItemReducer(state = 'home', action) {
  switch (action.type) {
    case ActionsType.SET_DRAWER_SELECTED_ITEM_SUCCESS:
      return {
        loading: false,
        drawer_selected_item: action.payload.drawer_selected_item,
      };
    default:
      return state;
  }
}

import * as ActionsType from '../constants/AppConfigs';

export function AppConfigsReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_APP_CONFIGS_REQUEST:
      return {
        loading: true,
        app_configs: {},
      };
    case ActionsType.GET_APP_CONFIGS_SUCCESS:
      return {
        loading: false,
        app_configs: action.payload.app_configs,
      };
    case ActionsType.GET_APP_CONFIGS_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export function ProductsCategoriesReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_PRODUCTS_CATEGORIES_REQUEST:
      return {
        loading: true,
        products_categories: [],
      };
    case ActionsType.GET_PRODUCTS_CATEGORIES_SUCCESS:
      return {
        loading: false,
        products_categories: action.payload.products_categories,
      };
    case ActionsType.GET_PRODUCTS_CATEGORIES_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

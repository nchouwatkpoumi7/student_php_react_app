import * as ActionsType from '../constants/Cart';

export function CartReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_CART_REQUEST:
      return {
        loading: true,
        cart: null,
      };
    case ActionsType.GET_CART_SUCCESS:
      return {
        loading: false,
        cart: action.payload.cart,
      };
    case ActionsType.GET_CART_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

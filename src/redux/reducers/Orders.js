import * as ActionsType from '../constants/Orders';

export function OrderReducer(state = {}, action) {
	switch (action.type) {
	case ActionsType.GET_ORDERS_REQUEST:
		return {
			loading: true,
			orders: [],
		};
	case ActionsType.GET_ORDERS_SUCCESS:
		return {
			loading: false,
			orders: action.payload.orders,
		};
	case ActionsType.GET_ORDERS_FAIL:
		return {
			loading: false,
			error: action.error
		};
	default:
		return state;
	}
}

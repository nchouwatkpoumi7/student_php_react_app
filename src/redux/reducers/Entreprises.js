import * as ActionsType from '../constants/Entreprises';

export function EntrepriseReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_ENTREPRISES_REQUEST:
      return {
        loading: true,
        entreprises: [],
      };
    case ActionsType.GET_ENTREPRISES_SUCCESS:
      return {
        loading: false,
        entreprises: action.payload.entreprises,
      };
    case ActionsType.GET_ENTREPRISES_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

export function ServiceReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_SERVICES_REQUEST:
      return {
        loading: true,
        services: [],
      };
    case ActionsType.GET_SERVICES_SUCCESS:
      return {
        loading: false,
        services: action.payload.services,
      };
    case ActionsType.GET_SERVICES_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

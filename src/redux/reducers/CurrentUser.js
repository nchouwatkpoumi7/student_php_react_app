import * as ActionsType from '../constants/CurrentUser';

export function CurrentUserReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_CURRENTUSER_REQUEST:
      return {
        loading: true,
        currentUser: {},
      };
    case ActionsType.GET_CURRENTUSER_SUCCESS:
      return {
        loading: false,
        currentUser: action.payload.currentUser,
      };
    case ActionsType.GET_CURRENTUSER_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

import * as ActionsType from '../constants/LocationInfos';

export function LocationInfosReducer(state = {}, action) {
  switch (action.type) {
    case ActionsType.GET_LOCATIONINFOS_REQUEST:
      return {
        loading: true,
        locationInfos: [],
      };
    case ActionsType.GET_LOCATIONINFOS_SUCCESS:
      return {
        loading: false,
        locationInfos: action.payload.locationInfos,
      };
    case ActionsType.GET_LOCATIONINFOS_FAIL:
      return {
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

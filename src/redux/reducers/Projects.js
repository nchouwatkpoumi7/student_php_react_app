import * as ActionsType from '../constants/Projects';

export function ProjectsReducer(state = {}, action) {
	switch (action.type) {
	case ActionsType.GET_PROJECTS_REQUEST:
		return {
			loading: true,
			projects: [],
		};
	case ActionsType.GET_PROJECTS_SUCCESS:
		return {
			loading: false,
			projects: action.payload.projects,
		};
	case ActionsType.GET_PROJECTS_FAIL:
		return {
			loading: false,
			error: action.error,
		};
	default:
		return state;
	}
}

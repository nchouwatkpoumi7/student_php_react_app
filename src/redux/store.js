import {applyMiddleware, combineReducers, createStore} from 'redux';

import * as CurrentUserReducers from './reducers/CurrentUser';
import * as ProjectsReducers from './reducers/Projects';
import * as LocationInfosReducers from './reducers/LocationInfos';

import * as CartReducers from './reducers/Cart';
import * as OrderReducers from './reducers/Orders';
import * as ProductsReducers from './reducers/Products';
import * as EntreprisesReducers from './reducers/Entreprises';

import * as DrawerReducers from './reducers/DrawerSelectedItem';
import * as AppConfigsReducers from './reducers/AppConfigs';

import thunk from 'redux-thunk';

export default createStore(
  combineReducers({
    currentUser: CurrentUserReducers.CurrentUserReducer,
    locationInfos: LocationInfosReducers.LocationInfosReducer,
    projects: ProjectsReducers.ProjectsReducer,

    cart: CartReducers.CartReducer,
    orders: OrderReducers.OrderReducer,
    products: ProductsReducers.ProductsReducer,
    products_categories: ProductsReducers.ProductsCategoriesReducer,

    entreprises: EntreprisesReducers.EntrepriseReducer,
    services: EntreprisesReducers.ServiceReducer,

    drawer_selected_item: DrawerReducers.DrawerSelectedItemReducer,
    app_configs: AppConfigsReducers.AppConfigsReducer,
  }),
  applyMiddleware(thunk),
);

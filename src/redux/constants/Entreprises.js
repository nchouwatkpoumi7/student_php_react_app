export const GET_ENTREPRISES_REQUEST = 'GET_ENTREPRISES_REQUEST';
export const GET_ENTREPRISES_SUCCESS = 'GET_ENTREPRISES_SUCCESS';
export const GET_ENTREPRISES_FAIL = 'GET_ENTREPRISES_FAIL';

export const GET_SERVICES_REQUEST = 'GET_SERVICES_REQUEST';
export const GET_SERVICES_SUCCESS = 'GET_SERVICES_SUCCESS';
export const GET_SERVICES_FAIL = 'GET_SERVICES_FAIL';

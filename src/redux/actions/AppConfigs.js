import * as ActionsType from '../constants/AppConfigs';

import { AppConfigsProvider } from '../../services/AppConfigsServices';

export const getAppConfigsAction = () => async (dispatch) => {
	dispatch({ type: ActionsType.GET_APP_CONFIGS_REQUEST });

	await AppConfigsProvider.getAll()
		.then((app_configs) => {
			dispatch({
				type: ActionsType.GET_APP_CONFIGS_SUCCESS,
				payload: { app_configs },
			});
		})
		.catch((reason) => {
			console.log(reason);
			dispatch({
				type: ActionsType.GET_APP_CONFIGS_FAIL,
				payload: { error: 'Erreur lors de la recuperation des app configs' },
			});
		});
};

import * as ActionsType from '../constants/Orders';

import {OrdersProvider} from '../../services/OrdersService';

export const getOrdersAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_ORDERS_REQUEST});

  await OrdersProvider.getAll()
    .then(orders => {
      dispatch({
        type: ActionsType.GET_ORDERS_SUCCESS,
        payload: {orders},
      });
    })
    .catch(reason => {
      console.log(reason);
      dispatch({
        type: ActionsType.GET_ORDERS_FAIL,
        payload: {error: 'Erreur lors de la recuperation des commandes'},
      });
    });
};

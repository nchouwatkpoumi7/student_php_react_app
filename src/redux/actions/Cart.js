import * as ActionsType from '../constants/Cart';

import AsyncStorage from '@react-native-async-storage/async-storage';

export const getCartAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_CART_REQUEST});

  await AsyncStorage.getItem('@CART')
    .then(res => {
      const cart = JSON.parse(res);
      dispatch({
        type: ActionsType.GET_CART_SUCCESS,
        payload: {cart},
      });
    })
    .catch(reason => {
      console.log(reason);
      dispatch({
        type: ActionsType.GET_CART_SUCCESS,
        payload: {cart: []},
      });
    });
};

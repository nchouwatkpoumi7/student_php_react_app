import * as ActionsType from '../constants/Projects';

import {ProjectsProvider} from '../../services/ProjectsService';

export const getProjectsAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_PROJECTS_REQUEST});

  await ProjectsProvider.getAll()
    .then(projects => {
      dispatch({
        type: ActionsType.GET_PROJECTS_SUCCESS,
        payload: {projects},
      });
    })
    .catch(reason => {
      console.log(reason);
      dispatch({
        type: ActionsType.GET_PROJECTS_FAIL,
        payload: {error: 'Erreur lors de la recuperation des projets'},
      });
    });
};

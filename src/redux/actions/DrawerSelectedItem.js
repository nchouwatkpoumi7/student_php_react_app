import * as ActionsType from '../constants/DrawerSelectedItem';

export const setDrawerSelectedItemAction = item_key => async dispatch => {
  dispatch({
    type: ActionsType.SET_DRAWER_SELECTED_ITEM_SUCCESS,
    payload: {drawer_selected_item: item_key},
  });
};

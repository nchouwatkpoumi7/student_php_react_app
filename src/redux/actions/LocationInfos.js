import * as ActionsType from '../constants/LocationInfos';
import Geolocation from 'react-native-geolocation-service';
import axios from 'axios';
import {PermissionsAndroid} from 'react-native';

export const getLocationInfosAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_LOCATIONINFOS_REQUEST});

  PermissionsAndroid.requestMultiple([
    'android.permission.ACCESS_COARSE_LOCATION',
    'android.permission.ACCESS_FINE_LOCATION',
  ])
    .then(() => {
      const observable = Geolocation.watchPosition(
        async position => {
          Geolocation.clearWatch(observable);

          await axios
            .get(
              `http://open.mapquestapi.com/geocoding/v1/reverse?key=9APdAggzudiRWce6YxSlJ5s5ye0L2O65&location=${position.coords.latitude},${position.coords.longitude}&includeRoadMetadata=true&includeNearestIntersection=true`,
            )
            .then(res => {
              console.log(res.data.results[0].locations[0]);
              dispatch({
                type: ActionsType.GET_LOCATIONINFOS_SUCCESS,
                payload: {
                  locationInfos: {
                    coords: position.coords,
                    infos: res.data.results[0].locations[0],
                  },
                },
              });
            })
            .catch(e => {
              dispatch({
                type: ActionsType.GET_LOCATIONINFOS_SUCCESS,
                payload: {
                  locationInfos: {
                    coords: {},
                    infos: {},
                  },
                },
              });
              console.log(e);
            });
        },
        error => {
          // See error code charts below.
          console.log(error.code, error.message);
          dispatch({
            type: ActionsType.GET_LOCATIONINFOS_FAIL,
            error: {code: error.code, message: error.message},
            payload: {
              locationInfos: {
                coords: {},
                infos: {},
              },
            },
          });
        },
        {
          enableHighAccuracy: true,
          timeout: 15000,
          maximumAge: 10000,
          forceRequestLocation: true,
          interval: 1000,
        },
      );
    })
    .catch(e => {
      console.log(e);
      dispatch({
        type: ActionsType.GET_LOCATIONINFOS_FAIL,
        error: {code: 12, message: "Erreur d'autorisation"},
      });
    });
};

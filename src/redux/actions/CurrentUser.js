import * as ActionsType from '../constants/CurrentUser';
import auth from '@react-native-firebase/auth';
import { AuthProvider } from '../../providers/Auth';

export const getCurrentUserAction = () => async (dispatch) => {
	dispatch({ type: ActionsType.GET_CURRENTUSER_REQUEST });
	await AuthProvider.get(auth().currentUser.uid)
		.then((res) => {
			const user = {
				...auth().currentUser.toJSON(),
				...res
			};

			dispatch({
				type: ActionsType.GET_CURRENTUSER_SUCCESS,
				payload: { currentUser: user },
			});
		})
		.catch(() => {
			dispatch({
				type: ActionsType.GET_CURRENTUSER_FAIL,
				payload: { error: 'Erreur lors de la recuperation de l\'user courant' },
			});
		});
};

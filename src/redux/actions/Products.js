import * as ActionsType from '../constants/Products';

import {ProductsProvider} from '../../services/ProductsService';

export const getProductsAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_PRODUCTS_REQUEST});

  await ProductsProvider.getAll()
    .then(products => {
      dispatch({
        type: ActionsType.GET_PRODUCTS_SUCCESS,
        payload: {products},
      });
    })
    .catch(reason => {
      console.log(reason);
      dispatch({
        type: ActionsType.GET_PRODUCTS_FAIL,
        payload: {error: 'Erreur lors de la recuperation des produits'},
      });
    });
};

export const getProductsCategoriesAction = () => async dispatch => {
  dispatch({type: ActionsType.GET_PRODUCTS_CATEGORIES_REQUEST});

  await ProductsProvider.getCategories()
    .then(products_categories => {
      dispatch({
        type: ActionsType.GET_PRODUCTS_CATEGORIES_SUCCESS,
        payload: {products_categories},
      });
    })
    .catch(reason => {
      console.log(reason);
      dispatch({
        type: ActionsType.GET_PRODUCTS_CATEGORIES_FAIL,
        payload: {error: 'Erreur lors de la recuperation des produits'},
      });
    });
};

import * as ActionsType from '../constants/Entreprises';

import { EntrepriseProvider } from '../../services/EntreprisesService';

export const getEntreprisesAction = () => async (dispatch) => {
	dispatch({ type: ActionsType.GET_ENTREPRISES_REQUEST });

	await EntrepriseProvider.getAll()
		.then((entreprises) => {
			dispatch({
				type: ActionsType.GET_ENTREPRISES_SUCCESS,
				payload: { entreprises },
			});
		})
		.catch((reason) => {
			console.log(reason);
			dispatch({
				type: ActionsType.GET_ENTREPRISES_FAIL,
				payload: { error: 'Erreur lors de la recuperation des entreprises' },
			});
		});
};

export const getServicesAction = () => async (dispatch) => {
	dispatch({ type: ActionsType.GET_SERVICES_REQUEST });

	await EntrepriseProvider.getCategories()
		.then((services) => {
			dispatch({
				type: ActionsType.GET_SERVICES_SUCCESS,
				payload: { services },
			});
		})
		.catch((reason) => {
			console.log(reason);
			dispatch({
				type: ActionsType.GET_SERVICES_FAIL,
				payload: { error: 'Erreur lors de la recuperation des services' },
			});
		});
};

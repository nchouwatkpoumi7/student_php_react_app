import React from 'react';
import {Root} from 'native-base';
import {NavigationContainer} from '@react-navigation/native';
import {currentTheme} from './src/themes/DefaultTheme';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import MainNavigation from './src/navigation/MainNavigation';

const App = () => {
  return (
    <Root>
      <NavigationContainer theme={currentTheme}>
        <Provider store={store}>
          <MainNavigation />
        </Provider>
      </NavigationContainer>
    </Root>
  );
};

export default App;
